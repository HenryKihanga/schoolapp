package com.example.shoolapp;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.shoolapp.model.Course;

import java.util.ArrayList;
import java.util.List;

public class FragmentViewCourse extends Fragment {
    private List<Course> courses;
    private RecyclerView recyclerView;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.view_courses_fragment,container,false);
        recyclerView = root.findViewById(R.id.recycler_view_new);
        recyclerView.setLayoutManager(new LinearLayoutManager(container.getContext()));
        recyclerView.setHasFixedSize(true);
        recyclerView.setVisibility(View.VISIBLE);


        courses = new ArrayList< >();
        for (int a = 0; a< Course.courses.length; a++){
            courses.add(Course.courses[a]);
        }


        MyAdapter adapter = new MyAdapter(courses,container.getContext());
        recyclerView.setAdapter(adapter);


        return root;
    }


}
