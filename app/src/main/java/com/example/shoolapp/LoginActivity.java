package com.example.shoolapp;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.shoolapp.database.DatabaseHelper;
import com.example.shoolapp.model.PasswordUtils;
import com.example.shoolapp.model.User;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;

public class LoginActivity extends AppCompatActivity {
    TextInputLayout userNameTextInput;
    TextInputLayout passwordTextInput;
    Button loginBtn;

    PasswordUtils passwordUtils = new PasswordUtils();
    DatabaseHelper databaseHelper = new DatabaseHelper(this);
    User user = databaseHelper.user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        userNameTextInput = findViewById(R.id.textInput_user_name);
        passwordTextInput = findViewById(R.id.textInput_password);
        loginBtn = findViewById(R.id.button_login);

        loginBtn.setOnClickListener(
                new View.OnClickListener() {
                    @RequiresApi(api = Build.VERSION_CODES.O)
                    @Override
                    public void onClick(View v) {
                        if(!validateUserName()|!validatePassword()){
                            Toast.makeText(LoginActivity.this, "Cant login there are some errors", Toast.LENGTH_LONG).show();
                        }
                        else {
                            if(validateuser()){
                                if(user.getRole().equals("admin")){
                                    Intent i = new Intent(LoginActivity.this,DrawerActivity.class);
                                    startActivity(i);
                                }
                            }
                            else
                                Snackbar.make(v, "Incorrect Password or Username", Snackbar.LENGTH_LONG)
                                        .setAction("Error", null).setDuration(1000).show();
                        }

                    }
                });
    }
    private boolean validateUserName() {
        String userName = userNameTextInput.getEditText().getText().toString().trim();

        if (userName.isEmpty()) {
            userNameTextInput.setError("Field can't be empty");
            return false;
        } else {
            userNameTextInput.setError(null);
            return true;
        }
    }
    private boolean validatePassword() {
        String userPass = passwordTextInput.getEditText().getText().toString().trim();

        if (userPass.isEmpty()) {
            passwordTextInput.setError("Field can't be empty");
            return false;
        } else {
            passwordTextInput.setError(null);
            return true;
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    private boolean validateuser(){
        boolean passwordMatch = false;
        //calling user info from database helper
        databaseHelper.getUserInfo(userNameTextInput.getEditText().getText().toString().trim());


        // User provided password to validate
        String providedPassword = passwordTextInput.getEditText().getText().toString().trim();

        // Encrypted and Base64 encoded password read from database
        String securePassword = user.getPassWord();

        // Salt value stored in database
        String salt = user.getSalt();
        if(salt == null){

            return false;
        }
        else {
            passwordMatch = passwordUtils.verifyUserPassword(providedPassword, securePassword, salt);
            if(passwordMatch)
            {
                Toast.makeText(this,"SUCCESFUL LOGGED IN",Toast.LENGTH_LONG).show();
                return true;
            } else {

                return false;

            }
        }

    }

}
