package com.example.shoolapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.shoolapp.model.User;

public class MainActivity extends AppCompatActivity {
    Button loginbtn,registration_screen,viewcoursebtn,browsebtn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        loginbtn = findViewById(R.id.logninrefer);
        registration_screen=findViewById(R.id.reg_ref);
        viewcoursebtn=findViewById(R.id.course_list_refer);
        browsebtn=findViewById(R.id.browse);

        loginbtn.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent i = new Intent(MainActivity.this,LoginActivity.class);
                        startActivity(i);

                    }
                }
        );
        registration_screen.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent a = new Intent(MainActivity.this,StudentRegistration.class);
                        startActivity(a);

                    }
                }
        );
        viewcoursebtn.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent b = new Intent(MainActivity.this,DrawerActivity.class);
                        startActivity(b);

                    }
                }
        );

        browsebtn.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent openVideo = new Intent(Intent.ACTION_VIEW, Uri.parse("smsto:+2550653470846"));
                        startActivity(openVideo);
                    }
                }
        );


        //create a music player
//
//        MediaPlayer notification =MediaPlayer.create(this, Settings.System.DEFAULT_ALARM_ALERT_URI);
//        notification.setLooping(true);
//        notification.start();



    }
}
