package com.example.shoolapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;

import androidx.recyclerview.widget.RecyclerView;

import com.example.shoolapp.model.Course;

import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyviewHolder> {

    private List<Course> courses;
    private Context context;


    public MyAdapter(List<Course> courses, Context context) {
        this.courses = courses;
        this.context = context;
    }


    @NonNull
    @Override
    public MyviewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.course_list, parent, false);


        return new MyviewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyviewHolder holder, int position) {
        Course course = courses.get(position);

        holder.course_code.setText(course.getCode());
        holder.course_name.setText(course.getName());
        holder.course_credit.setText(course.getCredit());
        holder.course_type.setText(course.getType());
        if (course.isRegistered() == true) {
            holder.course_register.setChecked(true);
        }


        holder.timetable_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (0 == 1) {
                    //show message
                    showMessage("Alert", "Currently there is no timetable ");
                    return;

                } else {
                    StringBuffer buffer = new StringBuffer();
                    for (int i = 0; i <= 2; i++) {
                        buffer.append("Day :" + "Tuesday\n");
                        buffer.append("Venue :" + "D01 luhanga hall\n");
                        buffer.append("Time :" + "1800-2000\n\n");

                    }
                    //show data
                    showMessage("Course Weekly Timetable", buffer.toString());

                }


            }
        });

    }
//method to manage timetable dialog
    public void showMessage(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.show();

    }

    @Override
    public int getItemCount() {
        return courses.size();
    }


    public class MyviewHolder extends RecyclerView.ViewHolder {
        TextView course_code;
        TextView course_name;
        TextView course_credit;
        TextView course_type;
        CheckBox course_register;
        Button   timetable_btn;

        public MyviewHolder(@NonNull View itemView) {
            super(itemView);

            course_code = itemView.findViewById(R.id.course_code);
            course_name = itemView.findViewById(R.id.course_name);
            course_credit = itemView.findViewById(R.id.course_credit);
            course_type = itemView.findViewById(R.id.course_type);
            course_register = itemView.findViewById(R.id.register_course);
            timetable_btn = itemView.findViewById(R.id.timetable_btn);
        }
    }


}
