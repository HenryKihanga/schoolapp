package com.example.shoolapp;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.shoolapp.database.DatabaseHelper;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Random;
import java.util.regex.Pattern;

public class RegistrationActivity extends AppCompatActivity {
    private TextInputLayout textInputFname, textInputMname, textInputLname, textInputPhone, textInputEmail , textInputRegno;
    private TextInputEditText textInputEditTextRegno;
    private Button buttonRegister;
    private Spinner program_spinner, study_year_spinner, study_semester_spinner, word_spinner, district_spinner, region_spinner;
    private ImageButton setdate_of_birth_button,setdate_of_registration_button;
    private TextView birthdate_text_editor,regdate_text_editor;
    private DatePickerDialog.OnDateSetListener birthdatePickerDialogListener;
    private DatePickerDialog.OnDateSetListener regdatePickerDialogListener;
    private RadioButton rdbmale;
    private Integer id =1;

    DatabaseHelper databaseHelper = new DatabaseHelper(this);


    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        //finding views
        textInputFname = findViewById(R.id.textInput_first_name);
        textInputMname = findViewById(R.id.textInput_middle_name);
        textInputLname = findViewById(R.id.textInput_last_name);
        textInputPhone = findViewById(R.id.textInput_phone_number);
        textInputEmail = findViewById(R.id.textInput_email_address);
        textInputRegno = findViewById(R.id.textInput_reg_no);
        textInputEditTextRegno = findViewById(R.id.textInputEditText_reg_no);
        buttonRegister = findViewById(R.id.button_register);
        program_spinner = findViewById(R.id.spinner_program);
        study_year_spinner = findViewById(R.id.spinner_year_of_study);
        study_semester_spinner = findViewById(R.id.spinner_semester_of_study);
        region_spinner = findViewById(R.id.spinner_region);
        district_spinner = findViewById(R.id.spinner_district);
        word_spinner = findViewById(R.id.spinner_word);
        rdbmale = findViewById(R.id.radioButton_male);
        birthdate_text_editor = findViewById(R.id.date_of_birth_text_editor);
        regdate_text_editor = findViewById(R.id.registration_date_text_editor);
        setdate_of_birth_button = findViewById(R.id.dob_picker_btn);
        setdate_of_registration_button = findViewById(R.id.dor_picker_btn);



        //my methods
//        insertPlacesInDatabase();
        insertProgramsInDatabase();
        insertRolesInDatabase();
        insertAdmin();
        spinners();
        datePickers();
        String regno = genStudentRegNumber();


        textInputEditTextRegno.setEnabled(false);
        textInputEditTextRegno.setText(regno);
//        textInputEditTextRegno.setTextColor(000000);
        textInputEditTextRegno.setTextSize(20);


        //get gender
        final String gender = rdbmale.getText().toString();
        buttonRegister.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        if (!validateEmail() | !validateFname() | !validateLname() | !validatePhoneNumber()) {
                            Toast.makeText(RegistrationActivity.this, "Cant login there are some errors", Toast.LENGTH_LONG).show();
                            return;
                        }

                        else {
                            StringBuffer s = new StringBuffer();
                            s.append(textInputFname.getEditText().getText().toString().trim() + "\n");
                            s.append(textInputMname.getEditText().getText().toString().trim() + "\n");
                            s.append(textInputLname.getEditText().getText().toString().trim() + "\n");
                            s.append(textInputPhone.getEditText().getText().toString().trim() + "\n");
                            s.append(textInputEmail.getEditText().getText().toString().trim() + "\n");
                            s.append(gender + "\n");
                            s.append(birthdate_text_editor.getText().toString() + "\n");
                            s.append(region_spinner.getSelectedItem().toString()+ "\n");
                            s.append(district_spinner.getSelectedItem().toString() + "\n");
                            s.append(word_spinner.getSelectedItem().toString() + "\n");
                            s.append(program_spinner.getSelectedItem().toString() + "\n");
                            s.append(study_year_spinner.getSelectedItem().toString() + "\n");
                            s.append(study_semester_spinner.getSelectedItem().toString() + "\n");
                            s.append(textInputRegno.getEditText().getText().toString().trim() + "\n");
                            s.append(regdate_text_editor.getText().toString() + "\n");
                            System.out.println(s);
                            Toast.makeText(RegistrationActivity.this, "Successful registered", Toast.LENGTH_LONG).show();
                        }

                    }

                }
        );


    }
    //*******************SPINNERS METHOD******************************************
    private void spinners(){
        //defining an object of database class

        DatabaseHelper databaseHelper = new DatabaseHelper(this);
        //Spinner for programs
        ArrayAdapter<String> programAdapter =
                new ArrayAdapter<String>(RegistrationActivity.this,
                        android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.programs));
        programAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        program_spinner.setAdapter(programAdapter);


        //Spinner for year of study
        ArrayAdapter<String> yearAdapter =
                new ArrayAdapter<String>(RegistrationActivity.this,
                        android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.year_of_study));
        yearAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        study_year_spinner.setAdapter(yearAdapter);


        //Spinner for semester of study
        ArrayAdapter<String> semesterAdapter =
                new ArrayAdapter<String>(RegistrationActivity.this,
                        android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.semester_of_study));
        semesterAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        study_semester_spinner.setAdapter(semesterAdapter);


        //Spinner for regions
        ArrayList<String> listofRegions = databaseHelper.getPlaces(0);
        ArrayAdapter<String> regionAdapter =
                new ArrayAdapter<String>(RegistrationActivity.this,
                        android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.regions));
        regionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        region_spinner.setAdapter(regionAdapter);
        //get districts
        region_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                DatabaseHelper databaseHelper = new DatabaseHelper(RegistrationActivity.this);
                //get selected region
                String selectedRegionName = region_spinner.getSelectedItem().toString();
                Integer selectedRegionId = databaseHelper.getParentId(selectedRegionName);
                ArrayList<String> correspondingDistricts = databaseHelper.getPlaces(selectedRegionId);

                ArrayAdapter<String> districtAdapter =
                        new ArrayAdapter<String>(RegistrationActivity.this,
                                android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.district));
                districtAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                district_spinner.setAdapter(districtAdapter); }
            @Override
            public void onNothingSelected(AdapterView<?> parent) { }
        });
        //get words
        district_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                DatabaseHelper databaseHelper1 = new DatabaseHelper(RegistrationActivity.this);
                String selectedDistrictName = district_spinner.getSelectedItem().toString();
                System.out.println("XXXXXXXXXXXXXXXXXXXXx");
                System.out.println(selectedDistrictName);
                Integer selectedDistrictId = databaseHelper1.getParentId(selectedDistrictName);
                System.out.println(selectedDistrictId);
                ArrayList<String> correspondingWords = databaseHelper1.getPlaces(selectedDistrictId);
                System.out.println(correspondingWords);
                ArrayAdapter<String> wordsAdapter =
                        new ArrayAdapter<String>(RegistrationActivity.this,
                                android.R.layout.simple_list_item_1,getResources().getStringArray(R.array.word));
                wordsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                word_spinner.setAdapter(wordsAdapter);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }
//*****************************END OF SPINNERS METHOD******************************************






// ******************DATE PICKERS METHOD*************************************************

    private void datePickers(){

        //date of birth picker
        setdate_of_birth_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar calendar = Calendar.getInstance();
                int year = calendar.get(Calendar.YEAR);
                int month = calendar.get(Calendar.MONTH);
                int day = calendar.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(
                        RegistrationActivity.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        birthdatePickerDialogListener, year, month, day);

                datePickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                datePickerDialog.show();
            }
        });

        setdate_of_registration_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar calendar = Calendar.getInstance();
                int year = calendar.get(Calendar.YEAR);
                int month = calendar.get(Calendar.MONTH);
                int day = calendar.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(
                        RegistrationActivity.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        regdatePickerDialogListener, year, month, day);

                datePickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                datePickerDialog.show();
            }
        });

        birthdatePickerDialogListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month = month + 1;
                String date_of_birth = day + "/" + month + "/" + year;
                birthdate_text_editor.setText(date_of_birth);
            }
        };
        regdatePickerDialogListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month = month + 1;
                String date_of_birth = day + "/" + month + "/" + year;
                regdate_text_editor.setText(date_of_birth);
            }
        };

    }


//********************************END OF DATE PICKERS METHOD******************************************



//*******************************AUTO GENERATE REGISTRATION NUMBER************************************
    private String genStudentRegNumber(){
        StringBuffer s1 = new StringBuffer();
        StringBuffer s2 = new StringBuffer();
        StringBuffer s3 = new StringBuffer();
        StringBuffer s4 = new StringBuffer();
        StringBuffer s5 = new StringBuffer();
        StringBuffer s6 = new StringBuffer();
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        String uniformcode = year + "-04-";
        s1.append("0000");
        s2.append("000");
        s3.append("00");
        s4.append("0");
        s5.append("");
        s6.append(uniformcode);
        final int min = 0;
        final int max = 99999;
        final int random = new Random().nextInt((max - min) + 1) + min;

        if(random<10){
            s1.append(random);
            s6.append(s1);
            return s6.toString();
        }
        else if(random<100){
            s2.append(random);
            s6.append(s2);
            return s6.toString();
        }
        else if(random<1000){
            s3.append(random);
            s6.append(s3);
            return s6.toString();
        }
        else if(random<10000){
            s4.append(random);
            s6.append(s4);
            return s6.toString();
        }
        else if(random<100000){
            s5.append(random);
            s6.append(s5);
            return s6.toString();
        }
        else {
            return "can't store more than 99999";
        }


    }

//*******************************END OF AUTOMATIC GENERATION OF REGISTRATION NUMBER*****************




// ******************************TEXT FIELDS INPUT VALIDATIONS ***************************************
    private boolean validateFname() {
        String fname = textInputFname.getEditText().getText().toString().trim();

        if (fname.isEmpty()) {
            textInputFname.setError("Field can't be empty");
            return false;
        } else {
            textInputFname.setError(null);
            return true;
        }
    }

    private boolean validateLname() {
        String lname = textInputLname.getEditText().getText().toString().trim();

        if (lname.isEmpty()) {
            textInputLname.setError("Field can't be empty");
            return false;
        } else {
            textInputLname.setError(null);
            return true;
        }
    }

    private boolean validatePhoneNumber() {
        String phone = textInputPhone.getEditText().getText().toString().trim();

        if (phone.isEmpty()) {
            textInputPhone.setError("Field can't be empty");
            return false;
        } else {
            textInputPhone.setError(null);
            return true;
        }
    }

    private boolean validateEmail() {
        String email = textInputEmail.getEditText().getText().toString().trim();
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\." + "[a-zA-Z0-9_+&*-]+)*@" +
                "(?:[a-zA-Z0-9-]+\\.)+[a-z" + "A-Z]{2,7}$";
        Pattern pat = Pattern.compile(emailRegex);
        boolean isValidEmail = pat.matcher(email).matches();


        if (email.isEmpty()) {
            textInputEmail.setError("Field can't be empty");
            return false;
        } else {

            if (isValidEmail == true) {
                textInputEmail.setError(null);
                return true;
            } else {
                textInputEmail.setError("Enter valid email");
                return false;
            }
        }
    };


    //INSERTING PROGRAMS
    private void insertProgramsInDatabase(){

        databaseHelper.insertPrograms(1,"BSc in Computer Engineering and IT");
        databaseHelper.insertPrograms(2,"BSc in Telecommunication Engineering");
        databaseHelper.insertPrograms(3,"BSc in Electronics Science");
        databaseHelper.insertPrograms(4,"BSc in Computer Science");
        databaseHelper.insertPrograms(5,"BSc with Computer Science");

    }

    //INSERTING ROLES
    private void insertRolesInDatabase(){
        databaseHelper.insertRoles(1,"admin","System administrator,he/she register both staff and students");
        databaseHelper.insertRoles(2,"teacher","Teaches subjects,he/she view courses assigned and time table");
        databaseHelper.insertRoles(3,"student","Take subjects, he/she can view and register courses");
    }

    //INSERTING ADMIN CREDENTIALS
    @RequiresApi(api = Build.VERSION_CODES.O)
    private void insertAdmin(){
        databaseHelper.insertUsers("admin","admin",1);
    }


    //INSERTING PLACES IN TABLE PLACE
    private void insertPlacesInDatabase(){
        //mikoa ya tanzania bara
//        databaseHelper.insertPlaces(1,0,"Arusha");
//        databaseHelper.insertPlaces(2,0,"Dar es Salaam");
//        databaseHelper.insertPlaces(3,0,"Dodoma");
//        databaseHelper.insertPlaces(4,0,"Geita");
//        databaseHelper.insertPlaces(5,0,"Iringa");
//        databaseHelper.insertPlaces(6,0,"Kagera");
//        databaseHelper.insertPlaces(7,0,"Katavi");
//        databaseHelper.insertPlaces(8,0,"Kigoma");
//        databaseHelper.insertPlaces(9,0,"Kilimanjaro");
//        databaseHelper.insertPlaces(10,0,"Lindi");
//        databaseHelper.insertPlaces(11,0,"Manyara");
//        databaseHelper.insertPlaces(12,0,"Mara");
//        databaseHelper.insertPlaces(13,0,"Mbeya");
//        databaseHelper.insertPlaces(14,0,"Morogoro");
//        databaseHelper.insertPlaces(15,0,"Mtwara");
//        databaseHelper.insertPlaces(16,0,"Mwanza");
//        databaseHelper.insertPlaces(17,0,"Njombe");
//        databaseHelper.insertPlaces(18,0,"Pwani");
//        databaseHelper.insertPlaces(19,0,"Rukwa");
//        databaseHelper.insertPlaces(20,0,"Ruvuma");
//        databaseHelper.insertPlaces(21,0,"Shinyanga");
//        databaseHelper.insertPlaces(22,0,"Simiyu");
//        databaseHelper.insertPlaces(23,0,"Singida");
//        databaseHelper.insertPlaces(25,0,"Tabora");
//        databaseHelper.insertPlaces(26,0,"Tanga");
////        //wilaya za mkoa wa arusha
//        databaseHelper.insertPlaces(28,1,"Arusha Mjini");
//        databaseHelper.insertPlaces(29,1,"Arusha Vijijini");
//        databaseHelper.insertPlaces(30,1,"Karatu");
//        databaseHelper.insertPlaces(31,1,"Longido");
//        databaseHelper.insertPlaces(32,1,"Meru");
//        databaseHelper.insertPlaces(33,1,"Monduli");
//        databaseHelper.insertPlaces(34,1,"Ngorongoro");
//        //wilaya za mkoa wa Dar es Salaam
//        databaseHelper.insertPlaces(35,2,"Ilala");
//        databaseHelper.insertPlaces(36,2,"Kinondoni");
//        databaseHelper.insertPlaces(37,2,"Temeke");
//        //wilaya za mkoa wa Dodoma
//        databaseHelper.insertPlaces(38,3,"Bahi");
//        databaseHelper.insertPlaces(39,3,"Chamwino");
//        databaseHelper.insertPlaces(40,3,"Chemba");
//        databaseHelper.insertPlaces(41,3,"Dodoma Mjini");
//        databaseHelper.insertPlaces(42,3,"Kondoa");
//        databaseHelper.insertPlaces(43,3,"Kongwa");
//        databaseHelper.insertPlaces(44,3,"Mpwapwa");
//        //wilaya za mkoa wa Geita
//        databaseHelper.insertPlaces(45,4,"Bukombe");
//        databaseHelper.insertPlaces(46,4,"Chato");
//        databaseHelper.insertPlaces(47,4,"Geita");
//        databaseHelper.insertPlaces(48,4,"Mbogwe");
//        databaseHelper.insertPlaces(49,4,"Nyang'hwale");
//        //wilaya za mkoa wa Iringa
//        databaseHelper.insertPlaces(50,5,"Iringa Mjini");
//        databaseHelper.insertPlaces(51,5,"Iringa Vijijini");
//        databaseHelper.insertPlaces(52,5,"Kilolo");
//        databaseHelper.insertPlaces(53,5,"Mafinga Mjini");
//        databaseHelper.insertPlaces(54,5,"Mufindi");
//        //wilaya za mkoa wa Kagera
//        databaseHelper.insertPlaces(55,6,"Biharamulo");
//        databaseHelper.insertPlaces(56,6,"Bukoba Mjini");
//        databaseHelper.insertPlaces(57,6,"Bukoba Vijijini");
//        databaseHelper.insertPlaces(58,6,"Karagwe");
//        databaseHelper.insertPlaces(59,6,"Kyerwa");
//        databaseHelper.insertPlaces(60,6,"Muleba");
//        databaseHelper.insertPlaces(61,6,"Ngara");
//        //wilaya za mkoa wa Katavi
//        databaseHelper.insertPlaces(62,7,"Mlele");
//        databaseHelper.insertPlaces(63,7,"Mpanda Mjini");
//        databaseHelper.insertPlaces(64,7,"Mpanda Vijijini");
//        //wilaya za mkoa wa Kigoma
//        databaseHelper.insertPlaces(65,8,"Buhigwe");
//        databaseHelper.insertPlaces(66,8,"Kakonko");
//        databaseHelper.insertPlaces(67,8,"Kasulu Vijijini");
//        databaseHelper.insertPlaces(68,8,"Kasulu Mjini");
//        databaseHelper.insertPlaces(69,8,"Kibondo");
//        databaseHelper.insertPlaces(70,8,"Kigoma Vijijini");
//        databaseHelper.insertPlaces(71,8,"Kigoma-Ujiji");
//        databaseHelper.insertPlaces(72,8,"Uvinza");
//        //wilaya za mkoa wa Kilimanjaro
//        databaseHelper.insertPlaces(73,9,"Hai");
//        databaseHelper.insertPlaces(74,9,"Moshi Mjini");
//        databaseHelper.insertPlaces(75,9,"Moshi Vijijini");
//        databaseHelper.insertPlaces(76,9,"Mwanga");
//        databaseHelper.insertPlaces(77,9,"Rombo");
//        databaseHelper.insertPlaces(78,9,"Same");
//        databaseHelper.insertPlaces(79,9,"Siha");
//        //wilaya za mkoa wa Lindi
//        databaseHelper.insertPlaces(80,10,"Kilwa");
//        databaseHelper.insertPlaces(81,10,"Lindi Vijijini");
//        databaseHelper.insertPlaces(82,10,"Lindi Mjini");
//        databaseHelper.insertPlaces(83,10,"Liwale");
//        databaseHelper.insertPlaces(84,10,"Nachingwea");
//        databaseHelper.insertPlaces(85,10,"Ruangwa");
//        //wilaya za mkoa wa Manyara
//        databaseHelper.insertPlaces(86,11,"Babati Mjini");
//        databaseHelper.insertPlaces(87,11,"Babati Vijijini");
//        databaseHelper.insertPlaces(88,11,"Hanang");
//        databaseHelper.insertPlaces(89,11,"Kiteto");
//        databaseHelper.insertPlaces(90,11,"Mbulu");
//        databaseHelper.insertPlaces(91,11,"Simanjiro");
//        //wilaya za mkoa wa Mara
//        databaseHelper.insertPlaces(92,12," Bunda");
//        databaseHelper.insertPlaces(93,12,"Butiama");
//        databaseHelper.insertPlaces(94,12,"Musoma Mjini");
//        databaseHelper.insertPlaces(95,12,"Musoma Vijijini");
//        databaseHelper.insertPlaces(96,12,"Rorya");
//        databaseHelper.insertPlaces(97,12,"Serengeti");
//        databaseHelper.insertPlaces(98,12,"Tarime");
//        //wilaya za mkoa wa Mbeya
//        databaseHelper.insertPlaces(99,13,"Chunya");
//        databaseHelper.insertPlaces(100,13,"Ileje");
//        databaseHelper.insertPlaces(101,13,"Kyela");
//        databaseHelper.insertPlaces(102,13,"Mbarali");
//        databaseHelper.insertPlaces(103,13,"Mbeya Vijijini");
//        databaseHelper.insertPlaces(104,13,"Mbeya Mjini");
//        databaseHelper.insertPlaces(105,13,"Mbozi");
//        databaseHelper.insertPlaces(106,13,"Momba");
//        databaseHelper.insertPlaces(107,13,"Rungwe");
//        databaseHelper.insertPlaces(108,13,"Tunduma");
//        //wilaya za mkoa wa Morogoro
//        databaseHelper.insertPlaces(109,14,"Gairo");
//        databaseHelper.insertPlaces(110,14,"Kilombero");
//        databaseHelper.insertPlaces(111,14,"Kilosa");
//        databaseHelper.insertPlaces(112,14,"Morogoro Mjini");
//        databaseHelper.insertPlaces(113,14,"Morogoro Vijijini");
//        databaseHelper.insertPlaces(114,14,"Mvomero");
//        databaseHelper.insertPlaces(115,14,"Ulanga");
//        //wilaya za mkoa wa mtwara
//        databaseHelper.insertPlaces(116,15,"Masasi Mjini");
//        databaseHelper.insertPlaces(117,15,"Masasi Vijijini");
//        databaseHelper.insertPlaces(118,15,"Mtwara Vijijini");
//        databaseHelper.insertPlaces(119,15,"Mtwara Mjini");
//        databaseHelper.insertPlaces(120,15,"Nanyumbu");
//        databaseHelper.insertPlaces(121,15,"Newala");
//        databaseHelper.insertPlaces(122,15,"Tandahimba");
//        //wilaya za mkoa wa mwanza
//        databaseHelper.insertPlaces(123,16,"Ilemela");
//        databaseHelper.insertPlaces(124,16,"Nyamagana");
//        databaseHelper.insertPlaces(125,16,"Sengerema");
//        databaseHelper.insertPlaces(126,16,"Kwimba");
//        databaseHelper.insertPlaces(127,16,"Magu");
//        databaseHelper.insertPlaces(128,16,"Misungwi");
//        databaseHelper.insertPlaces(129,16,"Ukerewe");
//        //wilaya za mkoa wa Njombe
//        databaseHelper.insertPlaces(130,17,"Ludewa");
//        databaseHelper.insertPlaces(131,17,"Makambako");
//        databaseHelper.insertPlaces(132,17,"Makete");
//        databaseHelper.insertPlaces(133,17,"Njombe Vijijini");
//        databaseHelper.insertPlaces(134,17,"Njombe Mjini");
//        databaseHelper.insertPlaces(135,17,"Wanging'ombe");
//        //wilaya za mkoa wa Pwani
//        databaseHelper.insertPlaces(136,18,"Bagamoyo");
//        databaseHelper.insertPlaces(137,18,"Kibaha Mjini");
//        databaseHelper.insertPlaces(138,18,"Kibaha Vijijini");
//        databaseHelper.insertPlaces(139,18,"Kisarawe");
//        databaseHelper.insertPlaces(140,18,"Mafia");
//        databaseHelper.insertPlaces(141,18,"Mkuranga");
//        databaseHelper.insertPlaces(142,18,"Rufiji");
//        //wilaya za mkoa wa Rukwa
//        databaseHelper.insertPlaces(143,19,"Kalambo");
//        databaseHelper.insertPlaces(144,19,"Nkasi");
//        databaseHelper.insertPlaces(145,19,"Sumbawanga Vijijini");
//        databaseHelper.insertPlaces(146,19,"Sumbawanga Mjini");
//        //wilaya za mkoa wa Ruvuma
//        databaseHelper.insertPlaces(147,20,"Mbinga");
//        databaseHelper.insertPlaces(148,20,"Namtumbo");
//        databaseHelper.insertPlaces(149,20,"Songea Vijijini");
//        databaseHelper.insertPlaces(150,20,"Songea Mjini");
//        databaseHelper.insertPlaces(151,20,"Tunduru");
//        databaseHelper.insertPlaces(152,20,"Nyasa");
//        //wilaya za mkoa wa Shinyanga
//        databaseHelper.insertPlaces(153,21,"Kahama Mjini");
//        databaseHelper.insertPlaces(154,21,"Kahama Vijijini");
//        databaseHelper.insertPlaces(155,21,"Kishapu");
//        databaseHelper.insertPlaces(156,21,"Shinyanga Vijijini");
//        databaseHelper.insertPlaces(157,21,"Shinyanga Mjini");
//        //wilaya za mkoa wa Simiyu
//        databaseHelper.insertPlaces(158,22," Bariadi");
//        databaseHelper.insertPlaces(159,22,"Busega");
//        databaseHelper.insertPlaces(160,22,"Itilima");
//        databaseHelper.insertPlaces(161,22,"Maswa");
//        databaseHelper.insertPlaces(162,22,"Meatu");
//        //wilaya za mkoa wa Singida
//        databaseHelper.insertPlaces(163,23,"Iramba");
//        databaseHelper.insertPlaces(164,23,"Manyoni");
//        databaseHelper.insertPlaces(165,23,"Singida Vijijini");
//        databaseHelper.insertPlaces(166,23,"Singida Mjini");
//        //wilaya za mkoa wa Tabora
//        databaseHelper.insertPlaces(167,25,"Igunga");
//        databaseHelper.insertPlaces(168,25,"Kaliua");
//        databaseHelper.insertPlaces(169,25,"Nzega");
//        databaseHelper.insertPlaces(170,25,"Sikonge");
//        databaseHelper.insertPlaces(171,25,"Uyui");
//        databaseHelper.insertPlaces(172,25,"Tabora Mjini");
//        databaseHelper.insertPlaces(173,25,"Urambo");
//        //wilaya za mkoa wa Tanga
//        databaseHelper.insertPlaces(174,26," Handeni");
//        databaseHelper.insertPlaces(175,26,"Kilindi");
//        databaseHelper.insertPlaces(176,26,"Korogwe");
//        databaseHelper.insertPlaces(177,26,"Lushoto");
//        databaseHelper.insertPlaces(178,26,"Mkinga");
//        databaseHelper.insertPlaces(179,26,"Muheza");
//        databaseHelper.insertPlaces(180,26,"Pangani");
//        databaseHelper.insertPlaces(181,26,"Tanga Mjini");
//        //Kata za wilaya ya Arusha Mjini mkoa wa Arusha
//        databaseHelper.insertPlaces(186,28,"Baraa");
//        databaseHelper.insertPlaces(187,28,"Daraja Mbili");
//        databaseHelper.insertPlaces(188,28,"Elerai");
//        databaseHelper.insertPlaces(189,28,"Engutoto");
//        databaseHelper.insertPlaces(190,28,"Kaloleni");
//        databaseHelper.insertPlaces(191,28,"Kimandolu");
//        databaseHelper.insertPlaces(192,28,"Lemara");
//        databaseHelper.insertPlaces(193,28,"Levolosi ");
//        databaseHelper.insertPlaces(194,28,"Ngarenaro");
//        databaseHelper.insertPlaces(195,28,"Oloirien");
//        databaseHelper.insertPlaces(196,28,"Sekei");
//        databaseHelper.insertPlaces(197,28,"Sokon I");
//        databaseHelper.insertPlaces(198,28,"Sombetini");
//        databaseHelper.insertPlaces(199,28,"Terrat");
//        databaseHelper.insertPlaces(200,28,"Themi ");
//        databaseHelper.insertPlaces(201,28," Unga L.T.D");
//        //Kata za wilaya ya Arusha Vijijini mkoa wa Arusha
//        databaseHelper.insertPlaces(202,29,"Bangata");
//        databaseHelper.insertPlaces(203,29," Bwawani ");
//        databaseHelper.insertPlaces(204,29,"Ilkiding'a ");
//        databaseHelper.insertPlaces(205,29,"Kimnyaki ");
//        databaseHelper.insertPlaces(206,29,"Kiranyi ");
//        databaseHelper.insertPlaces(207,29,"Kisongo");
//        databaseHelper.insertPlaces(208,29,"Mateves");
//        databaseHelper.insertPlaces(209,29,"Mlangarini");
//        databaseHelper.insertPlaces(210,29,"Moivo");
//        databaseHelper.insertPlaces(211,29,"Mussa ");
//        databaseHelper.insertPlaces(212,29,"Mwandeti");
//        databaseHelper.insertPlaces(213,29,"Nduruma ");
//        databaseHelper.insertPlaces(214,29,"Oldonyosambu ");
//        databaseHelper.insertPlaces(215,29,"Oljoro");
//        databaseHelper.insertPlaces(216,29,"Olkokola ");
//        databaseHelper.insertPlaces(217,29,"Olmotonyi ");
//        databaseHelper.insertPlaces(218,29,"Olorieni");
//        databaseHelper.insertPlaces(219,29,"Oltoroto");
//        databaseHelper.insertPlaces(220,29,"Oltrumet");
//        databaseHelper.insertPlaces(221,29,"Sambasha ");
//        databaseHelper.insertPlaces(222,29,"Sokon II");
//        //Kata za wilaya ya Karatu mkoa wa Arusha
//        databaseHelper.insertPlaces(223,30,"Baray");
//        databaseHelper.insertPlaces(224,30,"Buger ");
//        databaseHelper.insertPlaces(225,30,"Daa");
//        databaseHelper.insertPlaces(226,30,"Endabash");
//        databaseHelper.insertPlaces(227,30,"Endamaghang ");
//        databaseHelper.insertPlaces(228,30,"Endamarariek");
//        databaseHelper.insertPlaces(229,30,"Ganako");
//        databaseHelper.insertPlaces(230,30,"Kansay ");
//        databaseHelper.insertPlaces(231,30,"Karatu");
//        databaseHelper.insertPlaces(232,30,"Mang'ola");
//        databaseHelper.insertPlaces(233,30,"Mbulumbulu");
//        databaseHelper.insertPlaces(234,30,"Oldean");
//        databaseHelper.insertPlaces(235,30,"Qurus");
//        databaseHelper.insertPlaces(236,30,"Rhotia");
//        //Kata za wilaya ya Longido mkoa wa Arusha
//        databaseHelper.insertPlaces(237,31,"Eleng'ata Dapash");
//        databaseHelper.insertPlaces(238,31,"Engarenaibor");
//        databaseHelper.insertPlaces(239,31,"Engikaret");
//        databaseHelper.insertPlaces(240,31,"Gelai Lumbwa ");
//        databaseHelper.insertPlaces(241,31,"Gelai Meirugoi ");
//        databaseHelper.insertPlaces(242,31,"Ilorienito");
//        databaseHelper.insertPlaces(243,31,"Kamwanga");
//        databaseHelper.insertPlaces(244,31," Ketumbeine");
//        databaseHelper.insertPlaces(245,31,"Kimokouwa ");
//        databaseHelper.insertPlaces(246,31,"Longido");
//        databaseHelper.insertPlaces(247,31,"Matale");
//        databaseHelper.insertPlaces(248,31,"Mundarara");
//        databaseHelper.insertPlaces(249,31,"Namanga");
//        databaseHelper.insertPlaces(250,31,"Olmolog");
//        databaseHelper.insertPlaces(251,31,"Orbomba");
//        databaseHelper.insertPlaces(252,31,"Tingatinga");
//        //Kata za wilaya ya Meru mkoa wa Arusha
//        databaseHelper.insertPlaces(253,32,"Akheri");
//        databaseHelper.insertPlaces(254,32,"Kikatiti");
//        databaseHelper.insertPlaces(255,32,"Kikwe");
//        databaseHelper.insertPlaces(256,32,"King'ori");
//        databaseHelper.insertPlaces(257,32,"Leguruki ");
//        databaseHelper.insertPlaces(258,32,"Maji ya Chai");
//        databaseHelper.insertPlaces(259,32,"Makiba");
//        databaseHelper.insertPlaces(260,32,"Maroroni");
//        databaseHelper.insertPlaces(261,32,"Mbuguni ");
//        databaseHelper.insertPlaces(262,32,"Ngarenanyuki");
//        databaseHelper.insertPlaces(263,32," Nkoanrua");
//        databaseHelper.insertPlaces(264,32,"Nkoaranga ");
//        databaseHelper.insertPlaces(265,32," Nkoarisambu");
//        databaseHelper.insertPlaces(266,32," Poli");
//        databaseHelper.insertPlaces(267,32,"Seela Sing'isi");
//        databaseHelper.insertPlaces(268,32,"Songoro");
//        databaseHelper.insertPlaces(269,32,"Usa River");
//        //Kata za wilaya ya Monduli mkoa wa Arusha
//        databaseHelper.insertPlaces(270,33,"Engaruka");
//        databaseHelper.insertPlaces(271,33,"Engutoto");
//        databaseHelper.insertPlaces(272,33,"Esilalei ");
//        databaseHelper.insertPlaces(273,33,"Lepurko ");
//        databaseHelper.insertPlaces(274,33,"Lolkisale");
//        databaseHelper.insertPlaces(275,33,"Majengo");
//        databaseHelper.insertPlaces(276,33,"Makuyuni");
//        databaseHelper.insertPlaces(277,33,"Meserani");
//        databaseHelper.insertPlaces(278,33," Moita ");
//        databaseHelper.insertPlaces(279,33,"Monduli Juu");
//        databaseHelper.insertPlaces(280,33,"Monduli Mjini");
//        databaseHelper.insertPlaces(281,33,"Mswakini");
//        databaseHelper.insertPlaces(282,33,"Mto wa Mbu ");
//        databaseHelper.insertPlaces(283,33,"Selela");
//        databaseHelper.insertPlaces(284,33,"Sepeko");
//        //Kata za wilaya ya Ngorongoro mkoa wa Arusha
//        databaseHelper.insertPlaces(285,34,"Alailelai ");
//        databaseHelper.insertPlaces(286,34,"Arash");
//        databaseHelper.insertPlaces(287,34,"Digodigo");
//        databaseHelper.insertPlaces(288,34,"Enduleni");
//        databaseHelper.insertPlaces(289,34,"Enguserosambu");
//        databaseHelper.insertPlaces(290,34,"MKakesio");
//        databaseHelper.insertPlaces(291,34,"Maalon");
//        databaseHelper.insertPlaces(292,34,"Malambo ");
//        databaseHelper.insertPlaces(293,34,"Nainokanoka");
//        databaseHelper.insertPlaces(294,34," Naiyobi");
//        databaseHelper.insertPlaces(295,34,"Ngorongoro");
//        databaseHelper.insertPlaces(296,34,"Olbalbal");
//        databaseHelper.insertPlaces(297,34,"Oldonyosambu");
//        databaseHelper.insertPlaces(298,34,"Oloipiri ");
//        databaseHelper.insertPlaces(299,34,"Oloirien ");
//        databaseHelper.insertPlaces(300,34," Ololosokwan");
//        databaseHelper.insertPlaces(301,34,"Orgosorok");
//        databaseHelper.insertPlaces(302,34,"Pinyinyi");
//        databaseHelper.insertPlaces(303,34,"Sale");
//        databaseHelper.insertPlaces(304,34,"Samunge");
//        databaseHelper.insertPlaces(305,34,"Soit-Sambu");
//
//        databaseHelper.insertPlaces(306,35,"Buguruni");
//        databaseHelper.insertPlaces(307,35,"Chanika");
//        databaseHelper.insertPlaces(308,35,"Gerezani");
//        databaseHelper.insertPlaces(309,35,"Gongo la Mboto");
//        databaseHelper.insertPlaces(310,35,"Ilala");
//        databaseHelper.insertPlaces(311,35,"Jangwani");
//        databaseHelper.insertPlaces(312,35,"Kariakoo");
//        databaseHelper.insertPlaces(313,35,"Kimanga ");
//        databaseHelper.insertPlaces(314,35,"Kinyerezi");
//        databaseHelper.insertPlaces(315,35,"Kipawa");
//        databaseHelper.insertPlaces(316,35,"Kisutu");
//        databaseHelper.insertPlaces(317,35,"Kitunda");
//        databaseHelper.insertPlaces(318,35,"Kivukoni");
//        databaseHelper.insertPlaces(319,35,"Kiwalani");
//        databaseHelper.insertPlaces(320,35,"Majohe ");
//        databaseHelper.insertPlaces(321,35,"Mchafukoge");
//        databaseHelper.insertPlaces(322,35,"Mchikichini");
//        databaseHelper.insertPlaces(323,35,"Msongola ");
//        databaseHelper.insertPlaces(324,35," Pugu");
//        databaseHelper.insertPlaces(325,35,"Segerea");
//        databaseHelper.insertPlaces(326,35,"Tabata ");
//        databaseHelper.insertPlaces(327,35,"Ukonga ");
//        databaseHelper.insertPlaces(328,35,"Upanga Magharibi");
//        databaseHelper.insertPlaces(329,35," Upanga Mashariki ");
//        databaseHelper.insertPlaces(330,35,"Vingunguti");
//
//        databaseHelper.insertPlaces(331,36,"Bunju");
//        databaseHelper.insertPlaces(332,36," Goba ");
//        databaseHelper.insertPlaces(333,36,"• Hananasif");
//        databaseHelper.insertPlaces(334,36,"Kawe");
//        databaseHelper.insertPlaces(335,36,"Kibamba");
//        databaseHelper.insertPlaces(336,36,"kigogo");
//        databaseHelper.insertPlaces(337,36,"kijitonyama");
//        databaseHelper.insertPlaces(338,36,"Kimara");
//        databaseHelper.insertPlaces(339,36,"Kinondoni");
//        databaseHelper.insertPlaces(340,36,"Kunduchi");
//        databaseHelper.insertPlaces(341,36,"Kwembe");
//        databaseHelper.insertPlaces(342,36,"Mabibo");
//        databaseHelper.insertPlaces(343,36,"Mabwepande");
//        databaseHelper.insertPlaces(344,36,"Magomeni");
//        databaseHelper.insertPlaces(345,36,"Makongo");
//        databaseHelper.insertPlaces(346,36,"Makuburi");
//        databaseHelper.insertPlaces(347,36,"Makumbusho");
//        databaseHelper.insertPlaces(348,36,"Makurumia");
//        databaseHelper.insertPlaces(349,36,"Manzese");
//        databaseHelper.insertPlaces(350,36,"Mbezi");
//        databaseHelper.insertPlaces(351,36,"Mbezi juu");
//        databaseHelper.insertPlaces(352,36,"Mburahati");
//        databaseHelper.insertPlaces(353,36,"Mbweni");
//        databaseHelper.insertPlaces(354,36,"Mikocheni");
//        databaseHelper.insertPlaces(355,36,"Masasi");
//        databaseHelper.insertPlaces(356,36,"Msigani");
//        databaseHelper.insertPlaces(357,36,"Mwananyamala");
//        databaseHelper.insertPlaces(358,36,"Mzimuni");
//        databaseHelper.insertPlaces(359,36,"Ndugumbi");
//        databaseHelper.insertPlaces(360,36,"Saranga");
//        databaseHelper.insertPlaces(361,36,"Sinza");
//        databaseHelper.insertPlaces(362,36,"Tandale");
//        databaseHelper.insertPlaces(363,36,"Ubungo");
//        databaseHelper.insertPlaces(364,36,"Wazo");
//
//        databaseHelper.insertPlaces(365,37,"Azimio");
//        databaseHelper.insertPlaces(366,37,"Buza");
//        databaseHelper.insertPlaces(367,37,"Chamazi");
//        databaseHelper.insertPlaces(368,37,"Chang'ombe");
//        databaseHelper.insertPlaces(369,37,"Charambe");
//        databaseHelper.insertPlaces(370,37,"Keko");
//        databaseHelper.insertPlaces(371,37,"Kibada");
//        databaseHelper.insertPlaces(372,37,"Kiburugwa");
//        databaseHelper.insertPlaces(373,37,"kigambomi");
//        databaseHelper.insertPlaces(374,37,"Kijichi");
//        databaseHelper.insertPlaces(375,37,"Kilakala");
//        databaseHelper.insertPlaces(376,37,"Kimbiji");
//        databaseHelper.insertPlaces(377,37,"Kisarawe II");
//        databaseHelper.insertPlaces(378,37,"Kurasini");
//        databaseHelper.insertPlaces(379,37,"Makangarawe");
//        databaseHelper.insertPlaces(380,37,"Mbagala");
//        databaseHelper.insertPlaces(381,37,"Mbagala Kuu");
//        databaseHelper.insertPlaces(382,37,"Mianzi");
//        databaseHelper.insertPlaces(383,37,"Miburani");
//        databaseHelper.insertPlaces(384,37,"Mji Mwema");
//        databaseHelper.insertPlaces(385,37,"Mtoni");
//        databaseHelper.insertPlaces(386,37,"Pemba Mnazi");
//        databaseHelper.insertPlaces(387,37,"Samangira");
//        databaseHelper.insertPlaces(388,37,"Sandali");
//        databaseHelper.insertPlaces(389,37,"Tandika");
//        databaseHelper.insertPlaces(390,37,"Temeke");
//        databaseHelper.insertPlaces(391,37,"Toangoma");
//        databaseHelper.insertPlaces(392,37,"Tungi");
//        databaseHelper.insertPlaces(393,37,"Vijibweni");
//        databaseHelper.insertPlaces(394,37,"Yombo Vituka");
//
//        databaseHelper.insertPlaces(395,38,"Babayu");
//        databaseHelper.insertPlaces(396,38,"Bahi");
//        databaseHelper.insertPlaces(397,38,"chali");
//        databaseHelper.insertPlaces(398,38,"Chibelela");
//        databaseHelper.insertPlaces(399,38,"Chikola");
//        databaseHelper.insertPlaces(400,38,"Chipanga");
//        databaseHelper.insertPlaces(401,38,"Ibihwa");
//        databaseHelper.insertPlaces(402,38,"Ibugule");
//        databaseHelper.insertPlaces(403,38,"Ilindi");
//        databaseHelper.insertPlaces(404,38,"Kingwe");
//        databaseHelper.insertPlaces(405,38,"Lamati");
//        databaseHelper.insertPlaces(406,38,"Makanda");
//        databaseHelper.insertPlaces(407,38,"Mpalanga");
//        databaseHelper.insertPlaces(408,38,"Mpamantwa");
//        databaseHelper.insertPlaces(409,38,"Msisi");
//        databaseHelper.insertPlaces(410,38,"Mtitaa");
//        databaseHelper.insertPlaces(411,38,"Mundemu");
//        databaseHelper.insertPlaces(412,38,"Mwitikila");
//        databaseHelper.insertPlaces(413,38,"Nondwa");
//        databaseHelper.insertPlaces(414,38,"Zanka");
//
//        databaseHelper.insertPlaces(415,39,"Buigiri");
//        databaseHelper.insertPlaces(416,39,"Chiboli");
//        databaseHelper.insertPlaces(417,39,"Chilonwa");
//        databaseHelper.insertPlaces(418,39,"Chinugulu");
//        databaseHelper.insertPlaces(419,39,"Dabalo");
//        databaseHelper.insertPlaces(420,39,"Fufu");
//        databaseHelper.insertPlaces(421,39,"Handali");
//        databaseHelper.insertPlaces(422,39,"Haneti");
//        databaseHelper.insertPlaces(423,39,"Huzi");
//        databaseHelper.insertPlaces(424,39,"Idifu");
//        databaseHelper.insertPlaces(425,39,"Igandu");
//        databaseHelper.insertPlaces(426,39,"Ikowa");
//        databaseHelper.insertPlaces(427,39,"Iringa Mvumi Zamani");
//        databaseHelper.insertPlaces(428,39,"Itiso");
//        databaseHelper.insertPlaces(429,39,"Loja");
//        databaseHelper.insertPlaces(430,39,"Majereko");
//        databaseHelper.insertPlaces(431,39,"Makang'wa");
//        databaseHelper.insertPlaces(432,39,"Manchali");
//        databaseHelper.insertPlaces(433,39,"Manda");
//        databaseHelper.insertPlaces(434,39,"Manzese");
//        databaseHelper.insertPlaces(435,39,"Membe");
//        databaseHelper.insertPlaces(436,39,"Mlowa Bwawani");
//        databaseHelper.insertPlaces(437,39,"Mpwayungu");
//        databaseHelper.insertPlaces(438,39,"Msamato");
//        databaseHelper.insertPlaces(439,39,"Msanga");
//        databaseHelper.insertPlaces(440,39,"Muungano");
//        databaseHelper.insertPlaces(441,39,"Mvumi Makulu");
//        databaseHelper.insertPlaces(442,39,"Mvumi Mission");
//        databaseHelper.insertPlaces(443,39,"Nghambaku");
//        databaseHelper.insertPlaces(444,39,"Nhinhi");
//        databaseHelper.insertPlaces(445,39,"Segela");
//        databaseHelper.insertPlaces(446,39,"Zajilwa");
//
//        databaseHelper.insertPlaces(447,40,"Babayu");
//        databaseHelper.insertPlaces(448,40,"Chandama");
//        databaseHelper.insertPlaces(449,40,"Chemba");
//        databaseHelper.insertPlaces(450,40,"Churuku");
//        databaseHelper.insertPlaces(451,40,"Darai");
//        databaseHelper.insertPlaces(452,40,"Furkwa");
//        databaseHelper.insertPlaces(453,40,"Goima");
//        databaseHelper.insertPlaces(454,40,"Kwandi");
//        databaseHelper.insertPlaces(455,40,"Jangalo");
//        databaseHelper.insertPlaces(456,40,"Kimaha");
//        databaseHelper.insertPlaces(457,40,"Kwamtoro");
//        databaseHelper.insertPlaces(458,40,"Kidoka");
//        databaseHelper.insertPlaces(459,40,"Kinyamsindo");
//        databaseHelper.insertPlaces(460,40,"Lahoda");
//        databaseHelper.insertPlaces(461,40,"Laita");
//        databaseHelper.insertPlaces(462,40,"Makorongo");
//        databaseHelper.insertPlaces(463,40,"Mondo");
//        databaseHelper.insertPlaces(464,40,"Mpendo");
//        databaseHelper.insertPlaces(465,40,"Mrijo");
//        databaseHelper.insertPlaces(466,40,"Msaada");
//        databaseHelper.insertPlaces(467,40,"Ovada");
//        databaseHelper.insertPlaces(468,40,"Paranga");
//        databaseHelper.insertPlaces(469,40,"Sanzawa");
//        databaseHelper.insertPlaces(470,40,"Songoro");
//        databaseHelper.insertPlaces(471,40,"Soya");
//        databaseHelper.insertPlaces(472,40,"Tumbakose");
//
//        databaseHelper.insertPlaces(473,41,"Chanwa");
//        databaseHelper.insertPlaces(474,41,"Chwamwino");
//        databaseHelper.insertPlaces(475,41,"Chang'ombe");
//        databaseHelper.insertPlaces(476,41,"Chigongwe");
//        databaseHelper.insertPlaces(477,41,"Chihanga");
//        databaseHelper.insertPlaces(478,41,"Dodoma Makulu");
//        databaseHelper.insertPlaces(479,41,"Hazina");
//        databaseHelper.insertPlaces(480,41,"Hombolo");
//        databaseHelper.insertPlaces(481,41,"Ipagala");
//        databaseHelper.insertPlaces(482,41,"Ipala");
//        databaseHelper.insertPlaces(483,41,"Iyumbu");
//        databaseHelper.insertPlaces(484,41,"Kikombo");
//        databaseHelper.insertPlaces(485,41,"Kikuyu Kaskazini");
//        databaseHelper.insertPlaces(486,41,"Kikuyu Kusini");
//        databaseHelper.insertPlaces(487,41,"Kilimani");
//        databaseHelper.insertPlaces(488,41,"Kiwanja cha Ndege");
//        databaseHelper.insertPlaces(489,41,"Kizota");
//        databaseHelper.insertPlaces(490,41,"Madukani");
//        databaseHelper.insertPlaces(491,41,"Majengo");
//        databaseHelper.insertPlaces(492,41,"Makole");
//        databaseHelper.insertPlaces(493,41,"Makutupora");
//        databaseHelper.insertPlaces(494,41,"Mbabala");
//        databaseHelper.insertPlaces(495,41,"Mbalawala");
//        databaseHelper.insertPlaces(496,41,"Miyuji");
//        databaseHelper.insertPlaces(497,41,"Mkonze");
//        databaseHelper.insertPlaces(498,41,"Mnadani");
//        databaseHelper.insertPlaces(499,41,"Mpunguzi");
//        databaseHelper.insertPlaces(500,41,"Msalato");
//        databaseHelper.insertPlaces(501,41,"Mtumba");
//        databaseHelper.insertPlaces(502,41,"Nala");
//        databaseHelper.insertPlaces(503,41,"Ng'hong'honha");
//        databaseHelper.insertPlaces(504,41,"Ntyuka");
//        databaseHelper.insertPlaces(505,41,"Mzuguni");
//        databaseHelper.insertPlaces(506,41,"Tambukareli");
//        databaseHelper.insertPlaces(507,41,"Uhuru");
//        databaseHelper.insertPlaces(508,41,"Viwandani");
//        databaseHelper.insertPlaces(509,41,"Zuzu");
//
//        databaseHelper.insertPlaces(510,42,"Bereko");
//        databaseHelper.insertPlaces(511,42,"Bolisa");
//        databaseHelper.insertPlaces(512,42,"Bumbuta");
//        databaseHelper.insertPlaces(513,42,"Busi");
//        databaseHelper.insertPlaces(514,42,"Changaa");
//        databaseHelper.insertPlaces(515,42,"Chemchem");
//        databaseHelper.insertPlaces(516,42,"Haubi");
//        databaseHelper.insertPlaces(517,42,"Hondomairo");
//        databaseHelper.insertPlaces(518,42,"Itaswa");
//        databaseHelper.insertPlaces(519,42,"Itololo");
//        databaseHelper.insertPlaces(520,42,"Kalamba");
//        databaseHelper.insertPlaces(521,42,"Kikilo");
//        databaseHelper.insertPlaces(522,42,"Kokore");
//        databaseHelper.insertPlaces(523,42,"Kilimani");
//        databaseHelper.insertPlaces(524,42,"Kingare");
//        databaseHelper.insertPlaces(525,42,"kinyasi");
//        databaseHelper.insertPlaces(526,42,"Kisese");
//        databaseHelper.insertPlaces(527,42,"Kolo");
//        databaseHelper.insertPlaces(528,42,"Kondoa Mjini");
//        databaseHelper.insertPlaces(529,42,"Kwadelo");
//        databaseHelper.insertPlaces(530,42,"Masange");
//        databaseHelper.insertPlaces(531,42,"Mnenia");
//        databaseHelper.insertPlaces(532,42,"Pahi");
//        databaseHelper.insertPlaces(533,42,"Salanka");
//        databaseHelper.insertPlaces(534,42,"Serya");
//        databaseHelper.insertPlaces(535,42,"Soera");
//        databaseHelper.insertPlaces(536,42,"Suruke");
//        databaseHelper.insertPlaces(537,42,"Thawi");
//
//        databaseHelper.insertPlaces(538,43,"Chamkoroma");
//        databaseHelper.insertPlaces(539,43,"Chitego");
//        databaseHelper.insertPlaces(540,43,"Chiwe");
//        databaseHelper.insertPlaces(541,43,"Hogoro");
//        databaseHelper.insertPlaces(542,43,"Iduo");
//        databaseHelper.insertPlaces(543,43,"Kibaigwa");
//        databaseHelper.insertPlaces(544,43,"Kongwa");
//        databaseHelper.insertPlaces(545,43,"Lenjulu");
//        databaseHelper.insertPlaces(546,43,"Makawa");
//        databaseHelper.insertPlaces(547,43,"Matongoro");
//        databaseHelper.insertPlaces(548,43,"Mkoka");
//        databaseHelper.insertPlaces(549,43,"Mlali");
//        databaseHelper.insertPlaces(550,43,"Mtanana");
//        databaseHelper.insertPlaces(551,43,"Nghumbi");
//        databaseHelper.insertPlaces(552,43,"Ngomai");
//        databaseHelper.insertPlaces(553,43,"Njoge");
//        databaseHelper.insertPlaces(554,43,"Pandambili");
//        databaseHelper.insertPlaces(555,43,"Sagara");
//        databaseHelper.insertPlaces(556,43,"Sejeli");
//        databaseHelper.insertPlaces(557,43,"Songambele");
//        databaseHelper.insertPlaces(558,43,"Ugogoni");
//        databaseHelper.insertPlaces(559,43,"Zoissa");
//
//        databaseHelper.insertPlaces(560,44,"Berege");
//        databaseHelper.insertPlaces(561,44,"Chipogoro");
//        databaseHelper.insertPlaces(562,44,"Chitemo");
//        databaseHelper.insertPlaces(563,44,"Chunyu");
//        databaseHelper.insertPlaces(564,44,"Galigali");
//        databaseHelper.insertPlaces(565,44,"Godegode");
//        databaseHelper.insertPlaces(566,44,"Gulwe");
//        databaseHelper.insertPlaces(567,44,"Ipera");
//        databaseHelper.insertPlaces(568,44,"Iwondo");
//        databaseHelper.insertPlaces(569,44,"Kibakwe");
//        databaseHelper.insertPlaces(570,44,"Kimagai");
//        databaseHelper.insertPlaces(571,44,"Kingiti");
//        databaseHelper.insertPlaces(572,44,"Lufu");
//        databaseHelper.insertPlaces(573,44,"Luhundwa");
//        databaseHelper.insertPlaces(574,44,"Lumuma");
//        databaseHelper.insertPlaces(575,44,"Lupeta");
//        databaseHelper.insertPlaces(576,44,"Malolo");
//        databaseHelper.insertPlaces(577,44,"Massa");
//        databaseHelper.insertPlaces(578,44,"Matomondo");
//        databaseHelper.insertPlaces(579,44,"Mazae");
//        databaseHelper.insertPlaces(580,44,"Mbuga");
//        databaseHelper.insertPlaces(581,44,"Mima");
//        databaseHelper.insertPlaces(582,44,"Mlunduzi");
//        databaseHelper.insertPlaces(583,44,"Mpwapwa mjini");
//        databaseHelper.insertPlaces(584,44,"Mtera");
//        databaseHelper.insertPlaces(585,44,"Nghambi");
//        databaseHelper.insertPlaces(586,44,"Pwaga");
//        databaseHelper.insertPlaces(587,44,"Rudi");
//        databaseHelper.insertPlaces(588,44,"Ving'hawe");
//        databaseHelper.insertPlaces(589,44,"Wotta");
//
//        databaseHelper.insertPlaces(590,45,"Bugerenga");
//        databaseHelper.insertPlaces(591,45,"Bukombe");
//        databaseHelper.insertPlaces(592,45,"Busonzo");
//        databaseHelper.insertPlaces(593,45,"Butinzya");
//        databaseHelper.insertPlaces(594,45,"igulwa");
//        databaseHelper.insertPlaces(595,45,"Iyogelo");
//        databaseHelper.insertPlaces(596,45,"Lyambamgongo");
//        databaseHelper.insertPlaces(597,45,"Namonge");
//        databaseHelper.insertPlaces(598,45,"Ng'anzo");
//        databaseHelper.insertPlaces(599,45,"Runzewe Magharibi");
//        databaseHelper.insertPlaces(600,45,"Runzewe Mashariki");
//        databaseHelper.insertPlaces(601,45,"Ushirombo");
//        databaseHelper.insertPlaces(602,45,"Uyovu");
//
//        databaseHelper.insertPlaces(603,46,"Bukome");
//        databaseHelper.insertPlaces(604,46,"Buseresere");
//        databaseHelper.insertPlaces(605,46,"Butengorumasa");
//        databaseHelper.insertPlaces(606,46,"Buziku");
//        databaseHelper.insertPlaces(607,46,"Bwanga");
//        databaseHelper.insertPlaces(608,46,"Bwera");
//        databaseHelper.insertPlaces(609,46,"Bwina");
//        databaseHelper.insertPlaces(610,46,"Bwongera");
//        databaseHelper.insertPlaces(611,46,"Chato");
//        databaseHelper.insertPlaces(612,46,"Ichwankima");
//        databaseHelper.insertPlaces(613,46,"Ilemela(chato)");
//        databaseHelper.insertPlaces(614,46,"Ilyamchele");
//        databaseHelper.insertPlaces(615,46,"Iparamasa");
//        databaseHelper.insertPlaces(616,46,"Lachwamba");
//        databaseHelper.insertPlaces(617,46,"Kasenga");
//        databaseHelper.insertPlaces(618,46,"Katende");
//        databaseHelper.insertPlaces(619,46,"Kigongo");
//        databaseHelper.insertPlaces(620,46,"Makurugusi");
//        databaseHelper.insertPlaces(621,46,"Muganza");
//        databaseHelper.insertPlaces(622,46,"Muungano");
//        databaseHelper.insertPlaces(623,46,"Nyamirembe");
//        databaseHelper.insertPlaces(624,46,"Nyarutembo");
//
//        databaseHelper.insertPlaces(625,47,"Bugalama");
//        databaseHelper.insertPlaces(626,47,"Bugulula ");
//        databaseHelper.insertPlaces(627,47,"Buhalahala");
//        databaseHelper.insertPlaces(628,47,"Bujula");
//        databaseHelper.insertPlaces(629,47,"Bukoli ");
//        databaseHelper.insertPlaces(630,47,"Bukondo");
//        databaseHelper.insertPlaces(631,47,"Bulela");
//        databaseHelper.insertPlaces(632,47,"Bung'wangoko");
//        databaseHelper.insertPlaces(633,47,"Busanda");
//        databaseHelper.insertPlaces(634,47,"Butobela");
//        databaseHelper.insertPlaces(635,47,"Butundwe");
//        databaseHelper.insertPlaces(636,47,"Chigunga");
//        databaseHelper.insertPlaces(637,47,"Ihanamilo ");
//        databaseHelper.insertPlaces(638,47,"Isulwabutundwe");
//        databaseHelper.insertPlaces(639,47,"Izumacheli ");
//        databaseHelper.insertPlaces(640,47,"Kagu ");
//        databaseHelper.insertPlaces(641,47,"Kakubilo");
//        databaseHelper.insertPlaces(642,47,"Kalangalala");
//        databaseHelper.insertPlaces(643,47,"Kamena");
//        databaseHelper.insertPlaces(644,47,"Kamhanga");
//        databaseHelper.insertPlaces(645,47,"Kanyala");
//        databaseHelper.insertPlaces(646,47,"Kasamwa ");
//        databaseHelper.insertPlaces(647,47,"Kaseme");
//        databaseHelper.insertPlaces(648,47,"Katoma");
//        databaseHelper.insertPlaces(649,47,"Katoro");
//        databaseHelper.insertPlaces(650,47,"Lubanga");
//        databaseHelper.insertPlaces(651,47,"Ludete ");
//        databaseHelper.insertPlaces(652,47,"Lwamgasa ");
//        databaseHelper.insertPlaces(653,47,"Lwenzera");
//        databaseHelper.insertPlaces(654,47,"Magenge ");
//        databaseHelper.insertPlaces(655,47,"Mgusu");
//        databaseHelper.insertPlaces(656,47,"Mtakuja");
//        databaseHelper.insertPlaces(657,47,"Nkome");
//        databaseHelper.insertPlaces(658,47,"Nyachiluluma");
//        databaseHelper.insertPlaces(659,47,"Nyakagomba");
//        databaseHelper.insertPlaces(660,47,"Nyakamwaga ");
//        databaseHelper.insertPlaces(661,47,"Nyalwanzaja");
//        databaseHelper.insertPlaces(662,47,"Nyamalimbe ");
//        databaseHelper.insertPlaces(663,47,"Nyamboge");
//        databaseHelper.insertPlaces(664,47,"Nyamigota");
//        databaseHelper.insertPlaces(665,47,"Nyamwilolelwa");
//        databaseHelper.insertPlaces(666,47,"Nyanguku");
//        databaseHelper.insertPlaces(667,47,"Nyankumbu");
//        databaseHelper.insertPlaces(668,47,"Nyarugusu ");
//        databaseHelper.insertPlaces(669,47,"Nyaruyeye");
//        databaseHelper.insertPlaces(670,47,"Nyawilimilwa");
//        databaseHelper.insertPlaces(671,47,"Nzera");
//        databaseHelper.insertPlaces(672,47,"Senga");
//        databaseHelper.insertPlaces(673,47,"Shiloleli");
//
//        databaseHelper.insertPlaces(674,48,"Bukandwe");
//        databaseHelper.insertPlaces(675,48,"Ikobe");
//        databaseHelper.insertPlaces(676,48,"Ikunguigazi");
//        databaseHelper.insertPlaces(677,48,"Ilolangulu");
//        databaseHelper.insertPlaces(678,48,"Iponya");
//        databaseHelper.insertPlaces(679,48,"Isebya");
//        databaseHelper.insertPlaces(680,48," Lugunga");
//        databaseHelper.insertPlaces(681,48,"Lulembela");
//        databaseHelper.insertPlaces(682,48,"Masumbwe");
//        databaseHelper.insertPlaces(683,48,"Mbogwe");
//        databaseHelper.insertPlaces(684,48,"Nanda");
//        databaseHelper.insertPlaces(685,48,"Ngemo");
//        databaseHelper.insertPlaces(686,48,"Ng'homolwa");
//        databaseHelper.insertPlaces(687,48," Nyakafulu");
//        databaseHelper.insertPlaces(688,48,"Nyasato");
//        databaseHelper.insertPlaces(689,48," Ushirika");
//
//        databaseHelper.insertPlaces(690,49,"Bukwimba");
//        databaseHelper.insertPlaces(691,49,"Busolwa");
//        databaseHelper.insertPlaces(692,49,"Izunya ");
//        databaseHelper.insertPlaces(693,49,"Kafita ");
//        databaseHelper.insertPlaces(694,49,"Kakora");
//        databaseHelper.insertPlaces(695,49,"Kharumwa");
//        databaseHelper.insertPlaces(696,49,"Mwingiro");
//        databaseHelper.insertPlaces(697,49,"Nyabulanda");
//        databaseHelper.insertPlaces(698,49,"Nyang'hwale ");
//        databaseHelper.insertPlaces(699,49,"Nyijundu");
//        databaseHelper.insertPlaces(700,49,"Nyugwa");
//        databaseHelper.insertPlaces(701,49,"Shabaka");

//
//        databaseHelper.insertPlaces(702,50,"Gangilonga ");
//        databaseHelper.insertPlaces(703,50,"Ilala");
//        databaseHelper.insertPlaces(704,50,"Isakalilo ");
//        databaseHelper.insertPlaces(705,50,"Kihesa ");
//        databaseHelper.insertPlaces(706,50,"Kitanzini");
//        databaseHelper.insertPlaces(707,50,"Kitwiru ");
//        databaseHelper.insertPlaces(708,50,"Kwakilosa ");
//        databaseHelper.insertPlaces(709,50,"Makorongoni ");
//        databaseHelper.insertPlaces(710,50,"Mivinjeni ");
//        databaseHelper.insertPlaces(711,50,"Mkwawa");
//        databaseHelper.insertPlaces(712,50," Mlandege ");
//        databaseHelper.insertPlaces(713,50,"Mshindo ");
//        databaseHelper.insertPlaces(714,50,"Mtwivila ");
//        databaseHelper.insertPlaces(715,50,"Mwangata ");
//        databaseHelper.insertPlaces(716,50,"Nduli (Iringa mjini) ");
//        databaseHelper.insertPlaces(717,50," Ruaha");
//
//        databaseHelper.insertPlaces(718,51,"Idodi");
//        databaseHelper.insertPlaces(719,51," Ifunda ");
//        databaseHelper.insertPlaces(720,51," Ilolo Mpya");
//        databaseHelper.insertPlaces(721,51,"Itunundu");
//        databaseHelper.insertPlaces(722,51,"Izazi");
//        databaseHelper.insertPlaces(723,51,"Kalenga ");
//        databaseHelper.insertPlaces(724,51,"Kihorogota");
//        databaseHelper.insertPlaces(725,51,"Kiwere ");
//        databaseHelper.insertPlaces(726,51,"Luhota");
//        databaseHelper.insertPlaces(727,51,"Lumuli ");
//        databaseHelper.insertPlaces(728,51,"Lyamgungwe");
//        databaseHelper.insertPlaces(729,51,"Maboga");
//        databaseHelper.insertPlaces(730,51,"Maguliwa");
//        databaseHelper.insertPlaces(731,51,"Mahuninga");
//        databaseHelper.insertPlaces(732,51,"Malenga Makali ");
//        databaseHelper.insertPlaces(733,51,"Mgama ");
//        databaseHelper.insertPlaces(734,51,"Migoli");
//        databaseHelper.insertPlaces(735,51,"Mlenge ");
//        databaseHelper.insertPlaces(736,51,"Mlowa ");
//        databaseHelper.insertPlaces(737,51,"Mseke");
//        databaseHelper.insertPlaces(738,51,"Nduli");
//        databaseHelper.insertPlaces(739,51,"Nyang'oro");
//        databaseHelper.insertPlaces(740,51,"Nzihi ");
//        databaseHelper.insertPlaces(741,51,"Ulanda ");
//        databaseHelper.insertPlaces(742,51,"Wasa");
//
//        databaseHelper.insertPlaces(743,52,"Boma la Ng'ombe");
//        databaseHelper.insertPlaces(744,52,"Dabaga");
//        databaseHelper.insertPlaces(745,52," Ibumu");
//        databaseHelper.insertPlaces(746,52,"Idete");
//        databaseHelper.insertPlaces(747,52,"Ihimbo");
//        databaseHelper.insertPlaces(748,52," Ilula");
//        databaseHelper.insertPlaces(749,52," Image ");
//        databaseHelper.insertPlaces(750,52," Irole ");
//        databaseHelper.insertPlaces(751,52,"Kimala ");
//        databaseHelper.insertPlaces(752,52,"Lugalo ");
//        databaseHelper.insertPlaces(753,52," Mahenge");
//        databaseHelper.insertPlaces(754,52,"Masisiwe");
//        databaseHelper.insertPlaces(755,52,"Mlafu ");
//        databaseHelper.insertPlaces(756,52,"Mtitu ");
//        databaseHelper.insertPlaces(757,52,"Ng'ang'ange");
//        databaseHelper.insertPlaces(758,52,"Ng'uruhe ");
//        databaseHelper.insertPlaces(759,52,"Nyalumbu");
//        databaseHelper.insertPlaces(760,52,"Ruaha Mbuyuni");
//        databaseHelper.insertPlaces(761,52,"Udekwa");
//        databaseHelper.insertPlaces(762,52,"Uhambingeto ");
//        databaseHelper.insertPlaces(763,52,"Ukumbi");
//        databaseHelper.insertPlaces(764,52,"Ukwega");
//
//        databaseHelper.insertPlaces(765,53,"Boma (Mafinga) ");
//        databaseHelper.insertPlaces(766,53,"Isalavanu ");
//        databaseHelper.insertPlaces(767,53,"Kinyanambo");
//        databaseHelper.insertPlaces(768,53,"Sao Hill ");
//
//        databaseHelper.insertPlaces(769,54,"Meru");
//        databaseHelper.insertPlaces(770,54,"Idunda");
//        databaseHelper.insertPlaces(771,54,"Ifwagi ");
//        databaseHelper.insertPlaces(772,54,"Igombavanu");
//        databaseHelper.insertPlaces(773,54,"gowole ");
//        databaseHelper.insertPlaces(774,54,"Ihalimba ");
//        databaseHelper.insertPlaces(775,54,"Ihanu ");
//        databaseHelper.insertPlaces(776,54,"Ihowanza");
//        databaseHelper.insertPlaces(777,54,"Ikweha ");
//        databaseHelper.insertPlaces(778,54,"Isalavanu ");
//        databaseHelper.insertPlaces(779,54,"Itandula ");
//        databaseHelper.insertPlaces(780,54,"Kasanga");
//        databaseHelper.insertPlaces(781,54,"Kibengu");
//        databaseHelper.insertPlaces(782,54," Kiyowela ");
//        databaseHelper.insertPlaces(783,54,"Luhunga ");
//        databaseHelper.insertPlaces(784,54,"Makungu");
//        databaseHelper.insertPlaces(785,54,"Malangali");
//        databaseHelper.insertPlaces(786,54,"Mapanda ");
//        databaseHelper.insertPlaces(787,54,"Mbalamaziwa");
//        databaseHelper.insertPlaces(788,54,"Mdabulo ");
//        databaseHelper.insertPlaces(789,54," Mninga (Mufindi)");
//        databaseHelper.insertPlaces(790,54," Mpanga Tazara");
//        databaseHelper.insertPlaces(791,54," Mtambula");
//        databaseHelper.insertPlaces(792,54," Mtwango");
//        databaseHelper.insertPlaces(793,54," Nyololo");
//        databaseHelper.insertPlaces(794,54," Rungemba");
//        databaseHelper.insertPlaces(795,54," Sadani");
//
//        databaseHelper.insertPlaces(796,55,"Biharamulo Mjini");
//        databaseHelper.insertPlaces(797,55," Bisibo");
//        databaseHelper.insertPlaces(798,55," Kabindi");
//        databaseHelper.insertPlaces(799,55,"Kalenge");
//        databaseHelper.insertPlaces(800,55,"Kaniha");
//        databaseHelper.insertPlaces(801,55," Lusahunga");
//        databaseHelper.insertPlaces(802,55," Nemba");
//        databaseHelper.insertPlaces(803,55,"Nyabusozi");
//        databaseHelper.insertPlaces(804,55,"Nyakahura");
//        databaseHelper.insertPlaces(805,55," Nyamahanga");
//        databaseHelper.insertPlaces(806,55,"Nyamigogo ");
//        databaseHelper.insertPlaces(807,55," Nyantakara");
//        databaseHelper.insertPlaces(808,55,"Nyarubungo");
//        databaseHelper.insertPlaces(809,55,"Runazi");
//        databaseHelper.insertPlaces(810,55," Ruziba");
//
//        databaseHelper.insertPlaces(811,56,"Bakoba");
//        databaseHelper.insertPlaces(812,56,"Bilele ");
//        databaseHelper.insertPlaces(813,56,"Buhembe");
//        databaseHelper.insertPlaces(814,56,"Hamugembe");
//        databaseHelper.insertPlaces(815,56,"Ijuganyondo");
//        databaseHelper.insertPlaces(816,56,"Kagondo");
//        databaseHelper.insertPlaces(817,56,"Kahororo");
//        databaseHelper.insertPlaces(818,56,"Kashai");
//        databaseHelper.insertPlaces(819,56," Kibeta");
//        databaseHelper.insertPlaces(820,56,"Kitendaguro ");
//        databaseHelper.insertPlaces(821,56,"Miembeni ");
//        databaseHelper.insertPlaces(822,56,"Nshambya");
//        databaseHelper.insertPlaces(823,56," Nyanga ");
//        databaseHelper.insertPlaces(824,56,"Rwamishenye");
//
//        databaseHelper.insertPlaces(825,57,"Buhendangabo ");
//        databaseHelper.insertPlaces(826,57,"Bujugo");
//        databaseHelper.insertPlaces(827,57," Butelankuzi");
//        databaseHelper.insertPlaces(828,57," Butulage");
//        databaseHelper.insertPlaces(829,57,"Ibwera");
//        databaseHelper.insertPlaces(830,57,"Izimbya");
//        databaseHelper.insertPlaces(831,57,"Kaagya");
//        databaseHelper.insertPlaces(832,57," Kaibanja");
//        databaseHelper.insertPlaces(833,57,"Kanyangereko");
//        databaseHelper.insertPlaces(834,57,"Karabagaine");
//        databaseHelper.insertPlaces(835,57,"Kasharu");
//        databaseHelper.insertPlaces(836,57," Katerero");
//        databaseHelper.insertPlaces(837,57,"Katoma");
//        databaseHelper.insertPlaces(838,57," Katoro");
//        databaseHelper.insertPlaces(839,57," Kemondo");
//        databaseHelper.insertPlaces(840,57," Kibirizi");
//        databaseHelper.insertPlaces(841,57," Kikomero");
//        databaseHelper.insertPlaces(842,57," Kishanje");
//        databaseHelper.insertPlaces(843,57," Kishogo");
//        databaseHelper.insertPlaces(844,57,"Kyamulaile");
//        databaseHelper.insertPlaces(845,57," Maruku");
//        databaseHelper.insertPlaces(846,57," Mikoni");
//        databaseHelper.insertPlaces(847,57," Mugajwale");
//        databaseHelper.insertPlaces(848,57," Nyakato");
//        databaseHelper.insertPlaces(849,57," Nyakibimbili");
//        databaseHelper.insertPlaces(850,57,"Rubafu");
//        databaseHelper.insertPlaces(851,57," Rubale ");
//        databaseHelper.insertPlaces(853,57," Ruhunga");
//        databaseHelper.insertPlaces(854,57,"Rukoma");
//
//        databaseHelper.insertPlaces(855,58,"Bugene");
//        databaseHelper.insertPlaces(856,58,"Bweranyange");
//        databaseHelper.insertPlaces(857,58,"Chanika");
//        databaseHelper.insertPlaces(859,58," Chonyonyo");
//        databaseHelper.insertPlaces(860,58,"Igurwa ");
//        databaseHelper.insertPlaces(861,58," Iha");
//        databaseHelper.insertPlaces(862,58,"Ihembe");
//        databaseHelper.insertPlaces(863,58,"Kanoni");
//        databaseHelper.insertPlaces(864,58,"Kayanga ");
//        databaseHelper.insertPlaces(865,58,"Kibondo ");
//        databaseHelper.insertPlaces(866,58,"Kihanga");
//        databaseHelper.insertPlaces(867,58,"Kiruruma");
//        databaseHelper.insertPlaces(868,58,"Kituntu");
//        databaseHelper.insertPlaces(869,58,"Ndama ");
//        databaseHelper.insertPlaces(870,58,"Nyabiyonza");
//        databaseHelper.insertPlaces(871,58,"Nyaishozi");
//        databaseHelper.insertPlaces(872,58,"Nyakabanga");
//        databaseHelper.insertPlaces(873,58,"Nyakahanga");
//        databaseHelper.insertPlaces(874,58,"Nyakakika ");
//        databaseHelper.insertPlaces(875,58,"Nyakasimbi");
//        databaseHelper.insertPlaces(876,58," Rugera");
//        databaseHelper.insertPlaces(877,58,"Rugu");
//
//        databaseHelper.insertPlaces(878,59,"Bugomora");
//        databaseHelper.insertPlaces(879,59,"Businde");
//        databaseHelper.insertPlaces(880,59,"Isingiro");
//        databaseHelper.insertPlaces(881,59,"Kaisho ");
//        databaseHelper.insertPlaces(882,59,"Kamuli");
//        databaseHelper.insertPlaces(883,59,"Kibale");
//        databaseHelper.insertPlaces(884,59,"Kibingo");
//        databaseHelper.insertPlaces(885,59,"Kikukuru ");
//        databaseHelper.insertPlaces(886,59,"Kimuli");
//        databaseHelper.insertPlaces(887,59,"Kyerwa");
//        databaseHelper.insertPlaces(888,59,"Mabira");
//        databaseHelper.insertPlaces(889,59,"Murongo");
//        databaseHelper.insertPlaces(890,59,"Nkwenda");
//        databaseHelper.insertPlaces(891,59,"Nyakatuntu");
//        databaseHelper.insertPlaces(892,59,"Rukuraijo");
//        databaseHelper.insertPlaces(893,59,"Rutunguru");
//        databaseHelper.insertPlaces(894,59,"Rwabwere");
//        databaseHelper.insertPlaces(895,59,"Songambele");
//
//        databaseHelper.insertPlaces(896,60,"Biirabo");
//        databaseHelper.insertPlaces(897,60,"Bisheke");
//        databaseHelper.insertPlaces(898,60,"Buganguzi");
//        databaseHelper.insertPlaces(899,60,"Buhangaza");
//        databaseHelper.insertPlaces(900,60,"Bulyakashaju");
//        databaseHelper.insertPlaces(901,60,"Bumbire");
//        databaseHelper.insertPlaces(902,60,"Bureza");
//        databaseHelper.insertPlaces(903,60,"Burungura");
//        databaseHelper.insertPlaces(904,60,"Goziba");
//        databaseHelper.insertPlaces(905,60,"Gwanseli");
//        databaseHelper.insertPlaces(906,60,"Ibuga");
//        databaseHelper.insertPlaces(907,60,"Ijumbi");
//        databaseHelper.insertPlaces(908,60," Ikondo");
//        databaseHelper.insertPlaces(909,60,"Ikuza");
//        databaseHelper.insertPlaces(910,60,"Izigo");
//        databaseHelper.insertPlaces(911,60,"Kabirizi");
//        databaseHelper.insertPlaces(912,60,"Kagoma ");
//        databaseHelper.insertPlaces(913,60," Kamachumu");
//        databaseHelper.insertPlaces(914,60,"Karambi");
//        databaseHelper.insertPlaces(915,60," Kasharunga");
//        databaseHelper.insertPlaces(916,60," Kashasha");
//        databaseHelper.insertPlaces(917,60," Katoke");
//        databaseHelper.insertPlaces(918,60,"Kerebe");
//        databaseHelper.insertPlaces(919,60,"Kibanga");
//        databaseHelper.insertPlaces(920,60," Kikuku");
//        databaseHelper.insertPlaces(921,60,"Kimwani");
//        databaseHelper.insertPlaces(922,60,"Kishanda");
//        databaseHelper.insertPlaces(923,60,"Kyebitembe");
//        databaseHelper.insertPlaces(924,60,"Mafumbo ");
//        databaseHelper.insertPlaces(925,60,"Magata Karutanga ");
//        databaseHelper.insertPlaces(926,60,"Mayondwe");
//        databaseHelper.insertPlaces(927,60," Mazinga Kisiwa ");
//        databaseHelper.insertPlaces(928,60," Mubunda");
//        databaseHelper.insertPlaces(929,60," Muhutwe");
//        databaseHelper.insertPlaces(930,60," Muleba");
//        databaseHelper.insertPlaces(931,60,"Mushabago ");
//        databaseHelper.insertPlaces(932,60," Ngenge");
//        databaseHelper.insertPlaces(933,60," Nshamba");
//        databaseHelper.insertPlaces(934,60," Nyakabango");
//        databaseHelper.insertPlaces(935,60," Nyakatanga");
//        databaseHelper.insertPlaces(936,60," Ruhanga");
//        databaseHelper.insertPlaces(937,60,"Rulanda");
//        databaseHelper.insertPlaces(938,60,"Rutoro");
//
//        databaseHelper.insertPlaces(939,61,"Bugarama");
//        databaseHelper.insertPlaces(940,61," Bukiriro");
//        databaseHelper.insertPlaces(941,61,"Kabanga ");
//        databaseHelper.insertPlaces(942,61,"Kanazi ");
//        databaseHelper.insertPlaces(943,61," Kasulo");
//        databaseHelper.insertPlaces(944,61," Keza");
//        databaseHelper.insertPlaces(945,61," Kibimba");
//        databaseHelper.insertPlaces(946,61,"Kirushya");
//        databaseHelper.insertPlaces(947,61," Mabawe");
//        databaseHelper.insertPlaces(948,61,"Mbuba");
//        databaseHelper.insertPlaces(959,61,"Ngara");
//        databaseHelper.insertPlaces(960,61," Mugoma");
//        databaseHelper.insertPlaces(961,61,"Murukulazo ");
//        databaseHelper.insertPlaces(962,61," Murusagamba");
//        databaseHelper.insertPlaces(963,61," Ngara Mjini");
//        databaseHelper.insertPlaces(964,61," Ntobeye");
//        databaseHelper.insertPlaces(965,61," Nyakisasa ");
//        databaseHelper.insertPlaces(966,61," Nyamiyaga");
//        databaseHelper.insertPlaces(967,61," Rulenge");
//        databaseHelper.insertPlaces(968,61,"Rusumo");
//
//
//        //wilaya ya kagera iliyo sahaulika
//        databaseHelper.insertPlaces(969,6,"Missenyi");
//
//        //kata zake parent id itawekwa mwishoni baada ya kupata id ya wilaya
//        databaseHelper.insertPlaces(970,969,"Bugika");
//        databaseHelper.insertPlaces(971,969,"Bugorora ");
//        databaseHelper.insertPlaces(972,969,"Buyango");
//        databaseHelper.insertPlaces(973,969,"Bwanjai");
//        databaseHelper.insertPlaces(974,969,"Gera ");
//        databaseHelper.insertPlaces(975,969,"Ishozi");
//        databaseHelper.insertPlaces(976,969," Ishunju ");
//        databaseHelper.insertPlaces(977,969,"Kakunyu ");
//        databaseHelper.insertPlaces(978,969,"Kanyigo");
//        databaseHelper.insertPlaces(979,969,"Kashenye");
//        databaseHelper.insertPlaces(980,969,"Kassambya");
//        databaseHelper.insertPlaces(981,969,"Kilimilile");
//        databaseHelper.insertPlaces(983,969,"Kitobo ");
//        databaseHelper.insertPlaces(984,969,"Kyaka");
//        databaseHelper.insertPlaces(985,969,"Mabale ");
//        databaseHelper.insertPlaces(986,969,"Minziro");
//        databaseHelper.insertPlaces(987,969,"Mushasha ");
//        databaseHelper.insertPlaces(988,969,"Mutukula");
//        databaseHelper.insertPlaces(989,969," Nsunga");
//        databaseHelper.insertPlaces(989,969,"Ruzinga");
//
//
////      //***************************************************************8
//
//        databaseHelper.insertPlaces(990,62,"Ikuba");
//        databaseHelper.insertPlaces(991,62,"Ilela");
//        databaseHelper.insertPlaces(992,62," Ilunde");
//        databaseHelper.insertPlaces(993,62,"Inyonga");
//        databaseHelper.insertPlaces(994,62,"Itenka");
//        databaseHelper.insertPlaces(995,62,"Kapalala");
//        databaseHelper.insertPlaces(996,62,"Kasansa ");
//        databaseHelper.insertPlaces(997,62,"Kasokola");
//        databaseHelper.insertPlaces(998,62,"Kibaoni");
//        databaseHelper.insertPlaces(999,62,"Litapunga");
//        databaseHelper.insertPlaces(1000,62,"Machimboni");
//        databaseHelper.insertPlaces(1001,62,"Magamba");
//        databaseHelper.insertPlaces(1002,62,"Majimoto");
//        databaseHelper.insertPlaces(1003,62,"Mamba");
//        databaseHelper.insertPlaces(1004,62,"Mbede ");
//        databaseHelper.insertPlaces(1005,62,"Mtapenda ");
//        databaseHelper.insertPlaces(1006,62,"Mwamapuli ");
//        databaseHelper.insertPlaces(1007,62,"Nsenkwa");
//        databaseHelper.insertPlaces(1008,62,"Nsimbo ");
//        databaseHelper.insertPlaces(1009,62,"Sitalike ");
//        databaseHelper.insertPlaces(1010,62,"Ugala");
//        databaseHelper.insertPlaces(1011,62,"Urwila ");
//        databaseHelper.insertPlaces(1012,62,"Usevya");
//        databaseHelper.insertPlaces(1013,62,"Utende");
//
//
//        databaseHelper.insertPlaces(1014,63,"Ikola ");
//        databaseHelper.insertPlaces(1015,63,"Ilela");
//        databaseHelper.insertPlaces(1016,63,"Ilembo");
//        databaseHelper.insertPlaces(1017,63,"Ilunde");
//        databaseHelper.insertPlaces(1018,63,"Inyonga");
//        databaseHelper.insertPlaces(1019,63,"Kabungu");
//        databaseHelper.insertPlaces(1020,63,"Karema ");
//        databaseHelper.insertPlaces(1021,63,"Kashaulili");
//        databaseHelper.insertPlaces(1022,63,"Kasokola ");
//        databaseHelper.insertPlaces(1023,63,"Katuma");
//        databaseHelper.insertPlaces(1024,63,"Katumba ");
//        databaseHelper.insertPlaces(1025,63,"Kawajense ");
//        databaseHelper.insertPlaces(1026,63,"Kibaoni");
//        databaseHelper.insertPlaces(1027,63,"Machimboni");
//        databaseHelper.insertPlaces(1028,63,"Magamba");
//        databaseHelper.insertPlaces(1029,63,"Mamba");
//        databaseHelper.insertPlaces(1030,63,"Mbede ");
//        databaseHelper.insertPlaces(1031,63,"Mishamo ");
//        databaseHelper.insertPlaces(1032,63," Misunkumilo");
//        databaseHelper.insertPlaces(1033,63,"Mpanda Ndogo");
//        databaseHelper.insertPlaces(1034,63,"Mtapenda ");
//        databaseHelper.insertPlaces(1035,63,"Mwese");
//        databaseHelper.insertPlaces(1036,63,"Nsemulwa");
//        databaseHelper.insertPlaces(1037,63,"Nsimbo");
//        databaseHelper.insertPlaces(1038,63,"Shanwe");
//        databaseHelper.insertPlaces(1039,63,"Sitalike");
//        databaseHelper.insertPlaces(1040,63,"Ugalla");
//        databaseHelper.insertPlaces(1041,63,"Urwira");
//        databaseHelper.insertPlaces(1042,63,"Usevya ");
//        databaseHelper.insertPlaces(1043,63,"Utende");
//
//
//        databaseHelper.insertPlaces(1044,64,"Ikola");
//        databaseHelper.insertPlaces(1045,64,"Kabungu");
//        databaseHelper.insertPlaces(1046,64,"Kapalamsenga");
//        databaseHelper.insertPlaces(1047,64,"Karema");
//        databaseHelper.insertPlaces(1048,64,"Katuma");
//        databaseHelper.insertPlaces(1049,64,"Mishamo");
//        databaseHelper.insertPlaces(1050,64,"Mpandandogo");
//        databaseHelper.insertPlaces(1051,64,"Mwese");
//        databaseHelper.insertPlaces(1052,64,"Sibwesa");
//
//
//        databaseHelper.insertPlaces(1053,65,"Biharu");
//        databaseHelper.insertPlaces(1054,65,"Buhigwe");
//        databaseHelper.insertPlaces(1055,65,"Kajana");
//        databaseHelper.insertPlaces(1056,65,"Kibande ");
//        databaseHelper.insertPlaces(1057,65,"Kilelema");
//        databaseHelper.insertPlaces(1058,65,"Mugera");
//        databaseHelper.insertPlaces(1059,65,"Muhinda");
//        databaseHelper.insertPlaces(1060,65,"Munanila ");
//        databaseHelper.insertPlaces(1061,65,"Munyegera");
//        databaseHelper.insertPlaces(1062,65,"Munzeze");
//        databaseHelper.insertPlaces(1063,65,"Muyama ");
//        databaseHelper.insertPlaces(1064,65,"Mwayaya");
//        databaseHelper.insertPlaces(1065,65,"Nyamugali");
//        databaseHelper.insertPlaces(1066,65,"Rusaba");
//
//        databaseHelper.insertPlaces(1067,66,"Gwanumpu");
//        databaseHelper.insertPlaces(1068,66,"Kakonko");
//        databaseHelper.insertPlaces(1069,66,"Kasanda");
//        databaseHelper.insertPlaces(1070,66,"Kasuga");
//        databaseHelper.insertPlaces(1071,66,"Katanga");
//        databaseHelper.insertPlaces(1072,66,"Kiziguzigu");
//        databaseHelper.insertPlaces(1073,66,"Mugunzu");
//        databaseHelper.insertPlaces(1074,66,"Muhange");
//        databaseHelper.insertPlaces(1075,66,"Nyabibuye");
//        databaseHelper.insertPlaces(1076,66,"Nyamtukuza");
//        databaseHelper.insertPlaces(1077,66,"Rugenge");
//
//        databaseHelper.insertPlaces(1078,67,"Buhigwe");
//        databaseHelper.insertPlaces(1079,67,"Buhoro ");
//        databaseHelper.insertPlaces(1080,67,"Heru Ushingo");
//        databaseHelper.insertPlaces(1081,67," Janda");
//        databaseHelper.insertPlaces(1082,67,"Kagera Nkanda ");
//        databaseHelper.insertPlaces(1083,67,"Kajana ");
//        databaseHelper.insertPlaces(1084,67,"Meru");
//        databaseHelper.insertPlaces(1085,67,"Kigondo ");
//        databaseHelper.insertPlaces(1086,67," Kilelema");
//        databaseHelper.insertPlaces(1087,67,"Kitagata ");
//        databaseHelper.insertPlaces(1088,67,"Kitanga");
//        databaseHelper.insertPlaces(1089,67,"Kwaga");
//        databaseHelper.insertPlaces(1090,67,"Msambara");
//        databaseHelper.insertPlaces(1091,67,"Muhinda");
//        databaseHelper.insertPlaces(1092,67,"Muhunga");
//        databaseHelper.insertPlaces(1093,67,"Munanila");
//        databaseHelper.insertPlaces(1094,67,"Munyegera");
//        databaseHelper.insertPlaces(1095,67,"Murufiti");
//        databaseHelper.insertPlaces(1096,67,"Muyama ");
//        databaseHelper.insertPlaces(1097,67,"Muzenze ");
//        databaseHelper.insertPlaces(1098,67,"Muzye ");
//        databaseHelper.insertPlaces(1099,67,"Nyakitonto ");
//        databaseHelper.insertPlaces(1100,67,"Nyamidaho");
//        databaseHelper.insertPlaces(1101,67,"Nyamnyusi ");
//        databaseHelper.insertPlaces(1102,67," Nyamugali ");
//        databaseHelper.insertPlaces(1103,67,"Ruhita");
//        databaseHelper.insertPlaces(1104,67," Rungwe Mpya ");
//        databaseHelper.insertPlaces(1105,67," Rusaba");
//        databaseHelper.insertPlaces(1106,67,"Rusesa");
//        databaseHelper.insertPlaces(1107,67,"Titye");
//
//        databaseHelper.insertPlaces(1108,68,"Kasulu Mjini");
//        databaseHelper.insertPlaces(1109,68," Kigondo");
//        databaseHelper.insertPlaces(1110,68,"Msambara");
//        databaseHelper.insertPlaces(1111,68,"Muganza");
//        databaseHelper.insertPlaces(1112,68,"Muhunga");
//        databaseHelper.insertPlaces(1113,68,"Murufiti");
//        databaseHelper.insertPlaces(1114,68,"Nyansha ");
//        databaseHelper.insertPlaces(1115,68,"Nyumbigwa");
//        databaseHelper.insertPlaces(1116,68,"Ruhita");
//
//        databaseHelper.insertPlaces(1117,69,"Bitare");
//        databaseHelper.insertPlaces(1118,69," Bunyanbo");
//        databaseHelper.insertPlaces(1119,69,"Busagara ");
//        databaseHelper.insertPlaces(1120,69,"Busunzu");
//        databaseHelper.insertPlaces(1121,69,"Itaba");
//        databaseHelper.insertPlaces(1122,69,"Kibondo");
//        databaseHelper.insertPlaces(1123,69,"Kitahana");
//        databaseHelper.insertPlaces(1124,69,"Kizazi");
//        databaseHelper.insertPlaces(1125,69,"Kumsenga ");
//        databaseHelper.insertPlaces(1126,69,"Mabamba ");
//        databaseHelper.insertPlaces(1127,69,"Misezero");
//        databaseHelper.insertPlaces(1128,69,"Murungu ");
//
//        databaseHelper.insertPlaces(1129,70,"Bitale");
//        databaseHelper.insertPlaces(1130,70,"Kagongo");
//        databaseHelper.insertPlaces(1131,70,"Kagunga ");
//        databaseHelper.insertPlaces(1132,70,"Kalinzi");
//        databaseHelper.insertPlaces(1133,70," Mahembe ");
//        databaseHelper.insertPlaces(1134,70,"Matendo ");
//        databaseHelper.insertPlaces(1135,70,"Mkigo ");
//        databaseHelper.insertPlaces(1136,70,"Mkongoro");
//        databaseHelper.insertPlaces(1137,70,"Mngonya ");
//        databaseHelper.insertPlaces(1138,70,"Mwamgongo");
//        databaseHelper.insertPlaces(1139,70,"Mwandiga");
//
//        databaseHelper.insertPlaces(1140,71,"Bangwe");
//        databaseHelper.insertPlaces(1141,71,"Buhanda");
//        databaseHelper.insertPlaces(1142,71," Businde");
//        databaseHelper.insertPlaces(1143,71,"Buzebazeba");
//        databaseHelper.insertPlaces(1144,71,"Gungu");
//        databaseHelper.insertPlaces(1145,71,"Kagera (Kigoma) ");
//        databaseHelper.insertPlaces(1146,71,"Kasimbu ");
//        databaseHelper.insertPlaces(1147,71,"Kasingirima");
//        databaseHelper.insertPlaces(1148,71,"Katubuka ");
//        databaseHelper.insertPlaces(1149,71,"Kibirizi");
//        databaseHelper.insertPlaces(1150,71,"Kigoma ");
//        databaseHelper.insertPlaces(1151,71,"Kipampa");
//        databaseHelper.insertPlaces(1152,71,"Kitongoni ");
//        databaseHelper.insertPlaces(1153,71," Machinjioni (Kigoma) ");
//        databaseHelper.insertPlaces(1154,71,"Majengo (Kigoma) ");
//        databaseHelper.insertPlaces(1155,71," Mwanga Kaskazini");
//        databaseHelper.insertPlaces(1156,71,"Mwanga Kusini");
//        databaseHelper.insertPlaces(1157,71,"Rubuga");
//        databaseHelper.insertPlaces(1158,71,"Rusimbi");
//
//        databaseHelper.insertPlaces(1159,72,"Buhingu");
//        databaseHelper.insertPlaces(1160,72,"Igalula");
//        databaseHelper.insertPlaces(1161,72,"Ilagala ");
//        databaseHelper.insertPlaces(1162,72,"Itebula ");
//        databaseHelper.insertPlaces(1163,72,"Kalya");
//        databaseHelper.insertPlaces(1164,72,"Kandaga");
//        databaseHelper.insertPlaces(1165,72,"Kazuramimba");
//        databaseHelper.insertPlaces(1166,72,"Mganza ");
//        databaseHelper.insertPlaces(1167,72,"Mtego wa Noti ");
//        databaseHelper.insertPlaces(1168,72," Nguruka");
//        databaseHelper.insertPlaces(1169,72,"Sigunga");
//        databaseHelper.insertPlaces(1170,72,"Simbo");
//        databaseHelper.insertPlaces(1171,72,"Sunuka");
//        databaseHelper.insertPlaces(1172,72,"Uvinza");
//
//        databaseHelper.insertPlaces(1173,73,"Hai Mjini ");
//        databaseHelper.insertPlaces(1174,73,"KIA ");
//        databaseHelper.insertPlaces(1175,73,"Machame Kaskazini");
//        databaseHelper.insertPlaces(1176,73,"Machame Kusini ");
//        databaseHelper.insertPlaces(1177,73,"Machame Magharibi ");
//        databaseHelper.insertPlaces(1178,73,"Machame Mashariki");
//        databaseHelper.insertPlaces(1179,73,"Machame Narumu");
//        databaseHelper.insertPlaces(1180,73,"Machame Uroki");
//        databaseHelper.insertPlaces(1181,73,"Machame Weruweru");
//        databaseHelper.insertPlaces(1182,73,"Masama Kati ");
//        databaseHelper.insertPlaces(1183,73,"Masama Kusini");
//        databaseHelper.insertPlaces(1184,73,"Masama Magharibi ");
//        databaseHelper.insertPlaces(1185,73,"Masama Mashariki");
//        databaseHelper.insertPlaces(1186,73,"Masama Rundugai");
//
//        databaseHelper.insertPlaces(1187,74,"Boma Mbuzi");
//        databaseHelper.insertPlaces(1188,74,"Bondeni");
//        databaseHelper.insertPlaces(1189,74,"Kaloleni");
//        databaseHelper.insertPlaces(1190,74,"Karanga");
//        databaseHelper.insertPlaces(1191,74,"Kiboriloni");
//        databaseHelper.insertPlaces(1192,74,"Kilimanjaro");
//        databaseHelper.insertPlaces(1193,74,"Kiusa");
//        databaseHelper.insertPlaces(1194,74,"Korongoni");
//        databaseHelper.insertPlaces(1195,74,"Longuo B ");
//        databaseHelper.insertPlaces(1196,74,"Majengo");
//        databaseHelper.insertPlaces(1197,74,"Mawenzi");
//        databaseHelper.insertPlaces(1198,74,"Mfumuni");
//        databaseHelper.insertPlaces(1199,74,"Miembeni");
//        databaseHelper.insertPlaces(1200,74,"Mji Mpya ");
//        databaseHelper.insertPlaces(1201,74,"Msaranga");
//        databaseHelper.insertPlaces(1202,74,"Ng'ambo ");
//        databaseHelper.insertPlaces(1203,74,"Njoro");
//        databaseHelper.insertPlaces(1204,74,"Pasua");
//        databaseHelper.insertPlaces(1205,74,"Rau");
//        databaseHelper.insertPlaces(1206,74,"Shirimatunda ");
//        databaseHelper.insertPlaces(1207,74,"Soweto");
//
//        databaseHelper.insertPlaces(1208,75,"Arusha Chini ");
//        databaseHelper.insertPlaces(1209,75,"Kahe");
//        databaseHelper.insertPlaces(1210,75,"Kahe Mashariki");
//        databaseHelper.insertPlaces(1211,75,"Kibosho Kati ");
//        databaseHelper.insertPlaces(1212,75,"Kibosho Magharibi ");
//        databaseHelper.insertPlaces(1213,75,"Kibosho Mashariki");
//        databaseHelper.insertPlaces(1214,75,"Kilema Kaskazini ");
//        databaseHelper.insertPlaces(1215,75,"Kilema Kati ");
//        databaseHelper.insertPlaces(1216,75,"Kilema Kusini");
//        databaseHelper.insertPlaces(1217,75,"Kimochi");
//        databaseHelper.insertPlaces(1218,75," Kindi ");
//        databaseHelper.insertPlaces(1219,75," Kirima");
//        databaseHelper.insertPlaces(1220,75,"Kirua Vunjo Magharibi");
//        databaseHelper.insertPlaces(1221,75," Kirua Vunjo Mashariki");
//        databaseHelper.insertPlaces(1222,75," Kirua Vunjo Kusini");
//        databaseHelper.insertPlaces(1223,75," Mabogini");
//        databaseHelper.insertPlaces(1224,75,"Makuyuni");
//        databaseHelper.insertPlaces(1225,75," Mamba Kaskazini");
//        databaseHelper.insertPlaces(1226,75,"Mamba Kusini");
//        databaseHelper.insertPlaces(1227,75," Marangu Magharibi");
//        databaseHelper.insertPlaces(1228,75," Marangu Mashariki ");
//        databaseHelper.insertPlaces(1229,75," Mbokomu");
//        databaseHelper.insertPlaces(1230,75,"Mwika Kaskazini ");
//        databaseHelper.insertPlaces(1231,75,"Mwika Kusini ");
//        databaseHelper.insertPlaces(1232,75," Okaoni ");
//        databaseHelper.insertPlaces(1233,75," Old Moshi Magharibi");
//        databaseHelper.insertPlaces(1234,75," Old Moshi Mashariki");
//        databaseHelper.insertPlaces(1235,75," Uru Kaskazini");
//        databaseHelper.insertPlaces(1236,75," Uru Kusini");
//        databaseHelper.insertPlaces(1237,75," Uru Mashariki");
//        databaseHelper.insertPlaces(1238,75,"Uru Shimbwe");
//
//        databaseHelper.insertPlaces(1239,76,"Chomvu");
//        databaseHelper.insertPlaces(1240,76,"Jipe");
//        databaseHelper.insertPlaces(1241,76,"Kifula");
//        databaseHelper.insertPlaces(1242,76,"Kighare");
//        databaseHelper.insertPlaces(1243,76,"Kigonigoni");
//        databaseHelper.insertPlaces(1244,76,"Kileo ");
//        databaseHelper.insertPlaces(1245,76,"Kilomeni");
//        databaseHelper.insertPlaces(1246,76,"Kirongwe ");
//        databaseHelper.insertPlaces(1247,76,"Kirya ");
//        databaseHelper.insertPlaces(1248,76,"Kivisini ");
//        databaseHelper.insertPlaces(1249,76,"Kwakoa");
//        databaseHelper.insertPlaces(1250,76,"Lang'ata ");
//        databaseHelper.insertPlaces(1251,76,"Lembeni");
//        databaseHelper.insertPlaces(1252,76," Mgagao");
//        databaseHelper.insertPlaces(1253,76,"Msangeni");
//        databaseHelper.insertPlaces(1254,76,"Mwanga ");
//        databaseHelper.insertPlaces(1255,76,"Mwaniko ");
//        databaseHelper.insertPlaces(1256,76,"Ngujini ");
//        databaseHelper.insertPlaces(1257,76,"Shighatini");
//        databaseHelper.insertPlaces(1258,76,"Toloha");
//
//        databaseHelper.insertPlaces(1259,77,"Aleni ");
//        databaseHelper.insertPlaces(1260,77,"Holili");
//        databaseHelper.insertPlaces(1261,77,"Katangara Mrere");
//        databaseHelper.insertPlaces(1262,77,"Kelamfua Mokala");
//        databaseHelper.insertPlaces(1263,77,"Keni Mengeni");
//        databaseHelper.insertPlaces(1264,77,"Kirongo Samanga");
//        databaseHelper.insertPlaces(1265,77,"Kirwa Keni ");
//        databaseHelper.insertPlaces(1266,77,"Kisale Msaranga");
//        databaseHelper.insertPlaces(1267,77,"Kitirima Kingachi ");
//        databaseHelper.insertPlaces(1268,77,"Mahida");
//        databaseHelper.insertPlaces(1269,77,"Makiidi");
//        databaseHelper.insertPlaces(1270,77,"Mamsera ");
//        databaseHelper.insertPlaces(1271,77,"Manda ");
//        databaseHelper.insertPlaces(1272,77,"Marangu Kitowo");
//        databaseHelper.insertPlaces(1273,77,"Mengwe");
//        databaseHelper.insertPlaces(1274,77,"Motamburu Kitendeni");
//        databaseHelper.insertPlaces(1275,77,"Mrao Keryo");
//        databaseHelper.insertPlaces(1276,77,"Nanjala Reha");
//        databaseHelper.insertPlaces(1277,77,"Ngoyoni");
//        databaseHelper.insertPlaces(1278,77,"Olele ");
//        databaseHelper.insertPlaces(1279,77,"Shimbi ");
//        databaseHelper.insertPlaces(1280,77,"Tarakea Motamburu");
//        databaseHelper.insertPlaces(1281,77,"Ubetu Kahe");
//        databaseHelper.insertPlaces(1282,77,"Ushiri Ikuini");
//
//        databaseHelper.insertPlaces(1283,78,"Bendera");
//        databaseHelper.insertPlaces(1284,78,"Bombo");
//        databaseHelper.insertPlaces(1285,78,"Bwambo");
//        databaseHelper.insertPlaces(1286,78,"Chome");
//        databaseHelper.insertPlaces(1287,78,"Hedaru");
//        databaseHelper.insertPlaces(1288,78,"Kalemawe");
//        databaseHelper.insertPlaces(1289,78,"Kihurio");
//        databaseHelper.insertPlaces(1290,78,"Kirangare");
//        databaseHelper.insertPlaces(1291,78,"Kisima");
//        databaseHelper.insertPlaces(1292,78,"Kisiwani");
//        databaseHelper.insertPlaces(1293,78,"Lugulu");
//        databaseHelper.insertPlaces(1294,78,"Mabilioni");
//        databaseHelper.insertPlaces(1295,78,"Makanya");
//        databaseHelper.insertPlaces(1296,78,"Maore");
//        databaseHelper.insertPlaces(1297,78,"Mhezi");
//        databaseHelper.insertPlaces(1298,78,"Mpinji");
//        databaseHelper.insertPlaces(1299,78,"Mshewa");
//        databaseHelper.insertPlaces(1300,78,"Msindo");
//        databaseHelper.insertPlaces(1301,78,"Mtii");
//        databaseHelper.insertPlaces(1302,78,"Mwembe");
//        databaseHelper.insertPlaces(1303,78,"Myamba");
//        databaseHelper.insertPlaces(1304,78,"Ndungu");
//        databaseHelper.insertPlaces(1305,78,"Njoro");
//        databaseHelper.insertPlaces(1306,78,"Ruvu");
//        databaseHelper.insertPlaces(1307,78,"Same Mjini");
//        databaseHelper.insertPlaces(1308,78,"Stesheni");
//        databaseHelper.insertPlaces(1309,78,"Suji");
//        databaseHelper.insertPlaces(1310,78,"Vudee");
//        databaseHelper.insertPlaces(1311,78,"Vuje");
//        databaseHelper.insertPlaces(1312,78,"Vumari");
//        databaseHelper.insertPlaces(1313,78,"Vunta");
//
//        databaseHelper.insertPlaces(1314,79,"Biriri");
//        databaseHelper.insertPlaces(1315,79,"Gararagua");
//        databaseHelper.insertPlaces(1316,79,"Karansi");
//        databaseHelper.insertPlaces(1317,79,"Kashashi");
//        databaseHelper.insertPlaces(1318,79,"Livishi");
//        databaseHelper.insertPlaces(1319,79,"Makiwaru");
//        databaseHelper.insertPlaces(1320,79,"Naeny");
//        databaseHelper.insertPlaces(1321,79,"Nasai");
//        databaseHelper.insertPlaces(1322,79,"Ndumeti");
//        databaseHelper.insertPlaces(1323,79,"Ngarenairobi");
//        databaseHelper.insertPlaces(1324,79,"Olkolili");
//        databaseHelper.insertPlaces(1325,79,"Sanya Juu");
//
//        databaseHelper.insertPlaces(1326,80,"Chumo");
//        databaseHelper.insertPlaces(1327,80,"Kandawale");
//        databaseHelper.insertPlaces(1328,80,"Kibata");
//        databaseHelper.insertPlaces(1329,80,"Kikole");
//        databaseHelper.insertPlaces(1330,80,"Kijumbi");
//        databaseHelper.insertPlaces(1331,80,"Kilwa Masoko");
//        databaseHelper.insertPlaces(1332,80,"Kipatimu");
//        databaseHelper.insertPlaces(1333,80,"Kivinje Singino");
//        databaseHelper.insertPlaces(1334,80,"Kiranjeranje");
//        databaseHelper.insertPlaces(1335,80,"Lihimalyao");
//        databaseHelper.insertPlaces(1336,80,"Likawage");
//        databaseHelper.insertPlaces(1337,80,"Mandawa");
//        databaseHelper.insertPlaces(1338,80,"Miteja");
//        databaseHelper.insertPlaces(1339,80,"Mingumbi");
//        databaseHelper.insertPlaces(1340,80,"Mitole");
//        databaseHelper.insertPlaces(1341,80,"Namayuni");
//        databaseHelper.insertPlaces(1342,80,"Miguruwe");
//        databaseHelper.insertPlaces(1343,80,"Nanjirinji");
//        databaseHelper.insertPlaces(1344,80,"Njinjo");
//        databaseHelper.insertPlaces(1345,80,"Pande Mikoma");
//        databaseHelper.insertPlaces(1346,80,"Somanga");
//        databaseHelper.insertPlaces(1347,80,"Songosongo");
//        databaseHelper.insertPlaces(1348,80,"Tingi (Kilwa)");
//
//        databaseHelper.insertPlaces(1349,81,"Chiponda");
//        databaseHelper.insertPlaces(1350,81,"Kilangala");
//        databaseHelper.insertPlaces(1351,81,"Kilolambwani");
//        databaseHelper.insertPlaces(1352,81,"Kinyope");
//        databaseHelper.insertPlaces(1353,81,"Kitomanga");
//        databaseHelper.insertPlaces(1354,81,"Kiwalala");
//        databaseHelper.insertPlaces(1355,81,"Kiwawa");
//        databaseHelper.insertPlaces(1356,81,"Longa");
//        databaseHelper.insertPlaces(1357,81,"Majengo");
//        databaseHelper.insertPlaces(1358,81,"Mandwanga");
//        databaseHelper.insertPlaces(1359,81,"Matimba");
//        databaseHelper.insertPlaces(1360,81,"Mchinga");
//        databaseHelper.insertPlaces(1361,81,"Milola");
//        databaseHelper.insertPlaces(1362,81,"Mipingo");
//        databaseHelper.insertPlaces(1363,81,"Mnara");
//        databaseHelper.insertPlaces(1364,81,"Mnolela");
//        databaseHelper.insertPlaces(1365,81,"Mtama");
//        databaseHelper.insertPlaces(1366,81,"Mtua");
//        databaseHelper.insertPlaces(1367,81,"Mtumbya");
//        databaseHelper.insertPlaces(1368,81,"Mvuleni");
//        databaseHelper.insertPlaces(1369,81,"Nachunyu");
//        databaseHelper.insertPlaces(1370,81,"Nahukahuka");
//        databaseHelper.insertPlaces(1371,81,"Namangwale");
//        databaseHelper.insertPlaces(1372,81,"Namupa");
//        databaseHelper.insertPlaces(1373,81,"Nangaru");
//        databaseHelper.insertPlaces(1374,81,"Navanga");
//        databaseHelper.insertPlaces(1375,81,"Nyangamara");
//        databaseHelper.insertPlaces(1376,81,"Nyangao");
//        databaseHelper.insertPlaces(1377,81,"Nyengedi");
//        databaseHelper.insertPlaces(1378,81,"Pangatena");
//        databaseHelper.insertPlaces(1379,81,"Rutamba");
//        databaseHelper.insertPlaces(1380,81,"Sudi");
//
//        databaseHelper.insertPlaces(1381,82,"Chikonji");
//        databaseHelper.insertPlaces(1382,82,"Jamhuri");
//        databaseHelper.insertPlaces(1383,82,"Kitumbikwela");
//        databaseHelper.insertPlaces(1384,82,"Makonde");
//        databaseHelper.insertPlaces(1385,82,"Matopeni");
//        databaseHelper.insertPlaces(1386,82,"Mbanja");
//        databaseHelper.insertPlaces(1387,82,"Mikumbi");
//        databaseHelper.insertPlaces(1388,82,"Mingoyo");
//        databaseHelper.insertPlaces(1389,82,"Mitandi");
//        databaseHelper.insertPlaces(1390,82,"Mnazimoja");
//        databaseHelper.insertPlaces(1391,82,"Msanjihili");
//        databaseHelper.insertPlaces(1392,82,"Mtanda");
//        databaseHelper.insertPlaces(1393,82,"Mwenge");
//        databaseHelper.insertPlaces(1394,82,"Nachingwea");
//        databaseHelper.insertPlaces(1395,82,"Ndoro");
//        databaseHelper.insertPlaces(1396,82,"Ng'apa");
//        databaseHelper.insertPlaces(1397,82,"Rahaleo");
//        databaseHelper.insertPlaces(1398,82,"Rasbura");
//        databaseHelper.insertPlaces(1399,82,"Tandangongoro");
//        databaseHelper.insertPlaces(1400,82,"Wailesi");
//
//        databaseHelper.insertPlaces(1401,83,"Barikiwa");
//        databaseHelper.insertPlaces(1402,83,"Kiangara");
//        databaseHelper.insertPlaces(1403,83,"Kibutuka");
//        databaseHelper.insertPlaces(1404,83,"Kichonda");
//        databaseHelper.insertPlaces(1405,83,"Kimambi");
//        databaseHelper.insertPlaces(1406,83,"Likongowele");
//        databaseHelper.insertPlaces(1407,83,"Lilombe");
//        databaseHelper.insertPlaces(1408,83,"Liwale B");
//        databaseHelper.insertPlaces(1409,83,"Liwale Mjini");
//        databaseHelper.insertPlaces(1410,83,"Makata");
//        databaseHelper.insertPlaces(1411,83,"Mangirirkiti");
//        databaseHelper.insertPlaces(1412,83,"Mbaya");
//        databaseHelper.insertPlaces(1413,83,"Mihumo");
//        databaseHelper.insertPlaces(1414,83,"Mirui");
//        databaseHelper.insertPlaces(1415,83,"Mkutano");
//        databaseHelper.insertPlaces(1416,83,"Mlembwe");
//        databaseHelper.insertPlaces(1417,83,"Mpigamiti");
//        databaseHelper.insertPlaces(1418,83,"Nangando");
//        databaseHelper.insertPlaces(1419,83,"Nangano");
//        databaseHelper.insertPlaces(1420,83,"Ngongowele");
//
//        databaseHelper.insertPlaces(1421,84,"Boma");
//        databaseHelper.insertPlaces(1422,84,"Chiola");
//        databaseHelper.insertPlaces(1423,84,"Chiumbati Shuleni");
//        databaseHelper.insertPlaces(1424,84,"Kiegei");
//        databaseHelper.insertPlaces(1425,84,"Kilimani Hewa");
//        databaseHelper.insertPlaces(1426,84,"Kilimarondo");
//        databaseHelper.insertPlaces(1427,84,"Kipara Mnero");
//        databaseHelper.insertPlaces(1428,84,"Kipara Mtua");
//        databaseHelper.insertPlaces(1429,84,"Lionja");
//        databaseHelper.insertPlaces(1430,84,"Marambo");
//        databaseHelper.insertPlaces(1431,84,"Matekwe");
//        databaseHelper.insertPlaces(1432,84,"Mbondo");
//        databaseHelper.insertPlaces(1433,84,"Mchonda");
//        databaseHelper.insertPlaces(1434,84,"Mitumbati");
//        databaseHelper.insertPlaces(1435,84,"Mkoka");
//        databaseHelper.insertPlaces(1436,84,"Mkotokuyana");
//        databaseHelper.insertPlaces(1437,84,"Mnero Miembeni");
//        databaseHelper.insertPlaces(1438,84,"Mnero Ngongo");
//        databaseHelper.insertPlaces(1439,84,"Mpiruka");
//        databaseHelper.insertPlaces(1440,84,"Mutua");
//        databaseHelper.insertPlaces(1441,84,"Nachingwea Mjini");
//        databaseHelper.insertPlaces(1442,84,"Naipanga");
//        databaseHelper.insertPlaces(1443,84,"Naipingo");
//        databaseHelper.insertPlaces(1444,84,"Namapwia");
//        databaseHelper.insertPlaces(1445,84,"Namatula");
//        databaseHelper.insertPlaces(1446,84,"Nambambo");
//        databaseHelper.insertPlaces(1447,84,"Namikango");
//        databaseHelper.insertPlaces(1448,84,"Nang'ondo");
//        databaseHelper.insertPlaces(1449,84,"Nangowe");
//        databaseHelper.insertPlaces(1450,84,"Nditi");
//        databaseHelper.insertPlaces(1451,84,"Ndomoni");
//        databaseHelper.insertPlaces(1452,84,"Ngunichile");
//        databaseHelper.insertPlaces(1453,84,"Raha leo");
//        databaseHelper.insertPlaces(1454,84,"Ruponda");
//        databaseHelper.insertPlaces(1455,84,"Stesheni");
//        databaseHelper.insertPlaces(1456,84,"Ugawaji");
//
//        databaseHelper.insertPlaces(1457,85,"Chibula");
//        databaseHelper.insertPlaces(1458,85,"Chienjele");
//        databaseHelper.insertPlaces(1459,85,"Chinongwe");
//        databaseHelper.insertPlaces(1460,85,"Chunyu");
//        databaseHelper.insertPlaces(1461,85,"Likunja");
//        databaseHelper.insertPlaces(1462,85,"Luchelegwa");
//        databaseHelper.insertPlaces(1463,85,"Makanjiro");
//        databaseHelper.insertPlaces(1464,85,"Malolo");
//        databaseHelper.insertPlaces(1465,85,"Mandarawe");
//        databaseHelper.insertPlaces(1466,85,"Mandawa");
//        databaseHelper.insertPlaces(1467,85,"Matambarale");
//        databaseHelper.insertPlaces(1468,85,"Mbekenyera");
//        databaseHelper.insertPlaces(1469,85,"Mbwemkuru (Machang'anja)");
//        databaseHelper.insertPlaces(1470,85,"Mnacho");
//        databaseHelper.insertPlaces(1471,85,"Nachingwea");
//        databaseHelper.insertPlaces(1472,85,"Nambilaje");
//        databaseHelper.insertPlaces(1473,85,"Namichiga");
//        databaseHelper.insertPlaces(1474,85,"Nandagala");
//        databaseHelper.insertPlaces(1475,85,"Nanganga");
//        databaseHelper.insertPlaces(1476,85,"Narungombe");
//        databaseHelper.insertPlaces(1477,85,"Nkowe");
//        databaseHelper.insertPlaces(1478,85,"Ruangwa (mji)");
//
//        databaseHelper.insertPlaces(1479,86,"Babati");
//        databaseHelper.insertPlaces(1480,86,"Bagara");
//        databaseHelper.insertPlaces(1481,86,"Bonga");
//        databaseHelper.insertPlaces(1482,86,"Maisaka");
//        databaseHelper.insertPlaces(1483,86,"Mutuka");
//        databaseHelper.insertPlaces(1484,86,"Nangara");
//        databaseHelper.insertPlaces(1485,86,"Sigino");
//        databaseHelper.insertPlaces(1486,86,"Singe");
//
//        databaseHelper.insertPlaces(1487,87,"Arri");
//        databaseHelper.insertPlaces(1488,87,"Ayasanda");
//        databaseHelper.insertPlaces(1489,87,"Baoy");
//        databaseHelper.insertPlaces(1490,87,"Bashnet");
//        databaseHelper.insertPlaces(1491,87,"Dabil");
//        databaseHelper.insertPlaces(1492,87,"Dareda");
//        databaseHelper.insertPlaces(1493,87,"Duru");
//        databaseHelper.insertPlaces(1494,87,"Endakiso");
//        databaseHelper.insertPlaces(1495,87,"Gallapo");
//        databaseHelper.insertPlaces(1496,87,"Gidas");
//        databaseHelper.insertPlaces(1497,87,"Kiru");
//        databaseHelper.insertPlaces(1498,87,"Madunga");
//        databaseHelper.insertPlaces(1499,87,"Magara");
//        databaseHelper.insertPlaces(1500,87,"Magugu");
//        databaseHelper.insertPlaces(1501,87,"Mamire");
//        databaseHelper.insertPlaces(1502,87,"Mwada");
//        databaseHelper.insertPlaces(1503,87,"Nar");
//        databaseHelper.insertPlaces(1504,87,"Nkaiti");
//        databaseHelper.insertPlaces(1505,87,"Qash");
//        databaseHelper.insertPlaces(1506,87,"Riroda");
//        databaseHelper.insertPlaces(1507,87,"Ufana");
//
//        databaseHelper.insertPlaces(1508,88,"Balangdalalu");
//        databaseHelper.insertPlaces(1509,88,"Bassodesh");
//        databaseHelper.insertPlaces(1510,88,"Bassotu");
//        databaseHelper.insertPlaces(1511,88,"Dirima");
//        databaseHelper.insertPlaces(1512,88,"Endasaki");
//        databaseHelper.insertPlaces(1513,88,"Endasiwold");
//        databaseHelper.insertPlaces(1514,88,"Ganana");
//        databaseHelper.insertPlaces(1515,88,"Gehandu");
//        databaseHelper.insertPlaces(1516,88,"Gendabi");
//        databaseHelper.insertPlaces(1517,88,"Getanuwas");
//        databaseHelper.insertPlaces(1518,88,"Gidahababieg");
//        databaseHelper.insertPlaces(1519,88,"Gisambalang");
//        databaseHelper.insertPlaces(1520,88,"Gitting");
//        databaseHelper.insertPlaces(1521,88,"Hidet");
//        databaseHelper.insertPlaces(1522,88,"Hirbadaw");
//        databaseHelper.insertPlaces(1523,88,"Katesh");
//        databaseHelper.insertPlaces(1524,88,"Laghanga");
//        databaseHelper.insertPlaces(1525,88,"Lalaji");
//        databaseHelper.insertPlaces(1526,88,"Masakta");
//        databaseHelper.insertPlaces(1527,88,"Masqaroda");
//        databaseHelper.insertPlaces(1528,88,"Measkron");
//        databaseHelper.insertPlaces(1529,88,"Mogitu");
//        databaseHelper.insertPlaces(1530,88,"Nangwa");
//        databaseHelper.insertPlaces(1531,88,"Simbay");
//        databaseHelper.insertPlaces(1532,88,"Sirop");
//
//        databaseHelper.insertPlaces(1533,89,"Bwagamoyo");
//        databaseHelper.insertPlaces(1534,89,"Chapakazi");
//        databaseHelper.insertPlaces(1535,89,"Dongo");
//        databaseHelper.insertPlaces(1536,89,"Dosidosi");
//        databaseHelper.insertPlaces(1537,89,"Engusero");
//        databaseHelper.insertPlaces(1538,89,"Kibaya");
//        databaseHelper.insertPlaces(1539,89,"Kijungu ");
//        databaseHelper.insertPlaces(1540,89,"Lengatei");
//        databaseHelper.insertPlaces(1541,89,"Loolera");
//        databaseHelper.insertPlaces(1542,89,"Magungu");
//        databaseHelper.insertPlaces(1543,89,"Makame");
//        databaseHelper.insertPlaces(1544,89,"Matui");
//        databaseHelper.insertPlaces(1545,89,"Namelock");
//        databaseHelper.insertPlaces(1546,89,"Ndedo");
//        databaseHelper.insertPlaces(1547,89,"Njoro");
//        databaseHelper.insertPlaces(1548,89,"Olboloti");
//        databaseHelper.insertPlaces(1549,89,"Partimbo");
//        databaseHelper.insertPlaces(1550,89,"Songambele ");
//        databaseHelper.insertPlaces(1551,89,"Sunya");
//
//        databaseHelper.insertPlaces(1552,90,"Ayamaami");
//        databaseHelper.insertPlaces(1553,90,"Ayamohe");
//        databaseHelper.insertPlaces(1554,90,"Bargish");
//        databaseHelper.insertPlaces(1555,90,"Bashay");
//        databaseHelper.insertPlaces(1556,90,"Daudi");
//        databaseHelper.insertPlaces(1557,90," Dinamu ");
//        databaseHelper.insertPlaces(1558,90,"Dongobesh");
//        databaseHelper.insertPlaces(1559,90,"Endagikot");
//        databaseHelper.insertPlaces(1560,90,"Endamilay");
//        databaseHelper.insertPlaces(1561,90,"Eshkesh");
//        databaseHelper.insertPlaces(1562,90,"Gehandu");
//        databaseHelper.insertPlaces(1563,90,"Geterer");
//        databaseHelper.insertPlaces(1564,90,"Gidihim");
//        databaseHelper.insertPlaces(1565,90,"Gunyoda");
//        databaseHelper.insertPlaces(1566,90,"Hayderer ");
//        databaseHelper.insertPlaces(1567,90,"Haydom");
//        databaseHelper.insertPlaces(1568,90,"Imboru");
//        databaseHelper.insertPlaces(1569,90,"Kainam");
//        databaseHelper.insertPlaces(1570,90,"Maghang");
//        databaseHelper.insertPlaces(1571,90,"Marang");
//        databaseHelper.insertPlaces(1572,90,"Maretadu");
//        databaseHelper.insertPlaces(1573,90,"Masieda");
//        databaseHelper.insertPlaces(1574,90," Masqaroda ");
//        databaseHelper.insertPlaces(1575,90,"Murray (Mbulu) ");
//        databaseHelper.insertPlaces(1576,90,"Nahasey ");
//        databaseHelper.insertPlaces(1577,90,"Nambisi");
//        databaseHelper.insertPlaces(1578,90,"Sanu Baray");
//        databaseHelper.insertPlaces(1579,90," Tlawi");
//        databaseHelper.insertPlaces(1580,90,"Tumati ");
//        databaseHelper.insertPlaces(1581,90," Uhuru");
//        databaseHelper.insertPlaces(1582,90,"Yaeda Ampa");
//        databaseHelper.insertPlaces(1583,90,"Yaeda Chini");
//
//        databaseHelper.insertPlaces(1584,91,"Emboreet");
//        databaseHelper.insertPlaces(1585,91,"Endiamtu");
//        databaseHelper.insertPlaces(1586,91,"Endonyongijape");
//        databaseHelper.insertPlaces(1587,91,"Kitwai");
//        databaseHelper.insertPlaces(1588,91,"Komolo");
//        databaseHelper.insertPlaces(1589,91,"Loiborsoit ");
//        databaseHelper.insertPlaces(1590,91,"Loiborsiret ");
//        databaseHelper.insertPlaces(1591,91,"Mererani");
//        databaseHelper.insertPlaces(1592,91,"Msitu wa Tembo");
//        databaseHelper.insertPlaces(1593,91,"Naberera");
//        databaseHelper.insertPlaces(1594,91,"Naisinyai");
//        databaseHelper.insertPlaces(1595,91,"Ngorika");
//        databaseHelper.insertPlaces(1596,91,"Oljoro No.5");
//        databaseHelper.insertPlaces(1597,91,"Orkesumet");
//        databaseHelper.insertPlaces(1598,91,"Ruvu Remit");
//        databaseHelper.insertPlaces(1599,91,"Shambarai ");
//        databaseHelper.insertPlaces(1600,91,"Terrat");
//
//        databaseHelper.insertPlaces(1601,92,"Balili");
//        databaseHelper.insertPlaces(1602,92,"Bunda Mjini");
//        databaseHelper.insertPlaces(1603,92,"Bunda Stoo ");
//        databaseHelper.insertPlaces(1604,92,"Butimba ");
//        databaseHelper.insertPlaces(1605,92,"Chitengule");
//        databaseHelper.insertPlaces(1606,92," Hunyari");
//        databaseHelper.insertPlaces(1607,92,"gundu");
//        databaseHelper.insertPlaces(1608,92,"Iramba ");
//        databaseHelper.insertPlaces(1609,92,"Kabasa");
//        databaseHelper.insertPlaces(1610,92,"Kasuguti");
//        databaseHelper.insertPlaces(1611,92,"Ketare");
//        databaseHelper.insertPlaces(1612,92,"Kibara");
//        databaseHelper.insertPlaces(1613,92,"Kisorya ");
//        databaseHelper.insertPlaces(1614,92,"Kunzugu ");
//        databaseHelper.insertPlaces(1615,92,"Mcharo");
//        databaseHelper.insertPlaces(1616,92,"Mihingo ");
//        databaseHelper.insertPlaces(1617,92,"Mugeta");
//        databaseHelper.insertPlaces(1618,92,"Namhula");
//        databaseHelper.insertPlaces(1619,92,"Nampindi");
//        databaseHelper.insertPlaces(1620,92,"Nansimo");
//        databaseHelper.insertPlaces(1621,92," Neruma");
//        databaseHelper.insertPlaces(1622,92,"Nyamang'uta ");
//        databaseHelper.insertPlaces(1623,92," Nyamuswa ");
//        databaseHelper.insertPlaces(1624,92,"Nyasura ");
//        databaseHelper.insertPlaces(1625,92,"Salama");
//        databaseHelper.insertPlaces(1626,92,"Sazira");
//        databaseHelper.insertPlaces(1627,92,"Wariku");
//
//        databaseHelper.insertPlaces(1628,93,"Bisumwa ");
//        databaseHelper.insertPlaces(1629,93," Buhemba");
//        databaseHelper.insertPlaces(1630,93,"Bukabwa");
//        databaseHelper.insertPlaces(1631,93,"Buruma");
//        databaseHelper.insertPlaces(1632,93,"Busegwe ");
//        databaseHelper.insertPlaces(1633,93,"Buswahili");
//        databaseHelper.insertPlaces(1634,93," Butiama ");
//        databaseHelper.insertPlaces(1635,93,"Butuguri ");
//        databaseHelper.insertPlaces(1636,93,"Bwiregi");
//        databaseHelper.insertPlaces(1637,93,"Etaro");
//        databaseHelper.insertPlaces(1638,93,"Kukirango ");
//        databaseHelper.insertPlaces(1639,93," Kyanyari");
//        databaseHelper.insertPlaces(1640,93," Masaba");
//        databaseHelper.insertPlaces(1641,93,"Mirwa");
//        databaseHelper.insertPlaces(1642,93,"Muriaza");
//        databaseHelper.insertPlaces(1643,93,"Nyakatende");
//        databaseHelper.insertPlaces(1644,93,"Nyamimange");
//        databaseHelper.insertPlaces(1645,93,"Nyankanga");
//        databaseHelper.insertPlaces(1646,93," Nyegina");
//        databaseHelper.insertPlaces(1647,93,"Sirorisimba");
//
//
//        databaseHelper.insertPlaces(1648,94,"Buhare");
//        databaseHelper.insertPlaces(1649,94,"Bweri");
//        databaseHelper.insertPlaces(1650,94,"Iringo ");
//        databaseHelper.insertPlaces(1651,94,"Kamunyonge");
//        databaseHelper.insertPlaces(1652,94,"Kigera");
//        databaseHelper.insertPlaces(1653,94," Kitaji");
//        databaseHelper.insertPlaces(1654,94,"Makoko");
//        databaseHelper.insertPlaces(1655,94,"Mukendo ");
//        databaseHelper.insertPlaces(1656,94,"Mwigobero");
//        databaseHelper.insertPlaces(1657,94,"Mwisenge ");
//        databaseHelper.insertPlaces(1658,94,"Nyakato");
//        databaseHelper.insertPlaces(1659,94,"Nyamatare");
//        databaseHelper.insertPlaces(1660,94,"Nyasho");
//
//        databaseHelper.insertPlaces(1661,95,"Bugwema");
//        databaseHelper.insertPlaces(1662,95," Buhemba");
//        databaseHelper.insertPlaces(1663,95,"Bukabwa");
//        databaseHelper.insertPlaces(1664,95,"Bukima");
//        databaseHelper.insertPlaces(1665,95,"Bukumi");
//        databaseHelper.insertPlaces(1666,95,"Buruma ");
//        databaseHelper.insertPlaces(1667,95,"Buswahili");
//        databaseHelper.insertPlaces(1668,95,"Butiama");
//        databaseHelper.insertPlaces(1669,95,"Butuguri");
//        databaseHelper.insertPlaces(1670,95," Bwasi");
//        databaseHelper.insertPlaces(1671,95,"Bwiregi");
//        databaseHelper.insertPlaces(1672,95,"Etaro");
//        databaseHelper.insertPlaces(1673,95,"Kiriba");
//        databaseHelper.insertPlaces(1674,95," Kukirango");
//        databaseHelper.insertPlaces(1675,95,"Kyanyari");
//        databaseHelper.insertPlaces(1676,95,"Makojo");
//        databaseHelper.insertPlaces(1677,95,"Masaba");
//        databaseHelper.insertPlaces(1678,95,"Mugango");
//        databaseHelper.insertPlaces(1679,95,"Murangi");
//        databaseHelper.insertPlaces(1680,95,"Muriaza");
//        databaseHelper.insertPlaces(1681,95,"Nyakatende");
//        databaseHelper.insertPlaces(1682,95,"Nyambono ");
//        databaseHelper.insertPlaces(1683,95,"Nyamimange");
//        databaseHelper.insertPlaces(1684,95,"Nyamrandirira");
//        databaseHelper.insertPlaces(1685,95,"Nyankanga ");
//        databaseHelper.insertPlaces(1686,95,"Suguti");
//        databaseHelper.insertPlaces(1687,95,"Tegeruka");
//
//        databaseHelper.insertPlaces(1688,96,"Bukura");
//        databaseHelper.insertPlaces(1689,96,"Bukwe ");
//        databaseHelper.insertPlaces(1690,96," Goribe");
//        databaseHelper.insertPlaces(1691,96," Ikoma");
//        databaseHelper.insertPlaces(1692,96,"Kigunga");
//        databaseHelper.insertPlaces(1693,96," Kirogo ");
//        databaseHelper.insertPlaces(1694,96,"Kisumwa ");
//        databaseHelper.insertPlaces(1695,96,"Kitembe");
//        databaseHelper.insertPlaces(1696,96,"Komuge ");
//        databaseHelper.insertPlaces(1697,96," Koryo");
//        databaseHelper.insertPlaces(1698,96,"Kyang'ombe ");
//        databaseHelper.insertPlaces(1699,96,"Mirare ");
//        databaseHelper.insertPlaces(1700,96,"Mkoma");
//        databaseHelper.insertPlaces(1701,96," Nyahongo ");
//        databaseHelper.insertPlaces(1702,96,"Nyamagaro");
//        databaseHelper.insertPlaces(1703,96,"Nyamtinga");
//        databaseHelper.insertPlaces(1704,96,"Nyamunga ");
//        databaseHelper.insertPlaces(1705,96,"Nyathorogo");
//        databaseHelper.insertPlaces(1706,96,"Rabour ");
//        databaseHelper.insertPlaces(1707,96,"Roche ");
//        databaseHelper.insertPlaces(1708,96,"Tai");
//
//        databaseHelper.insertPlaces(1709,97,"Busawe");
//        databaseHelper.insertPlaces(1710,97,"Geitasamo");
//        databaseHelper.insertPlaces(1711,97,"Ikoma");
//        databaseHelper.insertPlaces(1712,97,"Issenye ");
//        databaseHelper.insertPlaces(1713,97,"Kebanchabancha");
//        databaseHelper.insertPlaces(1714,97,"Kenyamonta");
//        databaseHelper.insertPlaces(1715,97," Kisaka");
//        databaseHelper.insertPlaces(1716,97,"Kisangura");
//        databaseHelper.insertPlaces(1717,97," Kyambah");
//        databaseHelper.insertPlaces(1718,97,"Machochwe ");
//        databaseHelper.insertPlaces(1719,97,"Magange ");
//        databaseHelper.insertPlaces(1720,97,"Majimoto ");
//        databaseHelper.insertPlaces(1721,97,"Manchira");
//        databaseHelper.insertPlaces(1722,97," Mbalibali ");
//        databaseHelper.insertPlaces(1723,97,"Morotonga ");
//        databaseHelper.insertPlaces(1724,97,"Mosongo ");
//        databaseHelper.insertPlaces(1725,97,"Mugumu mjini");
//        databaseHelper.insertPlaces(1726,97," Natta");
//        databaseHelper.insertPlaces(1727,97,"Nyamatare");
//        databaseHelper.insertPlaces(1728,97,"Nyambureti ");
//        databaseHelper.insertPlaces(1729,97,"Nyamoko ");
//        databaseHelper.insertPlaces(1730,97,"Nyansurura ");
//        databaseHelper.insertPlaces(1731,97," Rigicha ");
//        databaseHelper.insertPlaces(1732,97,"Ring'wani");
//        databaseHelper.insertPlaces(1733,97,"Rung'abure");
//        databaseHelper.insertPlaces(1734,97,"Sedeco");
//        databaseHelper.insertPlaces(1735,97," Stendi Kuu");
//        databaseHelper.insertPlaces(1736,97,"Uwanja wa Ndege");
//
//        databaseHelper.insertPlaces(1737,98,"Binagi");
//        databaseHelper.insertPlaces(1738,98," Bomani ");
//        databaseHelper.insertPlaces(1739,98," Bumera");
//        databaseHelper.insertPlaces(1740,98,"Genyange");
//        databaseHelper.insertPlaces(1741,98,"Gorong'a ");
//        databaseHelper.insertPlaces(1742,98,"Itiryo ");
//        databaseHelper.insertPlaces(1743,98,"Kemambo");
//        databaseHelper.insertPlaces(1744,98,"Kentare ");
//        databaseHelper.insertPlaces(1745,98,"Kibasuka");
//        databaseHelper.insertPlaces(1746,98,"Kiore");
//        databaseHelper.insertPlaces(1747,98,"Komaswa ");
//        databaseHelper.insertPlaces(1748,98,"Manga (Tarime) ");
//        databaseHelper.insertPlaces(1749,98,"Matongo");
//        databaseHelper.insertPlaces(1750,98,"Mbogi");
//        databaseHelper.insertPlaces(1751,98,"Muriba ");
//        databaseHelper.insertPlaces(1752,98,"Mwema");
//        databaseHelper.insertPlaces(1753,98,"Nyakonga");
//        databaseHelper.insertPlaces(1754,98,"Nyamaranga");
//        databaseHelper.insertPlaces(1755,98,"Nyamisangura");
//        databaseHelper.insertPlaces(1756,98,"Nyamwaga ");
//        databaseHelper.insertPlaces(1757,98,"Nyandoto ");
//        databaseHelper.insertPlaces(1758,98,"Nyansincha");
//        databaseHelper.insertPlaces(1759,98,"Nyanungu");
//        databaseHelper.insertPlaces(1760,98,"Nyarero ");
//        databaseHelper.insertPlaces(1761,98,"Nyarokoba ");
//        databaseHelper.insertPlaces(1762,98,"Pemba ");
//        databaseHelper.insertPlaces(1763,98,"Sabasaba");
//        databaseHelper.insertPlaces(1764,98,"Sirari");
//        databaseHelper.insertPlaces(1765,98,"Susuni");
//        databaseHelper.insertPlaces(1766,98,"Turwa");
//
//        databaseHelper.insertPlaces(1767,99,"Chalangwa");
//        databaseHelper.insertPlaces(1768,99,"Chokaa (Chunya) ");
//        databaseHelper.insertPlaces(1769,99,"Galula");
//        databaseHelper.insertPlaces(1770,99,"Gua ");
//        databaseHelper.insertPlaces(1771,99,"Ifumbo");
//        databaseHelper.insertPlaces(1772,99,"Itewe");
//        databaseHelper.insertPlaces(1773,99,"Kambikatoto ");
//        databaseHelper.insertPlaces(1774,99,"Kanga (Chunya)");
//        databaseHelper.insertPlaces(1775,99,"Kapalala");
//        databaseHelper.insertPlaces(1776,99," Chalangwa");
//        databaseHelper.insertPlaces(1777,99,"Lupatingatinga ");
//        databaseHelper.insertPlaces(1778,99,"Luwalaje");
//        databaseHelper.insertPlaces(1779,99," Mafyeko");
//        databaseHelper.insertPlaces(1780,99,"Magamba ");
//        databaseHelper.insertPlaces(1781,99,"Makongorosi ");
//        databaseHelper.insertPlaces(1782,99,"Mamba (Chunya)");
//        databaseHelper.insertPlaces(1783,99,"Matundasi ");
//        databaseHelper.insertPlaces(1784,99,"Matwiga (Chunya)");
//        databaseHelper.insertPlaces(1785,99,"Mbangala");
//        databaseHelper.insertPlaces(1786,99,"Mbugani (Chunya) ");
//        databaseHelper.insertPlaces(1787,99," Mbuyuni (Chunya)");
//        databaseHelper.insertPlaces(1788,99,"Mkola");
//        databaseHelper.insertPlaces(1789,99,"Mtanila");
//        databaseHelper.insertPlaces(1790,99,"Mkwajuni (Chunya) ");
//        databaseHelper.insertPlaces(1791,99,"Mwambani");
//        databaseHelper.insertPlaces(1792,99," Namkukwe ");
//        databaseHelper.insertPlaces(1793,99,"Ngwala ");
//        databaseHelper.insertPlaces(1794,99,"Sangambi");
//        databaseHelper.insertPlaces(1795,99,"Saza");
//        databaseHelper.insertPlaces(1796,99,"Totowe");


//
//        databaseHelper.insertPlaces(1797,100,"Bupigu");
//        databaseHelper.insertPlaces(1798,100,"Chitete");
//        databaseHelper.insertPlaces(1799,100,"Ibaba");
//        databaseHelper.insertPlaces(1800,100,"Ikinga ");
//        databaseHelper.insertPlaces(1801,100,"Isongole");
//        databaseHelper.insertPlaces(1802,100,"Itale ");
//        databaseHelper.insertPlaces(1803,100,"Itumba");
//        databaseHelper.insertPlaces(1804,100,"Kafule");
//        databaseHelper.insertPlaces(1805,100,"Kalembo");
//        databaseHelper.insertPlaces(1806,100,"Lubanda");
//        databaseHelper.insertPlaces(1807,100,"Luswisi");
//        databaseHelper.insertPlaces(1808,100,"Malangali");
//        databaseHelper.insertPlaces(1809,100,"Mbebe");
//        databaseHelper.insertPlaces(1810,100,"Mlale");
//        databaseHelper.insertPlaces(1811,100,"Ndola");
//        databaseHelper.insertPlaces(1812,100,"Ngulilo ");
//        databaseHelper.insertPlaces(1813,100,"Ngulugulu");
//
//
//
//        databaseHelper.insertPlaces(1814,101,"Bujonde ");
//        databaseHelper.insertPlaces(1815,101,"Busale ");
//        databaseHelper.insertPlaces(1816,101,"Ikama ");
//        databaseHelper.insertPlaces(1817,101,"Ikimba ");
//        databaseHelper.insertPlaces(1818,101,"Ikolo ");
//        databaseHelper.insertPlaces(1819,101,"Ipande ");
//        databaseHelper.insertPlaces(1820,101," Ipinda");
//        databaseHelper.insertPlaces(1821,101," Itope");
//        databaseHelper.insertPlaces(1822,101,"Kajunjumele ");
//        databaseHelper.insertPlaces(1823,101,"Katumba Songwe ");
//        databaseHelper.insertPlaces(1824,101,"Kyela Mjini ");
//        databaseHelper.insertPlaces(1825,101,"Lusungo ");
//        databaseHelper.insertPlaces(1826,101,"Makwale");
//        databaseHelper.insertPlaces(1827,101,"Matema ");
//        databaseHelper.insertPlaces(1828,101,"Muungano (Kyela)");
//        databaseHelper.insertPlaces(1829,101,"Mwaya ");
//        databaseHelper.insertPlaces(1830,101,"Ndobo");
//        databaseHelper.insertPlaces(1831,101,"Ngana ");
//        databaseHelper.insertPlaces(1832,101,"Ngonga");
//        databaseHelper.insertPlaces(1833,101,"Talatala");
//
//        databaseHelper.insertPlaces(1834,102,"Chimala ");
//        databaseHelper.insertPlaces(1835,102,"Igava");
//        databaseHelper.insertPlaces(1836,102,"Igurusi ");
//        databaseHelper.insertPlaces(1837,102," Ihahi");
//        databaseHelper.insertPlaces(1838,102,"Imalilosongwe ");
//        databaseHelper.insertPlaces(1839,102,"Ipwani");
//        databaseHelper.insertPlaces(1840,102,"Itamboleo");
//        databaseHelper.insertPlaces(1841,102,"Kongolo");
//        databaseHelper.insertPlaces(1842,102,"Lugelele");
//        databaseHelper.insertPlaces(1843,102,"Luhanga");
//        databaseHelper.insertPlaces(1844,102,"Madibira");
//        databaseHelper.insertPlaces(1845,102,"Mahongole");
//        databaseHelper.insertPlaces(1846,102,"Mapogoro ");
//        databaseHelper.insertPlaces(1847,102,"Mawindi ");
//        databaseHelper.insertPlaces(1848,102,"Miyombweni ");
//        databaseHelper.insertPlaces(1849,102,"Mwatenga");
//        databaseHelper.insertPlaces(1850,102,"Ruiwa ");
//        databaseHelper.insertPlaces(1851,102,"Rujewa");
//        databaseHelper.insertPlaces(1852,102,"Ubaruku");
//        databaseHelper.insertPlaces(1853,102,"Utengule Usangu");
//
//        databaseHelper.insertPlaces(1854,103,"Bonde la Usongwe ");
//        databaseHelper.insertPlaces(1855,103,"Igale ");
//        databaseHelper.insertPlaces(1856,103,"Igoma");
//        databaseHelper.insertPlaces(1857,103,"Ihango ");
//        databaseHelper.insertPlaces(1858,103,"Ijombe ");
//        databaseHelper.insertPlaces(1859,103,"Ikukwa ");
//        databaseHelper.insertPlaces(1860,103,"Ilembo (Mbeya vijijini)");
//        databaseHelper.insertPlaces(1861,103,"Ilungu ");
//        databaseHelper.insertPlaces(1862,103,"Inyala");
//        databaseHelper.insertPlaces(1863,103,"Isuto ");
//        databaseHelper.insertPlaces(1864,103,"Itawa ");
//        databaseHelper.insertPlaces(1865,103,"Itewe ");
//        databaseHelper.insertPlaces(1866,103,"Iwiji ");
//        databaseHelper.insertPlaces(1867,103,"Iwiji ");
//        databaseHelper.insertPlaces(1868,103,"Iyunga Mapinduzi");
//        databaseHelper.insertPlaces(1869,103,"Lwanjiro ");
//        databaseHelper.insertPlaces(1870,103,"Maendeleo ");
//        databaseHelper.insertPlaces(1871,103,"Masoko");
//        databaseHelper.insertPlaces(1872,103,"Mshewe ");
//        databaseHelper.insertPlaces(1873,103,"Nsalala ");
//        databaseHelper.insertPlaces(1874,103,"Santilya ");
//        databaseHelper.insertPlaces(1875,103,"Swaya");
//        databaseHelper.insertPlaces(1876,103,"Tembela ");
//        databaseHelper.insertPlaces(1877,103,"Ulenje ");
//        databaseHelper.insertPlaces(1878,103,"Utengule Usongwe");
//
//        databaseHelper.insertPlaces(1879,104,"Foresti");
//        databaseHelper.insertPlaces(1880,104,"Ghana ");
//        databaseHelper.insertPlaces(1881,104,"Iduda ");
//        databaseHelper.insertPlaces(1882,104,"Iganjo");
//        databaseHelper.insertPlaces(1883,104,"Iganzo");
//        databaseHelper.insertPlaces(1884,104,"Igawilo");
//        databaseHelper.insertPlaces(1885,104,"Ilemi");
//        databaseHelper.insertPlaces(1886,104,"Ilomba");
//        databaseHelper.insertPlaces(1887,104,"Isanga ");
//        databaseHelper.insertPlaces(1888,104,"Isyesye ");
//        databaseHelper.insertPlaces(1889,104,"Itagano");
//        databaseHelper.insertPlaces(1890,104,"Itende");
//        databaseHelper.insertPlaces(1891,104,"Itezi");
//        databaseHelper.insertPlaces(1892,104,"Itiji ");
//        databaseHelper.insertPlaces(1893,104,"Iwambi ");
//        databaseHelper.insertPlaces(1894,104,"Iyela ");
//        databaseHelper.insertPlaces(1895,104,"Iyunga ");
//        databaseHelper.insertPlaces(1896,104,"Iziwa ");
//        databaseHelper.insertPlaces(1897,104,"Kalobe");
//        databaseHelper.insertPlaces(1898,104,"Maanga");
//        databaseHelper.insertPlaces(1899,104,"Mabatini");
//        databaseHelper.insertPlaces(1900,104,"Maendeleo ");
//        databaseHelper.insertPlaces(1901,104,"Majengo");
//        databaseHelper.insertPlaces(1902,104,"Mbalizi Road ");
//        databaseHelper.insertPlaces(1903,104,"Mwakibete");
//        databaseHelper.insertPlaces(1904,104,"Mwansekwa");
//        databaseHelper.insertPlaces(1905,104,"Mwansanga ");
//        databaseHelper.insertPlaces(1906,104,"Nonde ");
//        databaseHelper.insertPlaces(1907,104,"Nsalaga");
//        databaseHelper.insertPlaces(1908,104,"Nsoho");
//        databaseHelper.insertPlaces(1909,104,"Nzovwe ");
//        databaseHelper.insertPlaces(1910,104,"Ruanda (Mbeya)");
//        databaseHelper.insertPlaces(1911,104," Sinde");
//        databaseHelper.insertPlaces(1912,104,"Sisimba");
//        databaseHelper.insertPlaces(1913,104,"Tembela ");
//        databaseHelper.insertPlaces(1914,104,"Uyole");
//
//        databaseHelper.insertPlaces(1915,105,"Bara");
//        databaseHelper.insertPlaces(1916,105,"Halungu");
//        databaseHelper.insertPlaces(1917,105,"Igamba");
//        databaseHelper.insertPlaces(1918,105,"Ihanda ");
//        databaseHelper.insertPlaces(1919,105,"Ipunga ");
//        databaseHelper.insertPlaces(1920,105,"Isandula");
//        databaseHelper.insertPlaces(1921,105,"Isansa");
//        databaseHelper.insertPlaces(1922,105,"Itaka ");
//        databaseHelper.insertPlaces(1923,105,"Iyula");
//        databaseHelper.insertPlaces(1924,105,"Mlangali");
//        databaseHelper.insertPlaces(1925,105,"Mlowo");
//        databaseHelper.insertPlaces(1926,105,"Msia");
//        databaseHelper.insertPlaces(1927,105,"Myovizi");
//        databaseHelper.insertPlaces(1928,105,"Nambinzo");
//        databaseHelper.insertPlaces(1929,105,"Nanyala ");
//        databaseHelper.insertPlaces(1930,105,"Nyimbili");
//        databaseHelper.insertPlaces(1931,105,"Ruanda");
//        databaseHelper.insertPlaces(1932,105,"Vwawa");
//
//
//        databaseHelper.insertPlaces(1933,106,"Chilulumo");
//        databaseHelper.insertPlaces(1934,106,"Chitete ");
//        databaseHelper.insertPlaces(1935,106,"Chiwezi");
//        databaseHelper.insertPlaces(1936,106,"Ivuna ");
//        databaseHelper.insertPlaces(1937,106,"Kamsamba ");
//        databaseHelper.insertPlaces(1938,106,"Kapele ");
//        databaseHelper.insertPlaces(1939,106,"Mkulwe");
//        databaseHelper.insertPlaces(1940,106,"Mpapa");
//        databaseHelper.insertPlaces(1941,106,"Msangano");
//        databaseHelper.insertPlaces(1942,106,"Myunga");
//        databaseHelper.insertPlaces(1943,106,"Ndalambo");
//        databaseHelper.insertPlaces(1944,106,"Nkangamo");
//        databaseHelper.insertPlaces(1945,106,"Nzoka");
//
//
//        databaseHelper.insertPlaces(1946,107,"Bagamoyo");
//        databaseHelper.insertPlaces(1947,107,"Bujela");
//        databaseHelper.insertPlaces(1948,107,"Bulyaga");
//        databaseHelper.insertPlaces(1949,107,"Ibighi");
//        databaseHelper.insertPlaces(1950,107,"Ikama ");
//        databaseHelper.insertPlaces(1951,107,"Ikuti");
//        databaseHelper.insertPlaces(1952,107,"Ilima");
//        databaseHelper.insertPlaces(1953,107,"Isange");
//        databaseHelper.insertPlaces(1954,107,"Isongole ");
//        databaseHelper.insertPlaces(1955,107,"Itete");
//        databaseHelper.insertPlaces(1956,107,"Kabula ");
//        databaseHelper.insertPlaces(1957,107,"Kambasegela ");
//        databaseHelper.insertPlaces(1958,107,"Kandete");
//        databaseHelper.insertPlaces(1959,107,"Kawetele");
//        databaseHelper.insertPlaces(1960,107,"Kikole");
//        databaseHelper.insertPlaces(1961,107,"Kinyala");
//        databaseHelper.insertPlaces(1962,107,"Kisegese");
//        databaseHelper.insertPlaces(1963,107,"Kisiba");
//        databaseHelper.insertPlaces(1964,107,"Kisondela");
//        databaseHelper.insertPlaces(1965,107,"Kiwira");
//        databaseHelper.insertPlaces(1966,107,"Kyimo");
//        databaseHelper.insertPlaces(1967,107,"Lufilyo");
//        databaseHelper.insertPlaces(1968,107,"Lufingo");
//        databaseHelper.insertPlaces(1969,107,"Lupata");
//        databaseHelper.insertPlaces(1970,107,"Luteba");
//        databaseHelper.insertPlaces(1971,107,"Lwangwa");
//        databaseHelper.insertPlaces(1972,107,"Makandana");
//        databaseHelper.insertPlaces(1973,107,"Malindo");
//        databaseHelper.insertPlaces(1974,107,"Masebe");
//        databaseHelper.insertPlaces(1975,107,"Masoko");
//        databaseHelper.insertPlaces(1976,107,"Masukulu");
//        databaseHelper.insertPlaces(1977,107,"Mpombo");
//        databaseHelper.insertPlaces(1978,107,"Mpuguso");
//        databaseHelper.insertPlaces(1979,107,"Msasani ");
//        databaseHelper.insertPlaces(1980,107,"Nkunga ");
//        databaseHelper.insertPlaces(1981,107,"Suma");
//        databaseHelper.insertPlaces(1982,107,"Swaya");
//
//
//        databaseHelper.insertPlaces(1983,108,"Tunduma");
//
//        databaseHelper.insertPlaces(1984,109,"Chagongwe ");
//        databaseHelper.insertPlaces(1985,109,"Chakwale");
//        databaseHelper.insertPlaces(1986,109,"Chanjale ");
//        databaseHelper.insertPlaces(1987,109,"Gairo");
//        databaseHelper.insertPlaces(1988,109,"Idibo");
//        databaseHelper.insertPlaces(1989,109,"Iyogwe");
//        databaseHelper.insertPlaces(1990,109,"Kibedya");
//        databaseHelper.insertPlaces(1991,109,"Mandege");
//        databaseHelper.insertPlaces(1992,109,"Msingisi");
//        databaseHelper.insertPlaces(1993,109," Nongwe");
//        databaseHelper.insertPlaces(1994,109,"Rubeho");
//
//        databaseHelper.insertPlaces(1995,110,"Chisano");
//        databaseHelper.insertPlaces(1996,110,"Chita");
//        databaseHelper.insertPlaces(1997,110,"Idete");
//        databaseHelper.insertPlaces(1998,110,"Ifakara");
//        databaseHelper.insertPlaces(1999,110,"Kamwene");
//        databaseHelper.insertPlaces(2000,110,"Kibaoni");
//        databaseHelper.insertPlaces(2001,110,"Kiberege");
//        databaseHelper.insertPlaces(2002,110,"Kidatu");
//        databaseHelper.insertPlaces(2003,110,"Kisawasawa");
//        databaseHelper.insertPlaces(2004,110,"Lumemo");
//        databaseHelper.insertPlaces(2005,110,"Mang'ula");
//        databaseHelper.insertPlaces(2006,110,"Masagati");
//        databaseHelper.insertPlaces(2007,110,"Mbingu");
//        databaseHelper.insertPlaces(2008,110,"Mchombe");
//        databaseHelper.insertPlaces(2009,110,"Michenga");
//        databaseHelper.insertPlaces(2010,110,"Mkula");
//        databaseHelper.insertPlaces(2011,110,"Mlimba");
//        databaseHelper.insertPlaces(2012,110,"Mngeta");
//        databaseHelper.insertPlaces(2013,110,"Mofu");
//        databaseHelper.insertPlaces(2014,110,"Mwaya");
//        databaseHelper.insertPlaces(2015,110,"Sanje");
//        databaseHelper.insertPlaces(2016,110,"Uchindile");
//        databaseHelper.insertPlaces(2017,110,"Utengule (Kilombero)");
//
//
//        databaseHelper.insertPlaces(2018,111,"Berega");
//        databaseHelper.insertPlaces(2019,111,"Chanzulu");
//        databaseHelper.insertPlaces(2020,111,"Dumila");
//        databaseHelper.insertPlaces(2021,111,"Kasiki");
//        databaseHelper.insertPlaces(2022,111,"Kidete");
//        databaseHelper.insertPlaces(2023,111,"Kidodi");
//        databaseHelper.insertPlaces(2024,111,"Kilangali");
//        databaseHelper.insertPlaces(2025,111,"Kimamba A ");
//        databaseHelper.insertPlaces(2026,111,"Kimamba B ");
//        databaseHelper.insertPlaces(2027,111,"Kisanga");
//        databaseHelper.insertPlaces(2028,111,"Kitete");
//        databaseHelper.insertPlaces(2029,111,"Lumbiji");
//        databaseHelper.insertPlaces(2030,111,"Mabula");
//        databaseHelper.insertPlaces(2031,111,"Mabwerebwere");
//        databaseHelper.insertPlaces(2032,111,"Madoto");
//        databaseHelper.insertPlaces(2033,111,"Magole");
//        databaseHelper.insertPlaces(2034,111,"Magomeni");
//        databaseHelper.insertPlaces(2035,111,"Magubike");
//        databaseHelper.insertPlaces(2036,111,"Maguha");
//        databaseHelper.insertPlaces(2037,111,"Malolo");
//        databaseHelper.insertPlaces(2038,111,"Mamboya");
//        databaseHelper.insertPlaces(2039,111,"Masanze");
//        databaseHelper.insertPlaces(2040,111,"Mbumi");
//        databaseHelper.insertPlaces(2041,111,"Mikumi");
//        databaseHelper.insertPlaces(2042,111,"Mkwatani");
//        databaseHelper.insertPlaces(2043,111,"Msowero");
//        databaseHelper.insertPlaces(2044,111,"Ruaha");
//        databaseHelper.insertPlaces(2045,111,"Rudewa");
//        databaseHelper.insertPlaces(2046,111,"Ruhembe");
//        databaseHelper.insertPlaces(2047,111,"Tindiga");
//        databaseHelper.insertPlaces(2048,111,"Ulaya");
//        databaseHelper.insertPlaces(2049,111,"Uleling`ombe");
//        databaseHelper.insertPlaces(2050,111,"Vidunda");
//        databaseHelper.insertPlaces(2051,111,"Zombo");
//
//        databaseHelper.insertPlaces(2052,112,"Bigwa");
//        databaseHelper.insertPlaces(2053,112,"Boma");
//        databaseHelper.insertPlaces(2054,112,"Chamwino");
//        databaseHelper.insertPlaces(2055,112,"Kauzeni");
//        databaseHelper.insertPlaces(2056,112,"Kichangani");
//        databaseHelper.insertPlaces(2057,112,"Kihonda");
//        databaseHelper.insertPlaces(2058,112,"Kihonda Maghorofani");
//        databaseHelper.insertPlaces(2059,112,"Kilakala");
//        databaseHelper.insertPlaces(2060,112,"Kingo");
//        databaseHelper.insertPlaces(2061,112,"Kingolwira");
//        databaseHelper.insertPlaces(2062,112,"Kiwanja cha Ndege");
//        databaseHelper.insertPlaces(2063,112,"Luhungo");
//        databaseHelper.insertPlaces(2064,112,"Lukobe");
//        databaseHelper.insertPlaces(2065,112,"Mafiga");
//        databaseHelper.insertPlaces(2066,112,"Mafisa");
//        databaseHelper.insertPlaces(2067,112,"Magadu");
//        databaseHelper.insertPlaces(2068,112,"Mazimbu");
//        databaseHelper.insertPlaces(2069,112,"Mbuyuni");
//        databaseHelper.insertPlaces(2070,112,"Mindu");
//        databaseHelper.insertPlaces(2071,112,"Mji Mkuu");
//        databaseHelper.insertPlaces(2072,112,"Mji Mpya");
//        databaseHelper.insertPlaces(2073,112,"Mkundi ");
//        databaseHelper.insertPlaces(2074,112,"Mlimani");
//        databaseHelper.insertPlaces(2075,112,"Mwembesongo");
//        databaseHelper.insertPlaces(2076,112,"Mzinga");
//        databaseHelper.insertPlaces(2077,112,"Sabasaba");
//        databaseHelper.insertPlaces(2078,112,"Sultan Area");
//        databaseHelper.insertPlaces(2079,112,"Tungi");
//        databaseHelper.insertPlaces(2080,112,"Uwanja wa Taifa");
//
//        databaseHelper.insertPlaces(2081,113,"Bungu");
//        databaseHelper.insertPlaces(2082,113," Bwakila Chini ");
//        databaseHelper.insertPlaces(2083,113,"Bwakila Juu ");
//        databaseHelper.insertPlaces(2084,113,"Gwata");
//        databaseHelper.insertPlaces(2085,113,"Kasanga");
//        databaseHelper.insertPlaces(2086,113,"Kibogwa");
//        databaseHelper.insertPlaces(2087,113,"Kibungo Juu");
//        databaseHelper.insertPlaces(2088,113,"Kidugalo");
//        databaseHelper.insertPlaces(2089,113,"Kinole");
//        databaseHelper.insertPlaces(2090,113,"Kiroka");
//        databaseHelper.insertPlaces(2091,113,"Kisaki");
//        databaseHelper.insertPlaces(2092,113,"Kisemu");
//        databaseHelper.insertPlaces(2093,113,"Kolero");
//        databaseHelper.insertPlaces(2094,113,"Konde (Morogoro) ");
//        databaseHelper.insertPlaces(2095,113,"Lundi");
//        databaseHelper.insertPlaces(2096,113,"Matuli");
//        databaseHelper.insertPlaces(2097,113,"Mikese");
//        databaseHelper.insertPlaces(2098,113,"Mkambalani");
//        databaseHelper.insertPlaces(2099,113," Mkulazi");
//        databaseHelper.insertPlaces(2100,113,"Mkuyuni");
//        databaseHelper.insertPlaces(2101,113,"Mngazi");
//        databaseHelper.insertPlaces(2102,113,"Mtombozi");
//        databaseHelper.insertPlaces(2103,113,"Mvuha");
//        databaseHelper.insertPlaces(2104,113,"Ngerengere");
//        databaseHelper.insertPlaces(2105,113,"Selembala");
//        databaseHelper.insertPlaces(2106,113,"Singisa");
//        databaseHelper.insertPlaces(2107,113,"Tawa");
//        databaseHelper.insertPlaces(2108,113,"Tegetero");
//        databaseHelper.insertPlaces(2109,113,"Tununguo");
//
//        databaseHelper.insertPlaces(2110,114,"Bunduki");
//        databaseHelper.insertPlaces(2111,114,"Diongoya");
//        databaseHelper.insertPlaces(2112,114,"Doma");
//        databaseHelper.insertPlaces(2113,114,"Hembeti");
//        databaseHelper.insertPlaces(2114,114,"Kanga");
//        databaseHelper.insertPlaces(2115,114,"Kibati");
//        databaseHelper.insertPlaces(2116,114,"Kikeo");
//        databaseHelper.insertPlaces(2117,114,"Langali");
//        databaseHelper.insertPlaces(2118,114,"Maskati");
//        databaseHelper.insertPlaces(2119,114,"Melela");
//        databaseHelper.insertPlaces(2120,114,"Mhonda");
//        databaseHelper.insertPlaces(2121,114,"Mlali");
//        databaseHelper.insertPlaces(2122,114,"Mtibwa");
//        databaseHelper.insertPlaces(2123,114,"Mvomero");
//        databaseHelper.insertPlaces(2124,114,"Mzumbe");
//        databaseHelper.insertPlaces(2125,114,"Nyandira");
//        databaseHelper.insertPlaces(2126,114,"Sungaji");
//        databaseHelper.insertPlaces(2127,114,"Tchenzema");
//
//        databaseHelper.insertPlaces(2128,115,"Biro");
//        databaseHelper.insertPlaces(2129,115,"Chirombola");
//        databaseHelper.insertPlaces(2130,115,"Euga");
//        databaseHelper.insertPlaces(2131,115,"Ilonga");
//        databaseHelper.insertPlaces(2132,115,"Iragua");
//        databaseHelper.insertPlaces(2133,115,"Isongo");
//        databaseHelper.insertPlaces(2134,115,"Itete");
//        databaseHelper.insertPlaces(2135,115,"Kichangani");
//        databaseHelper.insertPlaces(2136,115,"Kilosa Mpepo ");
//        databaseHelper.insertPlaces(2137,115,"Lukande");
//        databaseHelper.insertPlaces(2138,115,"Lupiro");
//        databaseHelper.insertPlaces(2139,115,"Mahenge (mji)");
//        databaseHelper.insertPlaces(2140,115,"Malinyi");
//        databaseHelper.insertPlaces(2141,115,"Mbuga");
//        databaseHelper.insertPlaces(2142,115,"Minepa");
//        databaseHelper.insertPlaces(2143,115,"Msogezi");
//        databaseHelper.insertPlaces(2144,115,"Mtimbira");
//        databaseHelper.insertPlaces(2145,115,"Mwaya");
//        databaseHelper.insertPlaces(2146,115,"Ngoheranga");
//        databaseHelper.insertPlaces(2147,115,"Ruaha");
//        databaseHelper.insertPlaces(2148,115,"Sali");
//        databaseHelper.insertPlaces(2149,115,"Sofi");
//        databaseHelper.insertPlaces(2150,115,"Usangule");
//        databaseHelper.insertPlaces(2151,115,"Vigoi");
//
//        databaseHelper.insertPlaces(2152,116,"Jida ");
//        databaseHelper.insertPlaces(2153,116,"Marika");
//        databaseHelper.insertPlaces(2154,116,"Migongo");
//        databaseHelper.insertPlaces(2155,116,"Mkomaindo");
//        databaseHelper.insertPlaces(2156,116,"Mkuti");
//        databaseHelper.insertPlaces(2157,116,"Mpindimbi");
//        databaseHelper.insertPlaces(2158,116,"Mtandi ");
//        databaseHelper.insertPlaces(2159,116,"Mwenge (Masasi)");
//        databaseHelper.insertPlaces(2160,116,"Mwenge Mtapika");
//        databaseHelper.insertPlaces(2161,116,"Nyasa (Masasi)");
//        databaseHelper.insertPlaces(2162,116,"Sululu (Masasi)");
//        databaseHelper.insertPlaces(2163,116,"Temeke (Masasi)");
//
//        databaseHelper.insertPlaces(2164,117,"Chigugu");
//        databaseHelper.insertPlaces(2165,117,"Chikolopola");
//        databaseHelper.insertPlaces(2166,117,"Chiungutwa");
//        databaseHelper.insertPlaces(2167,117,"Chiwale");
//        databaseHelper.insertPlaces(2168,117,"Chiwata ");
//        databaseHelper.insertPlaces(2169,117,"Lipumburu");
//        databaseHelper.insertPlaces(2170,117,"Lukuledi");
//        databaseHelper.insertPlaces(2171,117,"Lulindi");
//        databaseHelper.insertPlaces(2172,117,"Mbuyuni");
//        databaseHelper.insertPlaces(2173,117,"Mchauru");
//        databaseHelper.insertPlaces(2174,117,"Mkululu");
//        databaseHelper.insertPlaces(2175,117,"Mkundi");
//        databaseHelper.insertPlaces(2176,117,"Mlingula");
//        databaseHelper.insertPlaces(2177,117,"Mnavira");
//        databaseHelper.insertPlaces(2178,117,"Mpanyani");
//        databaseHelper.insertPlaces(2179,117,"Mwena");
//        databaseHelper.insertPlaces(2180,117,"Namajani");
//        databaseHelper.insertPlaces(2181,117,"Namalenga");
//        databaseHelper.insertPlaces(2182,117,"Namatutwe");
//        databaseHelper.insertPlaces(2183,117,"Nanganga ");
//        databaseHelper.insertPlaces(2184,117,"Nanjota");
//        databaseHelper.insertPlaces(2185,117,"Sindano");
//
//        databaseHelper.insertPlaces(2186,118,"Chawi");
//        databaseHelper.insertPlaces(2187,118,"Dihimba");
//        databaseHelper.insertPlaces(2188,118,"Kiromba");
//        databaseHelper.insertPlaces(2189,118,"Kitaya");
//        databaseHelper.insertPlaces(2190,118,"Kitere");
//        databaseHelper.insertPlaces(2191,118,"Kiyanga");
//        databaseHelper.insertPlaces(2192,118,"Libobe");
//        databaseHelper.insertPlaces(2193,118,"Madimba");
//        databaseHelper.insertPlaces(2194,118,"Mahurunga");
//        databaseHelper.insertPlaces(2195,118,"Mayanga");
//        databaseHelper.insertPlaces(2196,118,"Mbawala (Mtwara)");
//        databaseHelper.insertPlaces(2197,118,"Mbembaleo");
//        databaseHelper.insertPlaces(2198,118,"Milangominne");
//        databaseHelper.insertPlaces(2199,118,"Mnima");
//        databaseHelper.insertPlaces(2200,118,"Mpapura");
//        databaseHelper.insertPlaces(2201,118,"Msanga Mkuu");
//        databaseHelper.insertPlaces(2202,118,"Mtimbwilimbwi");
//        databaseHelper.insertPlaces(2203,118,"Mtiniko");
//        databaseHelper.insertPlaces(2204,118,"Muungano (Mtwara)");
//        databaseHelper.insertPlaces(2205,118,"Namtumbuka");
//        databaseHelper.insertPlaces(2206,118,"Nanguruwe");
//        databaseHelper.insertPlaces(2207,118,"Nanyamba");
//        databaseHelper.insertPlaces(2208,118,"Naumbu");
//        databaseHelper.insertPlaces(2209,118,"Ndumbwe ");
//        databaseHelper.insertPlaces(2210,118,"Nitekela ");
//        databaseHelper.insertPlaces(2211,118,"Njengwa ");
//        databaseHelper.insertPlaces(2212,118,"Tangazo (Mtwara) ");
//        databaseHelper.insertPlaces(2213,118,"Ziwani");
//
//        databaseHelper.insertPlaces(2214,119,"Chikongola");
//        databaseHelper.insertPlaces(2215,119,"Chuno");
//        databaseHelper.insertPlaces(2216,119,"Jangwani");
//        databaseHelper.insertPlaces(2217,119,"Kisungule");
//        databaseHelper.insertPlaces(2218,119,"Likombe");
//        databaseHelper.insertPlaces(2219,119,"Magengeni ");
//        databaseHelper.insertPlaces(2220,119,"Majengo");
//        databaseHelper.insertPlaces(2221,119,"Mitengo");
//        databaseHelper.insertPlaces(2222,119,"Mtonya");
//        databaseHelper.insertPlaces(2223,119,"Naliendele");
//        databaseHelper.insertPlaces(2224,119,"Rahaleo");
//        databaseHelper.insertPlaces(2225,119,"Railway");
//        databaseHelper.insertPlaces(2226,119,"Shangani");
//        databaseHelper.insertPlaces(2227,119,"Ufukoni ");
//        databaseHelper.insertPlaces(2228,119,"Vigaeni");
//
//        databaseHelper.insertPlaces(2229,120,"Chipuputa ");
//        databaseHelper.insertPlaces(2230,120,"Likokona ");
//        databaseHelper.insertPlaces(2231,120,"Lumesule");
//        databaseHelper.insertPlaces(2232,120,"Mangaka ");
//        databaseHelper.insertPlaces(2233,120,"Maratani ");
//        databaseHelper.insertPlaces(2234,120,"Masuguru (Nanyumbu) ");
//        databaseHelper.insertPlaces(2235,120,"Mikangaula");
//        databaseHelper.insertPlaces(2236,120,"Mkonona");
//        databaseHelper.insertPlaces(2237,120,"Mnanje");
//        databaseHelper.insertPlaces(2238,120,"Nandete");
//        databaseHelper.insertPlaces(2239,120,"Nangomba");
//        databaseHelper.insertPlaces(2240,120,"Nanyumbu");
//        databaseHelper.insertPlaces(2241,120," Napacho");
//        databaseHelper.insertPlaces(2242,120,"Sengenya");
//
//        databaseHelper.insertPlaces(2243,121,"Chihangu");
//        databaseHelper.insertPlaces(2244,121,"Chilangalanga");
//        databaseHelper.insertPlaces(2245,121,"Chitekete");
//        databaseHelper.insertPlaces(2246,121,"Chiwonga");
//        databaseHelper.insertPlaces(2247,121,"Kitangari");
//        databaseHelper.insertPlaces(2248,121,"Luchingu");
//        databaseHelper.insertPlaces(2249,121,"Makonga");
//        databaseHelper.insertPlaces(2250,121,"Makote");
//        databaseHelper.insertPlaces(2251,121,"Makukwe");
//        databaseHelper.insertPlaces(2252,121,"Malatu");
//        databaseHelper.insertPlaces(2253,121,"Maputi");
//        databaseHelper.insertPlaces(2254,121,"Mchemo");
//        databaseHelper.insertPlaces(2255,121,"Mcholi I");
//        databaseHelper.insertPlaces(2256,121,"Mcholi II ");
//        databaseHelper.insertPlaces(2257,121,"Mdimbampelempele");
//        databaseHelper.insertPlaces(2258,121,"Mkoma II ");
//        databaseHelper.insertPlaces(2259,121,"Mkunya");
//        databaseHelper.insertPlaces(2260,121,"Mkwedu");
//        databaseHelper.insertPlaces(2261,121,"Mnekachi");
//        databaseHelper.insertPlaces(2262,121,"Mnyambe");
//        databaseHelper.insertPlaces(2263,121,"Mtonya (Newala)");
//        databaseHelper.insertPlaces(2264,121,"Mtopwa");
//        databaseHelper.insertPlaces(2265,121,"Mtunguru");
//        databaseHelper.insertPlaces(2266,121,"Nakahako");
//        databaseHelper.insertPlaces(2267,121,"Nambali");
//        databaseHelper.insertPlaces(2268,121,"Namiyonga");
//        databaseHelper.insertPlaces(2269,121,"Nandwahi");
//        databaseHelper.insertPlaces(2270,121,"Nanguruwe");
//
//
//        databaseHelper.insertPlaces(2271,122,"Chaume ");
//        databaseHelper.insertPlaces(2272,122,"Chikongola");
//        databaseHelper.insertPlaces(2273,122,"Chingungwe ");
//        databaseHelper.insertPlaces(2274,122,"Dinduma");
//        databaseHelper.insertPlaces(2275,122,"Kitama (Tandahimba) ");
//        databaseHelper.insertPlaces(2276,122,"Kwanyama");
//        databaseHelper.insertPlaces(2277,122,"Litehu");
//        databaseHelper.insertPlaces(2278,122,"Luagala");
//        databaseHelper.insertPlaces(2279,122,"Lukokoda ");
//        databaseHelper.insertPlaces(2280,122,"Lyenje");
//        databaseHelper.insertPlaces(2281,122,"Mahuta ");
//        databaseHelper.insertPlaces(2282,122,"Maundo ");
//        databaseHelper.insertPlaces(2283,122,"Mchichira");
//        databaseHelper.insertPlaces(2284,122,"Mdimbamnyoma");
//        databaseHelper.insertPlaces(2285,122,"Mdumbwe");
//        databaseHelper.insertPlaces(2286,122,"Michenjele");
//        databaseHelper.insertPlaces(2287,122,"Mihambwe");
//        databaseHelper.insertPlaces(2288,122,"Mihuta");
//        databaseHelper.insertPlaces(2289,122,"Milingodi");
//        databaseHelper.insertPlaces(2290,122,"Mkonjowano");
//        databaseHelper.insertPlaces(2291,122,"Mkoreha");
//        databaseHelper.insertPlaces(2292,122,"Mkundi (Tandahimba)");
//        databaseHelper.insertPlaces(2293,122,"Mkwiti");
//        databaseHelper.insertPlaces(2294,122,"Mnyawa");
//        databaseHelper.insertPlaces(2295,122,"Nambahu");
//        databaseHelper.insertPlaces(2296,122,"Namikupa");
//        databaseHelper.insertPlaces(2297,122,"Nanhyanga ");
//        databaseHelper.insertPlaces(2298,122,"Naputa");
//        databaseHelper.insertPlaces(2299,122,"Ngunja");
//        databaseHelper.insertPlaces(2300,122,"Tandahimba");
//
//        databaseHelper.insertPlaces(2301,123,"Bugogwa");
//        databaseHelper.insertPlaces(2302,123,"Buswelu");
//        databaseHelper.insertPlaces(2303,123,"Ilemela ");
//        databaseHelper.insertPlaces(2304,123,"Kirumba ");
//        databaseHelper.insertPlaces(2305,123,"Kitangiri");
//        databaseHelper.insertPlaces(2306,123,"Nyakato ");
//        databaseHelper.insertPlaces(2307,123,"Nyamanoro");
//        databaseHelper.insertPlaces(2308,123,"Pasiansi");
//        databaseHelper.insertPlaces(2309,123,"Sangabuye");
//
//        databaseHelper.insertPlaces(2310,124,"Buhongwa");
//        databaseHelper.insertPlaces(2311,124,"Butimba");
//        databaseHelper.insertPlaces(2312,124,"Igogo");
//        databaseHelper.insertPlaces(2313,124,"Igoma");
//        databaseHelper.insertPlaces(2314,124,"Isamilo");
//        databaseHelper.insertPlaces(2315,124,"Mahina");
//        databaseHelper.insertPlaces(2316,124,"Mbugani");
//        databaseHelper.insertPlaces(2317,124,"Mikuyuni");
//        databaseHelper.insertPlaces(2318,124,"Mirongo");
//        databaseHelper.insertPlaces(2319,124,"Mkolani");
//        databaseHelper.insertPlaces(2320,124,"Nyamagana");
//        databaseHelper.insertPlaces(2321,124,"Pamba");
//
//        databaseHelper.insertPlaces(2322,125,"Bulyaheke");
//        databaseHelper.insertPlaces(2323,125,"Bupandwa");
//        databaseHelper.insertPlaces(2324,125,"Busisi");
//        databaseHelper.insertPlaces(2325,125,"Buyagu");
//        databaseHelper.insertPlaces(2326,125,"Buzilasoga");
//        databaseHelper.insertPlaces(2327,125,"Chifunfu");
//        databaseHelper.insertPlaces(2328,125,"Ibisabageni");
//        databaseHelper.insertPlaces(2329,125,"Igalula");
//        databaseHelper.insertPlaces(2330,125,"Igulumuki");
//        databaseHelper.insertPlaces(2331,125,"Irenza");
//        databaseHelper.insertPlaces(2332,125,"Kafunzo");
//        databaseHelper.insertPlaces(2333,125,"Kagunga ");
//        databaseHelper.insertPlaces(2334,125,"Kalebezo");
//        databaseHelper.insertPlaces(2335,125,"Kasenyi");
//        databaseHelper.insertPlaces(2336,125,"Kasungamile");
//        databaseHelper.insertPlaces(2337,125,"Katunguru");
//        databaseHelper.insertPlaces(2338,125,"Katwe");
//        databaseHelper.insertPlaces(2339,125,"Kazunzu");
//        databaseHelper.insertPlaces(2340,125,"Kishinda");
//        databaseHelper.insertPlaces(2341,125,"Lugata");
//        databaseHelper.insertPlaces(2342,125,"Maisome");
//        databaseHelper.insertPlaces(2343,125,"Mwabaluhi");
//        databaseHelper.insertPlaces(2344,125,"Nyakaliro");
//        databaseHelper.insertPlaces(2345,125,"Nyakasasa");
//        databaseHelper.insertPlaces(2346,125,"Nyakasungwa");
//        databaseHelper.insertPlaces(2347,125,"Nyamatongo");
//        databaseHelper.insertPlaces(2348,125,"Nyamazugo");
//        databaseHelper.insertPlaces(2349,125,"Nyampande");
//        databaseHelper.insertPlaces(2350,125,"Nyampulukano");
//        databaseHelper.insertPlaces(2351,125,"Nyanzenda");
//        databaseHelper.insertPlaces(2352,125,"Nyatukala");
//        databaseHelper.insertPlaces(2353,125,"Nyehunge");
//        databaseHelper.insertPlaces(2354,125,"Sima");
//        databaseHelper.insertPlaces(2355,125,"Tabaruka");
//
//        databaseHelper.insertPlaces(2356,126,"Bugando");
//        databaseHelper.insertPlaces(2357,126,"Bungulwa");
//        databaseHelper.insertPlaces(2358,126,"Bupamwa");
//        databaseHelper.insertPlaces(2359,126,"Fukalo");
//        databaseHelper.insertPlaces(2360,126,"Hungumalwa");
//        databaseHelper.insertPlaces(2361,126,"Igongwa");
//        databaseHelper.insertPlaces(2362,126,"Ilula");
//        databaseHelper.insertPlaces(2363,126,"Iseni");
//        databaseHelper.insertPlaces(2364,126,"Kikubiji");
//        databaseHelper.insertPlaces(2365,126,"Lyoma");
//        databaseHelper.insertPlaces(2366,126,"Maligisu");
//        databaseHelper.insertPlaces(2367,126,"Malya");
//        databaseHelper.insertPlaces(2368,126,"Mantare ");
//        databaseHelper.insertPlaces(2369,126,"Mhande ");
//        databaseHelper.insertPlaces(2370,126,"Mwabomba ");
//        databaseHelper.insertPlaces(2371,126,"Mwagi");
//        databaseHelper.insertPlaces(2372,126,"Mwakilyambiti ");
//        databaseHelper.insertPlaces(2373,126,"Mwamala");
//        databaseHelper.insertPlaces(2374,126,"Mwandu");
//        databaseHelper.insertPlaces(2375,126,"Mwang'halanga");
//        databaseHelper.insertPlaces(2376,126,"Mwankulwe");
//        databaseHelper.insertPlaces(2377,126,"Ng'hundi");
//        databaseHelper.insertPlaces(2378,126,"Ngudu");
//        databaseHelper.insertPlaces(2379,126,"Ngulla");
//        databaseHelper.insertPlaces(2380,126,"Nkalalo");
//        databaseHelper.insertPlaces(2381,126,"Nyambiti");
//        databaseHelper.insertPlaces(2382,126,"Nyamilama");
//        databaseHelper.insertPlaces(2383,126,"Shilembo");
//        databaseHelper.insertPlaces(2384,126,"Sumve");
//        databaseHelper.insertPlaces(2385,126,"Walla");
//
//
//        databaseHelper.insertPlaces(2386,127,"Bujashi");
//        databaseHelper.insertPlaces(2387,127,"Bukwe");
//        databaseHelper.insertPlaces(2388,127,"Jinjimili");
//        databaseHelper.insertPlaces(2389,127,"Kahangara");
//        databaseHelper.insertPlaces(2390,127,"Kisesa");
//        databaseHelper.insertPlaces(2391,127,"Kitongo Sima ");
//        databaseHelper.insertPlaces(2392,127,"Kongolo");
//        databaseHelper.insertPlaces(2393,127,"Lubugu");
//        databaseHelper.insertPlaces(2394,127,"Lutale");
//        databaseHelper.insertPlaces(2395,127,"Magu Mjini");
//        databaseHelper.insertPlaces(2396,127,"Mwamabanza");
//        databaseHelper.insertPlaces(2397,127,"Mwamanga");
//        databaseHelper.insertPlaces(2398,127,"Ng'haya ");
//        databaseHelper.insertPlaces(2399,127,"Nkungulu");
//        databaseHelper.insertPlaces(2400,127,"Nyanguge");
//        databaseHelper.insertPlaces(2401,127,"Nyigogo");
//        databaseHelper.insertPlaces(2402,127,"Shishani");
//        databaseHelper.insertPlaces(2403,127,"Sukuma");
//
//        databaseHelper.insertPlaces(2404,128,"Buhingo");
//        databaseHelper.insertPlaces(2405,128,"Bulemeji");
//        databaseHelper.insertPlaces(2406,128,"Busongo");
//        databaseHelper.insertPlaces(2407,128,"Fella");
//        databaseHelper.insertPlaces(2408,128,"Gulumungu");
//        databaseHelper.insertPlaces(2409,128,"Idetemya");
//        databaseHelper.insertPlaces(2410,128,"Igokelo");
//        databaseHelper.insertPlaces(2411,128," Ilalambogo ");
//        databaseHelper.insertPlaces(2412,128,"Ilujamate");
//        databaseHelper.insertPlaces(2413,128," Isesa");
//        databaseHelper.insertPlaces(2414,128,"Kanyelele");
//        databaseHelper.insertPlaces(2415,128,"Kasololo");
//        databaseHelper.insertPlaces(2416,128,"Kijima");
//        databaseHelper.insertPlaces(2417,128,"Koromije");
//        databaseHelper.insertPlaces(2418,128,"Lubili");
//        databaseHelper.insertPlaces(2419,128,"Mabuki");
//        databaseHelper.insertPlaces(2420,128,"Mamaye");
//        databaseHelper.insertPlaces(2421,128,"Mbarika");
//        databaseHelper.insertPlaces(2422,128,"Misasi");
//        databaseHelper.insertPlaces(2423,128,"Misungwi");
//        databaseHelper.insertPlaces(2424,128,"Mondo");
//        databaseHelper.insertPlaces(2425,128,"Mwaniko");
//        databaseHelper.insertPlaces(2426,128,"Nhundulu");
//        databaseHelper.insertPlaces(2427,128,"Shilalo ");
//        databaseHelper.insertPlaces(2428,128,"Sumbugu");
//        databaseHelper.insertPlaces(2429,128,"Ukiriguru");
//        databaseHelper.insertPlaces(2430,128,"Usagara");
//
//        databaseHelper.insertPlaces(2431,129,"Bukanda");
//        databaseHelper.insertPlaces(2432,129,"Bukiko");
//        databaseHelper.insertPlaces(2433,129,"Bukindo");
//        databaseHelper.insertPlaces(2434,129,"Bukongo");
//        databaseHelper.insertPlaces(2435,129,"Bukungu");
//        databaseHelper.insertPlaces(2436,129,"Bwiro");
//        databaseHelper.insertPlaces(2437,129,"Bwisya");
//        databaseHelper.insertPlaces(2438,129,"Igalla");
//        databaseHelper.insertPlaces(2439,129,"Ilangala");
//        databaseHelper.insertPlaces(2440,129,"Irugwa");
//        databaseHelper.insertPlaces(2441,129,"Kagera ");
//        databaseHelper.insertPlaces(2442,129,"Kagunguli");
//        databaseHelper.insertPlaces(2443,129,"Kakerege");
//        databaseHelper.insertPlaces(2444,129,"Mukituntu");
//        databaseHelper.insertPlaces(2445,129,"Muriti");
//        databaseHelper.insertPlaces(2446,129,"Murutunguru");
//        databaseHelper.insertPlaces(2447,129,"Nakatunguru");
//        databaseHelper.insertPlaces(2448,129,"Namagondo");
//        databaseHelper.insertPlaces(2449,129,"Namilembe");
//        databaseHelper.insertPlaces(2450,129,"Nansio");
//        databaseHelper.insertPlaces(2451,129,"Nduruma ");
//        databaseHelper.insertPlaces(2452,129,"Ngoma");
//        databaseHelper.insertPlaces(2453,129,"Nkilizya");
//        databaseHelper.insertPlaces(2454,129,"Nyamanga");
//
//
//        databaseHelper.insertPlaces(2455,130,"Ibumi");
//        databaseHelper.insertPlaces(2456,130,"Iwela");
//        databaseHelper.insertPlaces(2457,130,"Kilondo");
//        databaseHelper.insertPlaces(2458,130,"Lifuma");
//        databaseHelper.insertPlaces(2459,130,"Luana");
//        databaseHelper.insertPlaces(2460,130,"Ludende");
//        databaseHelper.insertPlaces(2461,130,"Ludewa");
//        databaseHelper.insertPlaces(2462,130,"Lugarawa");
//        databaseHelper.insertPlaces(2463,130,"Luilo");
//        databaseHelper.insertPlaces(2464,130,"Lumbila");
//        databaseHelper.insertPlaces(2465,130,"Lupanga");
//        databaseHelper.insertPlaces(2466,130,"Lupingu");
//        databaseHelper.insertPlaces(2467,130,"Madilu");
//        databaseHelper.insertPlaces(2468,130,"Madope");
//        databaseHelper.insertPlaces(2469,130,"Makonde (Ludewa)");
//        databaseHelper.insertPlaces(2470,130,"Manda (Ludewa)");
//        databaseHelper.insertPlaces(2471,130,"Masasi (Ludewa) ");
//        databaseHelper.insertPlaces(2472,130,"Mavanga");
//        databaseHelper.insertPlaces(2473,130,"Mawengi");
//        databaseHelper.insertPlaces(2474,130,"Milo");
//        databaseHelper.insertPlaces(2475,130,"Mkongobaki");
//        databaseHelper.insertPlaces(2476,130,"Mlangali (Ludewa)");
//        databaseHelper.insertPlaces(2477,130,"Mundindi");
//        databaseHelper.insertPlaces(2478,130,"Nkomang'ombe");
//        databaseHelper.insertPlaces(2479,130,"Ruhuhu");
//
//
//        databaseHelper.insertPlaces(2480,131,"Kitandililo");
//        databaseHelper.insertPlaces(2481,131,"Lyamkena");
//        databaseHelper.insertPlaces(2482,131,"Mahongole");
//        databaseHelper.insertPlaces(2483,131,"Mjimwema");
//        databaseHelper.insertPlaces(2484,131,"Mlowa (Makambako) ");
//        databaseHelper.insertPlaces(2485,131,"Mwembetogwa");
//        databaseHelper.insertPlaces(2486,131,"Ubena (Makambako)");
//        databaseHelper.insertPlaces(2487,131,"Utengule (Makambako)");
//
//        databaseHelper.insertPlaces(2488,132,"Bulongwa");
//        databaseHelper.insertPlaces(2489,132,"Ikuwo");
//        databaseHelper.insertPlaces(2490,132,"Iniho");
//        databaseHelper.insertPlaces(2491,132,"Ipelele");
//        databaseHelper.insertPlaces(2492,132,"Ipepo");
//        databaseHelper.insertPlaces(2493,132,"Isapulano");
//        databaseHelper.insertPlaces(2494,132,"Itundu");
//        databaseHelper.insertPlaces(2495,132,"Iwawa");
//        databaseHelper.insertPlaces(2496,132,"Kigala");
//        databaseHelper.insertPlaces(2497,132,"Kigulu");
//        databaseHelper.insertPlaces(2498,132,"Kipagilo");
//        databaseHelper.insertPlaces(2499,132,"Kitulo");
//        databaseHelper.insertPlaces(2500,132,"Lupalilo");
//        databaseHelper.insertPlaces(2501,132,"Lupila");
//        databaseHelper.insertPlaces(2502,132,"Luwumbu");
//        databaseHelper.insertPlaces(2503,132,"Mang'oto");
//        databaseHelper.insertPlaces(2504,132,"Matamba");
//        databaseHelper.insertPlaces(2505,132,"Mbalatse");
//        databaseHelper.insertPlaces(2506,132,"Mfumbi");
//        databaseHelper.insertPlaces(2507,132,"Mlondwe");
//        databaseHelper.insertPlaces(2508,132,"Tandala");
//        databaseHelper.insertPlaces(2509,132,"Ukwama");
//
//        databaseHelper.insertPlaces(2510,133,"Idamba");
//        databaseHelper.insertPlaces(2511,133,"Igongolo");
//        databaseHelper.insertPlaces(2512,133,"Ikondo");
//        databaseHelper.insertPlaces(2513,133,"Ikuna");
//        databaseHelper.insertPlaces(2514,133,"Kichiwa");
//        databaseHelper.insertPlaces(2515,133,"Kidegembye");
//        databaseHelper.insertPlaces(2516,133,"Lupembe");
//        databaseHelper.insertPlaces(2517,133,"Matembwe");
//        databaseHelper.insertPlaces(2518,133,"Mfriga");
//        databaseHelper.insertPlaces(2519,133," Mtwango");
//        databaseHelper.insertPlaces(2520,133,"Ninga");
//
//        databaseHelper.insertPlaces(2521,134,"Ihanga");
//        databaseHelper.insertPlaces(2522,134,"Iwungilo");
//        databaseHelper.insertPlaces(2523,134,"Kifanya");
//        databaseHelper.insertPlaces(2524,134,"Lugenge");
//        databaseHelper.insertPlaces(2525,134,"Luponde");
//        databaseHelper.insertPlaces(2526,134,"Makowo");
//        databaseHelper.insertPlaces(2527,134,"Matola");
//        databaseHelper.insertPlaces(2528,134,"Mji Mwema");
//        databaseHelper.insertPlaces(2529,134,"Njombe Mjini");
//        databaseHelper.insertPlaces(2530,134,"Ramadhani");
//        databaseHelper.insertPlaces(2531,134,"Utalingolo");
//        databaseHelper.insertPlaces(2532,134,"Uwemba");
//        databaseHelper.insertPlaces(2533,134,"Yakobi");
//
//        databaseHelper.insertPlaces(2534,135,"Igima");
//        databaseHelper.insertPlaces(2535,135,"Igosi");
//        databaseHelper.insertPlaces(2536,135,"Igwachanya");
//        databaseHelper.insertPlaces(2537,135,"Ilembula");
//        databaseHelper.insertPlaces(2538,135,"Imalinyi ");
//        databaseHelper.insertPlaces(2539,135,"Kidugala");
//        databaseHelper.insertPlaces(2540,135,"Kijombe");
//        databaseHelper.insertPlaces(2541,135,"Kipengele");
//        databaseHelper.insertPlaces(2542,135,"Luduga");
//        databaseHelper.insertPlaces(2543,135,"Makoga");
//        databaseHelper.insertPlaces(2544,135,"Mdandu");
//        databaseHelper.insertPlaces(2545,135,"Saja");
//        databaseHelper.insertPlaces(2546,135,"Uhambule");
//        databaseHelper.insertPlaces(2547,135,"Ulembwe");
//        databaseHelper.insertPlaces(2548,135,"Usuka");
//        databaseHelper.insertPlaces(2549,135,"Wangama");
//        databaseHelper.insertPlaces(2550,135,"Wanging'ombe");
//
//        databaseHelper.insertPlaces(2551,136,"Bwilingu");
//        databaseHelper.insertPlaces(2552,136,"Dunda");
//        databaseHelper.insertPlaces(2553,136,"Fukayosi");
//        databaseHelper.insertPlaces(2554,136,"Kerege |");
//        databaseHelper.insertPlaces(2555,136,"Kibindu");
//        databaseHelper.insertPlaces(2556,136,"Kimange ");
//        databaseHelper.insertPlaces(2557,136,"Kiromo ");
//        databaseHelper.insertPlaces(2558,136,"Kiwangwa");
//        databaseHelper.insertPlaces(2559,136,"Lugoba");
//        databaseHelper.insertPlaces(2560,136,"Magomeni");
//        databaseHelper.insertPlaces(2561,136,"Mandera ");
//        databaseHelper.insertPlaces(2562,136,"Mbwewe");
//        databaseHelper.insertPlaces(2563,136,"Miono");
//        databaseHelper.insertPlaces(2564,136,"Mkange");
//        databaseHelper.insertPlaces(2565,136,"Msata");
//        databaseHelper.insertPlaces(2566,136,"Msoga");
//        databaseHelper.insertPlaces(2567,136,"Pera");
//        databaseHelper.insertPlaces(2568,136,"Talawanda ");
//        databaseHelper.insertPlaces(2569,136,"Ubenazomozi");
//        databaseHelper.insertPlaces(2570,136,"Vigwaza");
//        databaseHelper.insertPlaces(2571,136,"Yombo");
//        databaseHelper.insertPlaces(2572,136,"Zinga");
//
//        databaseHelper.insertPlaces(2573,137,"Kibaha ");
//        databaseHelper.insertPlaces(2574,137,"Kongowe ");
//        databaseHelper.insertPlaces(2575,137,"Maili Moja ");
//        databaseHelper.insertPlaces(2576,137,"Mbwawa");
//        databaseHelper.insertPlaces(2577,137,"Misugusugu");
//        databaseHelper.insertPlaces(2578,137,"Mkuza");
//        databaseHelper.insertPlaces(2579,137,"Msangani ");
//        databaseHelper.insertPlaces(2580,137,"Pangani ");
//        databaseHelper.insertPlaces(2581,137,"Picha ya Ndege ");
//        databaseHelper.insertPlaces(2582,137,"Tumbi");
//        databaseHelper.insertPlaces(2583,137,"Visiga");
//
//        databaseHelper.insertPlaces(2584,138,"Bokomnemela");
//        databaseHelper.insertPlaces(2585,138,"Dutumi");
//        databaseHelper.insertPlaces(2586,138,"Gwata");
//        databaseHelper.insertPlaces(2587,138,"Janga");
//        databaseHelper.insertPlaces(2588,138,"Kikongo");
//        databaseHelper.insertPlaces(2589,138,"Kilangalanga");
//        databaseHelper.insertPlaces(2590,138,"Kwala ");
//        databaseHelper.insertPlaces(2591,138,"Magindu");
//        databaseHelper.insertPlaces(2592,138,"Mlandizi");
//        databaseHelper.insertPlaces(2593,138," Ruvu");
//        databaseHelper.insertPlaces(2594,138,"Soga");
//
//        databaseHelper.insertPlaces(2595,139,"Boga ");
//        databaseHelper.insertPlaces(2596,139,"Cholesamvula");
//        databaseHelper.insertPlaces(2597,139,"Kazimzumbwi ");
//        databaseHelper.insertPlaces(2598,139,"Kibuta");
//        databaseHelper.insertPlaces(2599,139,"Kiluvya");
//        databaseHelper.insertPlaces(2600,139,"Kisarawe ");
//        databaseHelper.insertPlaces(2601,139,"Kurui");
//        databaseHelper.insertPlaces(2602,139,"Mafizi");
//        databaseHelper.insertPlaces(2603,139,"Maneromango");
//        databaseHelper.insertPlaces(2604,139,"Marui");
//        databaseHelper.insertPlaces(2605,139,"Marumbo");
//        databaseHelper.insertPlaces(2606,139,"Masaki");
//        databaseHelper.insertPlaces(2607,139,"Msimbu");
//        databaseHelper.insertPlaces(2608,139,"Mzenga");
//        databaseHelper.insertPlaces(2609,139,"Vihingo ");
//        databaseHelper.insertPlaces(2610,139,"Vikumbulu");
//
//        databaseHelper.insertPlaces(2611,140,"Kanga ");
//        databaseHelper.insertPlaces(2612,140,"Kirongwe ");
//        databaseHelper.insertPlaces(2613,140,"Baleni");
//        databaseHelper.insertPlaces(2614,140,"Kilindoni");
//        databaseHelper.insertPlaces(2615,140,"Miburani ");
//        databaseHelper.insertPlaces(2616,140,"Kiegeani ");
//        databaseHelper.insertPlaces(2617,140,"Jibondo");
//        databaseHelper.insertPlaces(2618,140,"Ndagoni");
//
//        databaseHelper.insertPlaces(2619,141,"Bupu");
//        databaseHelper.insertPlaces(2620,141,"Kimanzichana");
//        databaseHelper.insertPlaces(2621,141,"Kiparang'anda ");
//        databaseHelper.insertPlaces(2622,141,"Kisiju");
//        databaseHelper.insertPlaces(2623,141,"Kitomondo");
//        databaseHelper.insertPlaces(2624,141,"Lukanga");
//        databaseHelper.insertPlaces(2625,141,"Magawa");
//        databaseHelper.insertPlaces(2626,141,"Mbezi");
//        databaseHelper.insertPlaces(2627,141,"Mkamba");
//        databaseHelper.insertPlaces(2628,141,"Mkuranga ");
//        databaseHelper.insertPlaces(2629,141,"Mwalusembe");
//        databaseHelper.insertPlaces(2630,141,"Njia Nne ");
//        databaseHelper.insertPlaces(2631,141,"Nyamato ");
//        databaseHelper.insertPlaces(2632,141,"Panzuo");
//        databaseHelper.insertPlaces(2633,141,"Shungubweni");
//        databaseHelper.insertPlaces(2634,141,"Tambani");
//        databaseHelper.insertPlaces(2635,141,"Tengelea");
//        databaseHelper.insertPlaces(2636,141,"Vianzi");
//        databaseHelper.insertPlaces(2637,141,"Vikindu");
//
//        databaseHelper.insertPlaces(2638,142,"Chemchem");
//        databaseHelper.insertPlaces(2639,142,"Chumbi ");
//        databaseHelper.insertPlaces(2640,142,"Ikwiriri");
//        databaseHelper.insertPlaces(2641,142,"Kipugira");
//        databaseHelper.insertPlaces(2642,142,"Mbwara");
//        databaseHelper.insertPlaces(2643,142,"Mgomba");
//        databaseHelper.insertPlaces(2644,142,"Mkongo");
//        databaseHelper.insertPlaces(2645,142,"Mohoro");
//        databaseHelper.insertPlaces(2646,142,"Mwaseni ");
//        databaseHelper.insertPlaces(2647,142,"Ngarambe");
//        databaseHelper.insertPlaces(2648,142,"Ngorongo");
//        databaseHelper.insertPlaces(2649,142,"Umwe");
//        databaseHelper.insertPlaces(2650,142,"Utete");
//
//        databaseHelper.insertPlaces(2651,143,"Kasanga ");
//        databaseHelper.insertPlaces(2652,143,"Katazi");
//        databaseHelper.insertPlaces(2653,143,"Katete");
//        databaseHelper.insertPlaces(2654,143,"Kilesha");
//        databaseHelper.insertPlaces(2655,143,"Kisumba");
//        databaseHelper.insertPlaces(2656,143,"Legeza Mwendo ");
//        databaseHelper.insertPlaces(2657,143,"Mambwekenya ");
//        databaseHelper.insertPlaces(2658,143,"Matai");
//        databaseHelper.insertPlaces(2659,143,"Mkali");
//        databaseHelper.insertPlaces(2660,143,"Mkowe");
//        databaseHelper.insertPlaces(2661,143,"Mnamba");
//        databaseHelper.insertPlaces(2662,143,"Msanzi");
//        databaseHelper.insertPlaces(2663,143,"Mwazye");
//        databaseHelper.insertPlaces(2664,143,"Mwembenkoswe ");
//        databaseHelper.insertPlaces(2665,143,"Mwimbi");
//        databaseHelper.insertPlaces(2666,143,"Sopa");
//        databaseHelper.insertPlaces(2667,143,"Ulumi");
//
//        databaseHelper.insertPlaces(2668,144,"Chala");
//        databaseHelper.insertPlaces(2669,144,"Isale");
//        databaseHelper.insertPlaces(2670,144,"Kabwe");
//        databaseHelper.insertPlaces(2671,144,"Kala ");
//        databaseHelper.insertPlaces(2672,144,"Kate");
//        databaseHelper.insertPlaces(2673,144,"Kipande");
//        databaseHelper.insertPlaces(2674,144,"Kipili");
//        databaseHelper.insertPlaces(2675,144,"Kirando");
//        databaseHelper.insertPlaces(2676,144,"Korongwe");
//        databaseHelper.insertPlaces(2677,144,"Mkwamba");
//        databaseHelper.insertPlaces(2678,144,"Mtenga");
//        databaseHelper.insertPlaces(2679,144,"Namanyere");
//        databaseHelper.insertPlaces(2680,144,"Ninde");
//        databaseHelper.insertPlaces(2681,144,"Nkandasi");
//        databaseHelper.insertPlaces(2682,144,"Nkomolo ");
//        databaseHelper.insertPlaces(2683,144,"Sintali ");
//        databaseHelper.insertPlaces(2684,144,"Wampembe");
//
//        databaseHelper.insertPlaces(2685,145,"Ilemba");
//        databaseHelper.insertPlaces(2686,145,"Kaengesa");
//        databaseHelper.insertPlaces(2687,145,"Kalambanzite");
//        databaseHelper.insertPlaces(2688,145,"Kaoze");
//        databaseHelper.insertPlaces(2689,145,"Kipeta ");
//        databaseHelper.insertPlaces(2690,145,"Laela ");
//        databaseHelper.insertPlaces(2691,145,"Lusaka");
//        databaseHelper.insertPlaces(2692,145,"Mfinga");
//        databaseHelper.insertPlaces(2693,145,"Miangalua");
//        databaseHelper.insertPlaces(2694,145,"Milepa");
//        databaseHelper.insertPlaces(2695,145,"Mpui");
//        databaseHelper.insertPlaces(2696,145,"Msanda");
//        databaseHelper.insertPlaces(2697,145,"Mtowisa");
//        databaseHelper.insertPlaces(2698,145,"Muze");
//        databaseHelper.insertPlaces(2699,145,"Sandulula");
//
//        databaseHelper.insertPlaces(2700,146,"Chanji");
//        databaseHelper.insertPlaces(2701,146," Izia");
//        databaseHelper.insertPlaces(2702,146,"Kasense");
//        databaseHelper.insertPlaces(2703,146,"Katandala");
//        databaseHelper.insertPlaces(2704,146,"Kizwite");
//        databaseHelper.insertPlaces(2705,146,"Majengo ");
//        databaseHelper.insertPlaces(2706,146,"Malangali");
//        databaseHelper.insertPlaces(2707,146,"Matanga");
//        databaseHelper.insertPlaces(2708,146,"Mazwi");
//        databaseHelper.insertPlaces(2709,146,"Milanzi");
//        databaseHelper.insertPlaces(2710,146,"Mollo");
//        databaseHelper.insertPlaces(2711,146,"Ntendo ");
//        databaseHelper.insertPlaces(2712,146,"Old Sumbawanga");
//        databaseHelper.insertPlaces(2713,146,"Pito");
//        databaseHelper.insertPlaces(2714,146,"Senga");
//
//        databaseHelper.insertPlaces(2715,147,"Kambarage");
//        databaseHelper.insertPlaces(2716,147,"Kigonsera");
//        databaseHelper.insertPlaces(2717,147,"Kihangi Mahuka");
//        databaseHelper.insertPlaces(2718,147,"Kihungu");
//        databaseHelper.insertPlaces(2719,147,"Kikolo");
//        databaseHelper.insertPlaces(2720,147,"Kilimani (Mbinga)");
//        databaseHelper.insertPlaces(2721,147,"Kipapa");
//        databaseHelper.insertPlaces(2722,147,"Kipololo");
//        databaseHelper.insertPlaces(2723,147,"Kitanda");
//        databaseHelper.insertPlaces(2724,147,"Kitumbalomo ");
//        databaseHelper.insertPlaces(2725,147,"Kitura ");
//        databaseHelper.insertPlaces(2726,147,"Langiro");
//        databaseHelper.insertPlaces(2727,147,"Linda ");
//        databaseHelper.insertPlaces(2728,147,"Litembo");
//        databaseHelper.insertPlaces(2729,147,"Litumbandyosi");
//        databaseHelper.insertPlaces(2730,147,"Luwaita");
//        databaseHelper.insertPlaces(2731,147,"Maguu");
//        databaseHelper.insertPlaces(2732,147,"Mapera");
//        databaseHelper.insertPlaces(2733,147,"Matiri");
//        databaseHelper.insertPlaces(2734,147,"Mbangamao");
//        databaseHelper.insertPlaces(2735,147,"Mbinga Mjini");
//        databaseHelper.insertPlaces(2736,147,"Mbuji ");
//        databaseHelper.insertPlaces(2737,147,"Mkako");
//        databaseHelper.insertPlaces(2738,147,"Mkalanga");
//        databaseHelper.insertPlaces(2739,147,"Mkumbi");
//        databaseHelper.insertPlaces(2740,147,"Mpapa (Mbinga) ");
//        databaseHelper.insertPlaces(2741,147,"Mpepai");
//        databaseHelper.insertPlaces(2742,147,"Namswea ");
//        databaseHelper.insertPlaces(2743,147,"Myangayanga");
//        databaseHelper.insertPlaces(2744,147,"Ngima");
//        databaseHelper.insertPlaces(2745,147,"Nyoni");
//        databaseHelper.insertPlaces(2746,147,"Ruanda (Mbinga)");
//        databaseHelper.insertPlaces(2747,147," Ukata ");
//
//
//        databaseHelper.insertPlaces(2748,148,"Hanga");
//        databaseHelper.insertPlaces(2749,148,"Kitanda");
//        databaseHelper.insertPlaces(2750,148,"Ligera");
//        databaseHelper.insertPlaces(2751,148,"Likuyuseka ");
//        databaseHelper.insertPlaces(2752,148,"Limamu");
//        databaseHelper.insertPlaces(2753,148,"Litola ");
//        databaseHelper.insertPlaces(2754,148,"Luchili");
//        databaseHelper.insertPlaces(2755,148,"Luegu");
//        databaseHelper.insertPlaces(2756,148,"Lusewa");
//        databaseHelper.insertPlaces(2757,148,"Magazini");
//        databaseHelper.insertPlaces(2758,148,"Mchomoro");
//        databaseHelper.insertPlaces(2759,148,"Mgombasi");
//        databaseHelper.insertPlaces(2760,148,"Mkongo");
//        databaseHelper.insertPlaces(2761,148,"Mputa");
//        databaseHelper.insertPlaces(2762,148,"Msindo");
//        databaseHelper.insertPlaces(2763,148,"Namabengo");
//        databaseHelper.insertPlaces(2764,148,"Namtumbo");
//        databaseHelper.insertPlaces(2765,148,"Rwinga");
//
//        databaseHelper.insertPlaces(2766,149,"Gumbiro");
//        databaseHelper.insertPlaces(2767,149,"Kilagano");
//        databaseHelper.insertPlaces(2768,149,"Litisha");
//        databaseHelper.insertPlaces(2769,149,"Magagura");
//        databaseHelper.insertPlaces(2770,149,"Mahanje");
//        databaseHelper.insertPlaces(2771,149,"Maposeni");
//        databaseHelper.insertPlaces(2772,149," Matimira ");
//        databaseHelper.insertPlaces(2773,149,"Matumbi ");
//        databaseHelper.insertPlaces(2774,149," Mbinga Mhalule");
//        databaseHelper.insertPlaces(2775,149,"Mkongotema");
//        databaseHelper.insertPlaces(2776,149,"Mpandangindo");
//        databaseHelper.insertPlaces(2777,149,"Mpitimbi ");
//        databaseHelper.insertPlaces(2778,149,"Mtyangimbole");
//        databaseHelper.insertPlaces(2779,149,"Muhukuru");
//        databaseHelper.insertPlaces(2780,149,"Ndongosi");
//        databaseHelper.insertPlaces(2781,149,"Peramiho");
//        databaseHelper.insertPlaces(2782,149,"Wino (Songea) ");
//
//        databaseHelper.insertPlaces(2783,150,"Bombambili");
//        databaseHelper.insertPlaces(2784,150,"Lilambo");
//        databaseHelper.insertPlaces(2785,150,"Lizaboni");
//        databaseHelper.insertPlaces(2786,150,"Majengo");
//        databaseHelper.insertPlaces(2787,150,"Matarawe");
//        databaseHelper.insertPlaces(2788,150,"Mateka");
//        databaseHelper.insertPlaces(2789,150,"Matogoro");
//        databaseHelper.insertPlaces(2790,150,"Misufini");
//        databaseHelper.insertPlaces(2791,150,"Mjimwema");
//        databaseHelper.insertPlaces(2792,150,"Mletele");
//        databaseHelper.insertPlaces(2793,150,"Msamala");
//        databaseHelper.insertPlaces(2794,150,"Mshangano");
//        databaseHelper.insertPlaces(2795,150,"Mwengemshindo");
//        databaseHelper.insertPlaces(2796,150,"Ndilimalitembo");
//        databaseHelper.insertPlaces(2797,150,"Ruhuwiko");
//        databaseHelper.insertPlaces(2798,150,"Ruvuma");
//        databaseHelper.insertPlaces(2799,150,"SeedFarm");
//        databaseHelper.insertPlaces(2800,150,"Songea Mjini");
//        databaseHelper.insertPlaces(2801,150,"Subira");
//        databaseHelper.insertPlaces(2802,150,"Tanga");
//
//        databaseHelper.insertPlaces(2803,151,"Jakika");
//        databaseHelper.insertPlaces(2804,151,"Kalulu");
//        databaseHelper.insertPlaces(2805,151,"Kidodoma");
//        databaseHelper.insertPlaces(2806,151,"Ligoma");
//        databaseHelper.insertPlaces(2807,151,"Ligunga");
//        databaseHelper.insertPlaces(2808,151,"Lukumbule");
//        databaseHelper.insertPlaces(2809,151,"Majengo");
//        databaseHelper.insertPlaces(2810,151,"Marumba");
//        databaseHelper.insertPlaces(2811,151,"Masonya");
//        databaseHelper.insertPlaces(2812,151,"Matemanga");
//        databaseHelper.insertPlaces(2813,151,"Mbati");
//        databaseHelper.insertPlaces(2814,151,"Mbesa");
//        databaseHelper.insertPlaces(2815,151,"Mchangani");
//        databaseHelper.insertPlaces(2816,151,"Mchesi");
//        databaseHelper.insertPlaces(2817,151,"Mchoteka");
//        databaseHelper.insertPlaces(2818,151,"Mchuluka");
//        databaseHelper.insertPlaces(2819,151,"Mindu");
//        databaseHelper.insertPlaces(2820,151,"Misechela");
//        databaseHelper.insertPlaces(2821,151,"Mlingoti Magharibi");
//        databaseHelper.insertPlaces(2822,151,"Mlingoti Mashariki");
//        databaseHelper.insertPlaces(2823,151,"Mtina");
//        databaseHelper.insertPlaces(2824,151,"Muhuwesi");
//        databaseHelper.insertPlaces(2825,151,"Nakapanya");
//        databaseHelper.insertPlaces(2826,151,"Nakayaya");
//        databaseHelper.insertPlaces(2827,151,"Nalasi Magharibi");
//        databaseHelper.insertPlaces(2828,151,"Nalasi Mashariki");
//        databaseHelper.insertPlaces(2829,151,"Namasakata");
//        databaseHelper.insertPlaces(2830,151,"Namiungo");
//        databaseHelper.insertPlaces(2831,151,"Nampungu");
//        databaseHelper.insertPlaces(2832,151,"Namwinyu");
//        databaseHelper.insertPlaces(2833,151,"Nandembo");
//        databaseHelper.insertPlaces(2834,151,"Nanjoka");
//        databaseHelper.insertPlaces(2835,151,"Ngapa");
//        databaseHelper.insertPlaces(2836,151,"Sisikwasisi");
//        databaseHelper.insertPlaces(2837,151,"Tuwemacho");
//
//        databaseHelper.insertPlaces(2838,152,"Chiwanda");
//        databaseHelper.insertPlaces(2839,152,"Kihagara");
//        databaseHelper.insertPlaces(2840,152,"Kilosa");
//        databaseHelper.insertPlaces(2841,152,"Kingerikiti");
//        databaseHelper.insertPlaces(2842,152,"Liparamba");
//        databaseHelper.insertPlaces(2843,152,"Lipingo");
//        databaseHelper.insertPlaces(2844,152,"Lituhi");
//        databaseHelper.insertPlaces(2845,152,"Liuli");
//        databaseHelper.insertPlaces(2846,152,"Liwundi");
//        databaseHelper.insertPlaces(2847,152,"Luhangarasi");
//        databaseHelper.insertPlaces(2848,152,"Mbaha");
//        databaseHelper.insertPlaces(2849,152,"Mbambabay");
//        databaseHelper.insertPlaces(2850,152,"Mtipwili");
//        databaseHelper.insertPlaces(2851,152,"Ngumbo");
//        databaseHelper.insertPlaces(2852,152,"Tingi");
//
//        databaseHelper.insertPlaces(2853,153,"Busoka");
//        databaseHelper.insertPlaces(2854,153,"Isagehe");
//        databaseHelper.insertPlaces(2855,153,"Iyenze");
//        databaseHelper.insertPlaces(2856,153,"Kagongwa");
//        databaseHelper.insertPlaces(2857,153,"Kahama Mjini");
//        databaseHelper.insertPlaces(2858,153,"Kilago");
//        databaseHelper.insertPlaces(2859,153,"Kinaga");
//        databaseHelper.insertPlaces(2860,153,"Majengo");
//        databaseHelper.insertPlaces(2861,153,"Malunga");
//        databaseHelper.insertPlaces(2862,153,"Mhongolo");
//        databaseHelper.insertPlaces(2863,153,"Mhungula");
//        databaseHelper.insertPlaces(2864,153,"Mondo");
//        databaseHelper.insertPlaces(2865,153,"Mwendakulima");
//        databaseHelper.insertPlaces(2866,153,"Ngogwa");
//        databaseHelper.insertPlaces(2867,153,"Nyahanga");
//        databaseHelper.insertPlaces(2868,153,"Nyandekwa");
//        databaseHelper.insertPlaces(2869,153,"Nyasubi");
//        databaseHelper.insertPlaces(2870,153,"Nyihogo");
//        databaseHelper.insertPlaces(2871,153,"Wendele");
//        databaseHelper.insertPlaces(2872,153,"Zongomera");
//
//        databaseHelper.insertPlaces(2873,154,"Bukomela");
//        databaseHelper.insertPlaces(2874,154,"Bulige ");
//        databaseHelper.insertPlaces(2875,154,"Bugarama");
//        databaseHelper.insertPlaces(2876,154,"Bulungwa");
//        databaseHelper.insertPlaces(2877,154,"Bulyanhulu");
//        databaseHelper.insertPlaces(2878,154,"Busangi ");
//        databaseHelper.insertPlaces(2879,154,"Chambo");
//        databaseHelper.insertPlaces(2880,154,"Chela ");
//        databaseHelper.insertPlaces(2881,154,"Chona");
//        databaseHelper.insertPlaces(2882,154,"Idahina");
//        databaseHelper.insertPlaces(2883,154,"Igunda");
//        databaseHelper.insertPlaces(2884,154,"Igwamanoni");
//        databaseHelper.insertPlaces(2885,154,"Isaka");
//        databaseHelper.insertPlaces(2886,154,"Jana");
//        databaseHelper.insertPlaces(2887,154,"Kashishi");
//        databaseHelper.insertPlaces(2888,154,"Kinamapula");
//        databaseHelper.insertPlaces(2889,154,"Kisuke");
//        databaseHelper.insertPlaces(2890,154,"Lunguya");
//        databaseHelper.insertPlaces(2891,154,"Mapamba");
//        databaseHelper.insertPlaces(2892,154,"Mega");
//        databaseHelper.insertPlaces(2893,154,"Mpunze ");
//        databaseHelper.insertPlaces(2894,154,"Mwalugulu");
//        databaseHelper.insertPlaces(2895,154,"Mwanase");
//        databaseHelper.insertPlaces(2896,154,"Ngaya");
//        databaseHelper.insertPlaces(2897,154,"Ntobo ");
//        databaseHelper.insertPlaces(2898,154,"Nyankende");
//        databaseHelper.insertPlaces(2899,154,"Sabasabini");
//        databaseHelper.insertPlaces(2900,154,"Segese");
//        databaseHelper.insertPlaces(2901,154,"Shilela");
//        databaseHelper.insertPlaces(2902,154,"Ubagwe ");
//        databaseHelper.insertPlaces(2903,154,"Ukune");
//        databaseHelper.insertPlaces(2904,154,"Ulewe");
//        databaseHelper.insertPlaces(2905,154,"Ulowa");
//        databaseHelper.insertPlaces(2906,154,"Ushetu");
//        databaseHelper.insertPlaces(2907,154,"Uyogo");
//
//        databaseHelper.insertPlaces(2908,155,"Bubiki ");
//        databaseHelper.insertPlaces(2909,155,"Bunambiyu ");
//        databaseHelper.insertPlaces(2910,155,"Itima");
//        databaseHelper.insertPlaces(2911,155,"Kiloleli");
//        databaseHelper.insertPlaces(2912,155,"Kishapu");
//        databaseHelper.insertPlaces(2913,155,"Lagana");
//        databaseHelper.insertPlaces(2914,155,"Masanga");
//        databaseHelper.insertPlaces(2915,155,"Mondo");
//        databaseHelper.insertPlaces(2916,155,"Mwadui Lohumbo");
//        databaseHelper.insertPlaces(2917,155,"Mwakipoya");
//        databaseHelper.insertPlaces(2918,155,"Mwamalasa");
//        databaseHelper.insertPlaces(2919,155,"Mwamashele");
//        databaseHelper.insertPlaces(2920,155,"Ngofila");
//        databaseHelper.insertPlaces(2921,155,"Seke-Bukoro");
//        databaseHelper.insertPlaces(2922,155,"Shagihilu");
//        databaseHelper.insertPlaces(2923,155,"Somagedi");
//        databaseHelper.insertPlaces(2924,155,"Songwa");
//        databaseHelper.insertPlaces(2925,155,"Talaga");
//        databaseHelper.insertPlaces(2926,155,"Uchunga");
//        databaseHelper.insertPlaces(2927,155,"Ukenyenge");
//
//
//        databaseHelper.insertPlaces(2928,156,"Bukene");
//        databaseHelper.insertPlaces(2929,156,"Didia");
//        databaseHelper.insertPlaces(2930,156,"Ilola ");
//        databaseHelper.insertPlaces(2931,156,"Imesela");
//        databaseHelper.insertPlaces(2932,156,"Iselamagazi");
//        databaseHelper.insertPlaces(2933,156,"Itwangi");
//        databaseHelper.insertPlaces(2934,156,"Lyabuke");
//        databaseHelper.insertPlaces(2935,156,"Lyabusalu");
//        databaseHelper.insertPlaces(2936,156,"Lyamidati");
//        databaseHelper.insertPlaces(2937,156,"Masengwa");
//        databaseHelper.insertPlaces(2938,156,"Mwakitolyo");
//        databaseHelper.insertPlaces(2939,156,"Mwalukwa");
//        databaseHelper.insertPlaces(2940,156,"Mwamala");
//        databaseHelper.insertPlaces(2941,156," Mwantini");
//        databaseHelper.insertPlaces(2942,156,"Mwenge");
//        databaseHelper.insertPlaces(2943,156,"Nsalala");
//        databaseHelper.insertPlaces(2944,156,"Nyamalogo");
//        databaseHelper.insertPlaces(2945,156,"Nyida");
//        databaseHelper.insertPlaces(2946,156,"Pandagichiza ");
//        databaseHelper.insertPlaces(2947,156,"Puni");
//        databaseHelper.insertPlaces(2948,156,"Salawe");
//        databaseHelper.insertPlaces(2949,156,"Samuye");
//        databaseHelper.insertPlaces(2950,156,"Solwa");
//        databaseHelper.insertPlaces(2951,156,"Tinde");
//        databaseHelper.insertPlaces(2952,156,"Usanda");
//        databaseHelper.insertPlaces(2953,156,"Usule");
//
//
//        databaseHelper.insertPlaces(2954,157,"Chamaguha");
//        databaseHelper.insertPlaces(2955,157,"Chibe");
//        databaseHelper.insertPlaces(2956,157,"Ibadakuli");
//        databaseHelper.insertPlaces(2957,157,"Ibinzamata");
//        databaseHelper.insertPlaces(2958,157,"Kambarage");
//        databaseHelper.insertPlaces(2959,157,"Kitangili");
//        databaseHelper.insertPlaces(2960,157,"Kizumbi");
//        databaseHelper.insertPlaces(2961,157,"Koloto");
//        databaseHelper.insertPlaces(2962,157,"Lubaga");
//        databaseHelper.insertPlaces(2963,157,"Masekelo");
//        databaseHelper.insertPlaces(2964,157,"Mwamalili");
//        databaseHelper.insertPlaces(2965,157,"Mwawaza");
//        databaseHelper.insertPlaces(2966,157,"Ndala");
//        databaseHelper.insertPlaces(2967,157,"Ndembezi");
//        databaseHelper.insertPlaces(2968,157,"Ngokolo");
//        databaseHelper.insertPlaces(2969,157,"Old Shinyanga");
//        databaseHelper.insertPlaces(2970,157,"Shinyanga mjini");
//
//
//        databaseHelper.insertPlaces(2971,158,"Bariadi");
//        databaseHelper.insertPlaces(2972,158,"Bunamhala");
//        databaseHelper.insertPlaces(2973,158,"Dutwa");
//        databaseHelper.insertPlaces(2974,158,"Gambosi");
//        databaseHelper.insertPlaces(2975,158,"Gilya");
//        databaseHelper.insertPlaces(2976,158,"Guduwi");
//        databaseHelper.insertPlaces(2977,158,"Ikungulyabashashi");
//        databaseHelper.insertPlaces(2978,158,"Isanga");
//        databaseHelper.insertPlaces(2979,158,"Kasoli");
//        databaseHelper.insertPlaces(2980,158,"Kilalo");
//        databaseHelper.insertPlaces(2981,158,"Malambo");
//        databaseHelper.insertPlaces(2982,158,"Matongo");
//        databaseHelper.insertPlaces(2983,158,"Mhango");
//        databaseHelper.insertPlaces(2984,158,"Mwadobana");
//        databaseHelper.insertPlaces(2985,158,"Mwaubingi");
//        databaseHelper.insertPlaces(2986,158,"Mwaumatondo");
//        databaseHelper.insertPlaces(2987,158,"Ngulyati");
//        databaseHelper.insertPlaces(2988,158,"Nkindwabiye");
//        databaseHelper.insertPlaces(2989,158,"Nkololo");
//        databaseHelper.insertPlaces(2990,158,"Nyakabindi");
//        databaseHelper.insertPlaces(2991,158,"Nyangokolwa");
//        databaseHelper.insertPlaces(2992,158,"Sakwe");
//        databaseHelper.insertPlaces(2993,158,"Sapiwi");
//        databaseHelper.insertPlaces(2994,158,"Sima");
//        databaseHelper.insertPlaces(2995,158,"Somanda");
//
//        databaseHelper.insertPlaces(2996,159,"Badugu");
//        databaseHelper.insertPlaces(2997,159,"Igalukilo");
//        databaseHelper.insertPlaces(2998,159," Kabita");
//        databaseHelper.insertPlaces(2999,159,"Kalemela");
//        databaseHelper.insertPlaces(3000,159,"Kiloleli");
//        databaseHelper.insertPlaces(3001,159,"Lamadi");
//        databaseHelper.insertPlaces(3002,159,"Lutubiga");
//        databaseHelper.insertPlaces(3003,159,"Malili");
//        databaseHelper.insertPlaces(3004,159,"Mkula");
//        databaseHelper.insertPlaces(3005,159,"Mwamanyili");
//        databaseHelper.insertPlaces(3006,159,"Ngasamo");
//        databaseHelper.insertPlaces(3007,159,"Nyaluhande");
//        databaseHelper.insertPlaces(3008,159,"Shigala");
//
//
//        databaseHelper.insertPlaces(3009,160,"Budalabujiga");
//        databaseHelper.insertPlaces(3010,160,"Bugera");
//        databaseHelper.insertPlaces(3011,160,"Chinamili");
//        databaseHelper.insertPlaces(3012,160,"Ikindilo");
//        databaseHelper.insertPlaces(3013,160,"Kinang'weli");
//        databaseHelper.insertPlaces(3014,160,"Lagangabilili");
//        databaseHelper.insertPlaces(3015,160,"Lugulu");
//        databaseHelper.insertPlaces(3016,160,"Mbita");
//        databaseHelper.insertPlaces(3017,160,"Mhunze");
//        databaseHelper.insertPlaces(3018,160,"Migato");
//        databaseHelper.insertPlaces(3019,160,"Mwalushu");
//        databaseHelper.insertPlaces(3020,160,"Mwamapalala");
//        databaseHelper.insertPlaces(3021,160,"Mwamtani");
//        databaseHelper.insertPlaces(3022,160,"Mwaswale");
//        databaseHelper.insertPlaces(3023,160,"Ndolelezi");
//        databaseHelper.insertPlaces(3024,160,"Nhobora");
//        databaseHelper.insertPlaces(3025,160,"Nkoma");
//        databaseHelper.insertPlaces(3026,160,"Nkuyu");
//        databaseHelper.insertPlaces(3027,160,"Nyamalapa");
//        databaseHelper.insertPlaces(3028,160,"Sagata");
//        databaseHelper.insertPlaces(3029,160,"Sawida");
//        databaseHelper.insertPlaces(3030,160,"Zagayu");
//
//
//        databaseHelper.insertPlaces(3031,161,"Badi");
//        databaseHelper.insertPlaces(3032,161,"Binza");
//        databaseHelper.insertPlaces(3033,161,"Buchambi");
//        databaseHelper.insertPlaces(3034,161,"Budekwa");
//        databaseHelper.insertPlaces(3035,161,"Busilili");
//        databaseHelper.insertPlaces(3036,161,"Dakama");
//        databaseHelper.insertPlaces(3037,161,"Ipililo");
//        databaseHelper.insertPlaces(3038,161,"Isanga");
//        databaseHelper.insertPlaces(3039,161,"Kadoto");
//        databaseHelper.insertPlaces(3040,161,"Kulimi ");
//        databaseHelper.insertPlaces(3041,161,"Lalago");
//        databaseHelper.insertPlaces(3042,161,"Malampaka");
//        databaseHelper.insertPlaces(3043,161,"Masela");
//        databaseHelper.insertPlaces(3044,161,"Mpindo");
//        databaseHelper.insertPlaces(3045,161,"Mwamanenge");
//        databaseHelper.insertPlaces(3046,161,"Mwamashimba");
//        databaseHelper.insertPlaces(3047,161,"Mwang'honoli");
//        databaseHelper.insertPlaces(3048,161,"Nguliguli");
//        databaseHelper.insertPlaces(3049,161,"Ng'wigwa");
//        databaseHelper.insertPlaces(3050,161,"Nyabubinza");
//        databaseHelper.insertPlaces(3051,161,"Nyalikungu");
//        databaseHelper.insertPlaces(3052,161,"Senani");
//        databaseHelper.insertPlaces(3053,161,"Seng'wa");
//        databaseHelper.insertPlaces(3054,161," Shishiyu ");
//        databaseHelper.insertPlaces(3055,161,"Sukuma");
//        databaseHelper.insertPlaces(3056,161,"Zanzui");
//
//        databaseHelper.insertPlaces(3057,162,"Bukundi");
//        databaseHelper.insertPlaces(3058,162,"Imalaseko ");
//        databaseHelper.insertPlaces(3059,162,"Itinje ");
//        databaseHelper.insertPlaces(3060,162,"Kimali");
//        databaseHelper.insertPlaces(3061,162,"Kisesa");
//        databaseHelper.insertPlaces(3062,162,"Lingeka");
//        databaseHelper.insertPlaces(3063,162,"Lubiga");
//        databaseHelper.insertPlaces(3064,162,"Mwabuma");
//        databaseHelper.insertPlaces(3065,162,"Mwabusalu");
//        databaseHelper.insertPlaces(3066,162,"Mwabuzo");
//        databaseHelper.insertPlaces(3067,162,"Mwakisandu");
//        databaseHelper.insertPlaces(3068,162,"Mwamalole");
//        databaseHelper.insertPlaces(3069,162,"Mwamanimba");
//        databaseHelper.insertPlaces(3070,162,"Mwamanongu");
//        databaseHelper.insertPlaces(3071,162,"Mwamishali");
//        databaseHelper.insertPlaces(3072,162,"Mwandoya");
//        databaseHelper.insertPlaces(3073,162,"Mwangundo");
//        databaseHelper.insertPlaces(3074,162,"Mwanhuzi");
//        databaseHelper.insertPlaces(3075,162,"Mwanjolo");
//        databaseHelper.insertPlaces(3076,162,"Mwanyahina");
//        databaseHelper.insertPlaces(3077,162,"Mwasengela");
//        databaseHelper.insertPlaces(3078,162,"Ng'hoboko ");
//        databaseHelper.insertPlaces(3079,162,"Nkoma");
//        databaseHelper.insertPlaces(3080,162,"Sakasaka");
//        databaseHelper.insertPlaces(3081,162,"Tindabuligi");
//
//        databaseHelper.insertPlaces(3082,163,"Kaselya");
//        databaseHelper.insertPlaces(3083,163," Kidaru");
//        databaseHelper.insertPlaces(3084,163,"Kinampanda");
//        databaseHelper.insertPlaces(3085,163,"Kiomboi");
//        databaseHelper.insertPlaces(3086,163,"Kisiriri");
//        databaseHelper.insertPlaces(3087,163,"Kyengege");
//        databaseHelper.insertPlaces(3088,163,"Mbelekese");
//        databaseHelper.insertPlaces(3089,163,"Mgongo");
//        databaseHelper.insertPlaces(3090,163,"Mtekente");
//        databaseHelper.insertPlaces(3091,163,"Mtoa");
//        databaseHelper.insertPlaces(3092,163,"Ndago");
//        databaseHelper.insertPlaces(3093,163," Ndulungu ");
//        databaseHelper.insertPlaces(3094,163,"Ntwike");
//        databaseHelper.insertPlaces(3095,163,"Shelui");
//        databaseHelper.insertPlaces(3096,163,"Tulya");
//        databaseHelper.insertPlaces(3097,163,"Ulemo");
//        databaseHelper.insertPlaces(3098,163,"Urughu");
//
//        databaseHelper.insertPlaces(3099,164,"Aghondi");
//        databaseHelper.insertPlaces(3100,164,"Chikola");
//        databaseHelper.insertPlaces(3101,164,"Chikuyu");
//        databaseHelper.insertPlaces(3102,164,"Idodyandole");
//        databaseHelper.insertPlaces(3103,164,"Ipande");
//        databaseHelper.insertPlaces(3104,164,"Isseke");
//        databaseHelper.insertPlaces(3105,164,"Itigi ");
//        databaseHelper.insertPlaces(3106,164,"Itigi Majengo");
//        databaseHelper.insertPlaces(3107,164,"Kintinku");
//        databaseHelper.insertPlaces(3108,164,"Kitaraka");
//        databaseHelper.insertPlaces(3109,164,"Majiri");
//        databaseHelper.insertPlaces(3110,164,"Ipande");
//        databaseHelper.insertPlaces(3111,164,"Makuru");
//        databaseHelper.insertPlaces(3112,164,"Makutopora");
//        databaseHelper.insertPlaces(3113,164,"Manyoni");
//        databaseHelper.insertPlaces(3114,164,"Mgandu");
//        databaseHelper.insertPlaces(3115,164,"Mitundu");
//        databaseHelper.insertPlaces(3116,164,"Mkwese");
//        databaseHelper.insertPlaces(3117,164,"Muhalala");
//        databaseHelper.insertPlaces(3118,164,"Mvumi ");
//        databaseHelper.insertPlaces(3119,164,"Mwamagembe");
//        databaseHelper.insertPlaces(3120,164,"Nkonko ");
//        databaseHelper.insertPlaces(3121,164,"Rungwa");
//        databaseHelper.insertPlaces(3122,164,"Sanjaranda");
//        databaseHelper.insertPlaces(3123,164,"Sanza");
//        databaseHelper.insertPlaces(3124,164,"Saranda");
//        databaseHelper.insertPlaces(3125,164,"Sasajila");
//        databaseHelper.insertPlaces(3126,164,"Sasilo");
//        databaseHelper.insertPlaces(3127,164,"Solya");
//
//        databaseHelper.insertPlaces(3128,165,"Ikhanoda");
//        databaseHelper.insertPlaces(3129,165,"Ilongero");
//        databaseHelper.insertPlaces(3130,165,"Itaja");
//        databaseHelper.insertPlaces(3131,165,"Kijota");
//        databaseHelper.insertPlaces(3132,165,"Kinyagigi");
//        databaseHelper.insertPlaces(3133,165,"Kinyeto");
//        databaseHelper.insertPlaces(3134,165,"Maghojoa");
//        databaseHelper.insertPlaces(3135,165,"Makuro ");
//        databaseHelper.insertPlaces(3136,165,"Merya");
//        databaseHelper.insertPlaces(3137,165,"Mgori");
//        databaseHelper.insertPlaces(3138,165,"Mrama");
//        databaseHelper.insertPlaces(3139,165,"Msange");
//        databaseHelper.insertPlaces(3140,165,"Msisi");
//        databaseHelper.insertPlaces(3141,165,"Mtinko");
//        databaseHelper.insertPlaces(3142,165,"Mudida");
//        databaseHelper.insertPlaces(3143,165,"Mughunga ");
//        databaseHelper.insertPlaces(3144,165,"Mughamo");
//        databaseHelper.insertPlaces(3145,165,"Mwasauya");
//        databaseHelper.insertPlaces(3146,165,"Ngimu");
//        databaseHelper.insertPlaces(3147,165,"Ntonge");
//        databaseHelper.insertPlaces(3148,165,"Ughandi");
//
//
//        databaseHelper.insertPlaces(3149,166,"Ipembe");
//        databaseHelper.insertPlaces(3150,166,"Kindai");
//        databaseHelper.insertPlaces(3151,166,"Majengo");
//        databaseHelper.insertPlaces(3152,166,"Mandewa");
//        databaseHelper.insertPlaces(3153,166,"Misuna");
//        databaseHelper.insertPlaces(3154,166,"Mitunduruni");
//        databaseHelper.insertPlaces(3155,166,"Mtamaa");
//        databaseHelper.insertPlaces(3156,166,"Mtipa");
//        databaseHelper.insertPlaces(3157,166,"Mughanga");
//        databaseHelper.insertPlaces(3158,166,"Mungumaji");
//        databaseHelper.insertPlaces(3159,166,"Mwankoko ");
//        databaseHelper.insertPlaces(3160,166,"Uhamaka");
//        databaseHelper.insertPlaces(3161,166,"Unyambwa");
//        databaseHelper.insertPlaces(3162,166,"Unyamikumbi");
//        databaseHelper.insertPlaces(3163,166,"Utemini");
//
//        databaseHelper.insertPlaces(3164,167,"Bukoko");
//        databaseHelper.insertPlaces(3165,167,"Chabutwa");
//        databaseHelper.insertPlaces(3166,167,"Choma");
//        databaseHelper.insertPlaces(3167,167,"Igoweko");
//        databaseHelper.insertPlaces(3168,167,"Igunga ");
//        databaseHelper.insertPlaces(3169,167,"Igurubi");
//        databaseHelper.insertPlaces(3170,167,"Isakamaliwa");
//        databaseHelper.insertPlaces(3171,167,"Itumba ");
//        databaseHelper.insertPlaces(3172,167,"Itunduru");
//        databaseHelper.insertPlaces(3173,167,"Kining'ila");
//        databaseHelper.insertPlaces(3174,167,"Kinungu");
//        databaseHelper.insertPlaces(3175,167,"Mbutu");
//        databaseHelper.insertPlaces(3176,167,"Mwamashiga");
//        databaseHelper.insertPlaces(3177,167,"Mwamashimba");
//        databaseHelper.insertPlaces(3178,167,"Mwashiku ");
//        databaseHelper.insertPlaces(3179,167,"Mwisi");
//        databaseHelper.insertPlaces(3180,167,"Nanga");
//        databaseHelper.insertPlaces(3181,167,"Ndembezi");
//        databaseHelper.insertPlaces(3182,167,"Ngulu");
//        databaseHelper.insertPlaces(3183,167,"Nguvumoja");
//        databaseHelper.insertPlaces(3184,167,"Nkinga");
//        databaseHelper.insertPlaces(3185,167,"Ntobo");
//        databaseHelper.insertPlaces(3186,167," Nyandekwa ");
//        databaseHelper.insertPlaces(3187,167,"Simbo");
//        databaseHelper.insertPlaces(3188,167,"Sungwizi");
//        databaseHelper.insertPlaces(3189,167,"Ziba");
//
//
////        databaseHelper.insertPlaces(3180,168,"Ichemba");
//        databaseHelper.insertPlaces(3190,168," Igagala ");
//        databaseHelper.insertPlaces(3191,168,"Igombe Mkulu");
//        databaseHelper.insertPlaces(3192,168,"Igwisi");
//        databaseHelper.insertPlaces(3193,168,"Kaliua");
//        databaseHelper.insertPlaces(3194,168,"Kamsekwa");
//        databaseHelper.insertPlaces(3195,168,"Kanindo");
//        databaseHelper.insertPlaces(3196,168,"Kanoge");
//        databaseHelper.insertPlaces(3197,168,"Kashishi");
//        databaseHelper.insertPlaces(3198,168,"Kazaroho");
//        databaseHelper.insertPlaces(3199,168,"Milambo");
//        databaseHelper.insertPlaces(3200,168,"Mwongozo");
//        databaseHelper.insertPlaces(3201,168,"Sasu");
//        databaseHelper.insertPlaces(3202,168,"Seleli");
//        databaseHelper.insertPlaces(3203,168,"Silambo");
//        databaseHelper.insertPlaces(3204,168,"Ugunga");
//        databaseHelper.insertPlaces(3205,168,"Ukumbisiganga ");
//        databaseHelper.insertPlaces(3206,168,"Ushokola");
//        databaseHelper.insertPlaces(3207,168,"Usinge");
//        databaseHelper.insertPlaces(3208,168,"Uyowa");
//        databaseHelper.insertPlaces(3208,168,"Zugimlole");
//
//
//        databaseHelper.insertPlaces(3210,169,"Budushi");
//        databaseHelper.insertPlaces(3211,169,"Bukene");
//        databaseHelper.insertPlaces(3212,169,"Igusule");
//        databaseHelper.insertPlaces(3213,169,"Ijanija");
//        databaseHelper.insertPlaces(3214,169,"Ikindwa");
//        databaseHelper.insertPlaces(3215,169,"Isagenhe");
//        databaseHelper.insertPlaces(3216,169,"Isanzu");
//        databaseHelper.insertPlaces(3217,169,"Itilo");
//        databaseHelper.insertPlaces(3218,169,"Itobo");
//        databaseHelper.insertPlaces(3219,169," Kahama Nhalanga");
//        databaseHelper.insertPlaces(3220,169,"Karitu");
//        databaseHelper.insertPlaces(3221,169,"Kasela");
//        databaseHelper.insertPlaces(3222,169,"Lusu");
//        databaseHelper.insertPlaces(3223,169,"Magengati");
//        databaseHelper.insertPlaces(3224,169,"Mambali");
//        databaseHelper.insertPlaces(3225,169,"Mbogwe");
//        databaseHelper.insertPlaces(3226,169,"Miguwa");
//        databaseHelper.insertPlaces(3227,169,"Milambo Itobo");
//        databaseHelper.insertPlaces(3228,169,"Mizibaziba");
//        databaseHelper.insertPlaces(3229,169,"Mogwa");
//        databaseHelper.insertPlaces(3230,169,"Muhugi");
//        databaseHelper.insertPlaces(3231,169,"Mwakashanhala");
//        databaseHelper.insertPlaces(3232,169,"Mwamala");
//        databaseHelper.insertPlaces(3233,169,"Mwangoye");
//        databaseHelper.insertPlaces(3234,169,"Nata");
//        databaseHelper.insertPlaces(3235,169,"Ndala");
//        databaseHelper.insertPlaces(3236,169,"Nkiniziwa");
//        databaseHelper.insertPlaces(3237,169,"Nzega Mjini");
//        databaseHelper.insertPlaces(3238,169,"Nzega Ndogo");
//        databaseHelper.insertPlaces(3239,169,"Puge");
//        databaseHelper.insertPlaces(3240,169,"Semembela");
//        databaseHelper.insertPlaces(3241,169,"Shigamba");
//        databaseHelper.insertPlaces(3242,169,"Sigili");
//        databaseHelper.insertPlaces(3243,169,"Tongi");
//        databaseHelper.insertPlaces(3244,169,"Uduka");
//        databaseHelper.insertPlaces(3245,169,"Utwigu");
//        databaseHelper.insertPlaces(3246,169,"Wela");
//
//        databaseHelper.insertPlaces(3247,170,"Chabutwa");
//        databaseHelper.insertPlaces(3248,170,"Igigwa");
//        databaseHelper.insertPlaces(3249,170,"Ipole");
//        databaseHelper.insertPlaces(3250,170," Kiloleli (Sikonge)");
//        databaseHelper.insertPlaces(3251,170,"Kiloli");
//        databaseHelper.insertPlaces(3252,170,"Kipanga");
//        databaseHelper.insertPlaces(3253,170,"Kipili");
//        databaseHelper.insertPlaces(3254,170,"Kisanga");
//        databaseHelper.insertPlaces(3255,170,"Kitunda");
//        databaseHelper.insertPlaces(3256,170,"Mishenu");
//        databaseHelper.insertPlaces(3257,170,"Mole");
//        databaseHelper.insertPlaces(3258,170,"Mpombwe");
//        databaseHelper.insertPlaces(3259,170,"Ngoywa");
//        databaseHelper.insertPlaces(3260,170,"Pangale");
//        databaseHelper.insertPlaces(3261,170,"Sikonge");
//        databaseHelper.insertPlaces(3262,170,"Tutuo");
//        databaseHelper.insertPlaces(3263,170,"Usunga");
//
//        databaseHelper.insertPlaces(3264,171,"Bukumbi");
//        databaseHelper.insertPlaces(3265,171,"Goweko");
//        databaseHelper.insertPlaces(3266,171,"Ibelamilundi");
//        databaseHelper.insertPlaces(3267,171,"Ibiri");
//        databaseHelper.insertPlaces(3268,171,"Igalula");
//        databaseHelper.insertPlaces(3269,171,"Ikongolo");
//        databaseHelper.insertPlaces(3270,171," Ilolanguru");
//        databaseHelper.insertPlaces(3271,171," Isikizya");
//        databaseHelper.insertPlaces(3272,171,"Kigwa (Uyui) ");
//        databaseHelper.insertPlaces(3273,171,"Kizengi");
//        databaseHelper.insertPlaces(3274,171,"Loya");
//        databaseHelper.insertPlaces(3275,171,"Lutende");
//        databaseHelper.insertPlaces(3276,171,"Mabama");
//        databaseHelper.insertPlaces(3277,171,"Magiri");
//        databaseHelper.insertPlaces(3278,171,"Miswaki");
//        databaseHelper.insertPlaces(3279,171,"Miyenze");
//        databaseHelper.insertPlaces(3280,171," Ndono");
//        databaseHelper.insertPlaces(3281,171,"Nsimbo");
//        databaseHelper.insertPlaces(3282,171,"Nsololo ");
//        databaseHelper.insertPlaces(3283,171,"Shitage");
//        databaseHelper.insertPlaces(3284,171,"Tura");
//        databaseHelper.insertPlaces(3285,171,"Ufulumwa");
//        databaseHelper.insertPlaces(3286,171,"Upuge");
//        databaseHelper.insertPlaces(3287,171,"Usagari");
//
//
//        databaseHelper.insertPlaces(3288,172,"Chemchem");
//        databaseHelper.insertPlaces(3289,172,"Cheyo");
//        databaseHelper.insertPlaces(3290,172,"Gongoni");
//        databaseHelper.insertPlaces(3291,172,"Ifucha");
//        databaseHelper.insertPlaces(3292,172,"Ikomwa ");
//        databaseHelper.insertPlaces(3293,172,"Ipuli");
//        databaseHelper.insertPlaces(3294,172,"Isevya");
//        databaseHelper.insertPlaces(3295,172,"Itetemia");
//        databaseHelper.insertPlaces(3296,172,"Itonjanda");
//        databaseHelper.insertPlaces(3297,172,"Kabila");
//        databaseHelper.insertPlaces(3298,172,"Kakola");
//        databaseHelper.insertPlaces(3299,172,"Kalunde");
//        databaseHelper.insertPlaces(3300,172,"Kanyenye");
//        databaseHelper.insertPlaces(3301,172,"Kiloleni");
//        databaseHelper.insertPlaces(3302,172,"Kitete");
//        databaseHelper.insertPlaces(3303,172,"Malolo (Tabora)");
//        databaseHelper.insertPlaces(3304,172,"Mbugani");
//        databaseHelper.insertPlaces(3305,172,"Misha ");
//        databaseHelper.insertPlaces(3306,172,"Mtendeni");
//        databaseHelper.insertPlaces(3307,172,"Ndevelwa");
//        databaseHelper.insertPlaces(3308,172,"Ng'ambo");
//        databaseHelper.insertPlaces(3309,172,"Ntalikwa ");
//        databaseHelper.insertPlaces(3310,172,"Tambukareli");
//        databaseHelper.insertPlaces(3311,172,"Tumbi");
//        databaseHelper.insertPlaces(3312,172,"Uyui");
//
//
//        databaseHelper.insertPlaces(3313,173,"Imalamakoye");
//        databaseHelper.insertPlaces(3314,173,"Itundu");
//        databaseHelper.insertPlaces(3315,173," Kapilula");
//        databaseHelper.insertPlaces(3316,173,"Kasisi");
//        databaseHelper.insertPlaces(3317,173,"Kiloleni");
//        databaseHelper.insertPlaces(3318,173,"Muungano");
//        databaseHelper.insertPlaces(3319,173,"Nsenda");
//        databaseHelper.insertPlaces(3320,173,"Songambele");
//        databaseHelper.insertPlaces(3321,173,"Ugalla");
//        databaseHelper.insertPlaces(3322,173,"Ukondamoyo");
//        databaseHelper.insertPlaces(3323,173,"Urambo");
//        databaseHelper.insertPlaces(3324,173,"Usisya");
//        databaseHelper.insertPlaces(3325,173,"Ussoke");
//        databaseHelper.insertPlaces(3326,173,"Uyogo");
//        databaseHelper.insertPlaces(3327,173,"Uyumbu");
//        databaseHelper.insertPlaces(3328,173,"Vumilia");
//
//        databaseHelper.insertPlaces(3329,174,"Segera");
//        databaseHelper.insertPlaces(3330,174,"Ndolwa");
////zingine zipo chini
//        databaseHelper.insertPlaces(3331,174,"Kabuku");
//        databaseHelper.insertPlaces(3332,174,"Kwamatuku");
//        databaseHelper.insertPlaces(3333,174,"Kwedizinga");
//        databaseHelper.insertPlaces(3334,174,"Mgambo (Handeni) ");
//        databaseHelper.insertPlaces(3335,174,"Komkonga ");
//        databaseHelper.insertPlaces(3336,174,"Mkata ");
//        databaseHelper.insertPlaces(3337,174,"Chanika");
//
//        databaseHelper.insertPlaces(3338,175,"Jaila ");
//        databaseHelper.insertPlaces(3339,175,"Kibirashi ");
////        databaseHelper.insertPlaces(3330,1,"Kikunde");
//        databaseHelper.insertPlaces(3340,175,"Kilindi ");
//        databaseHelper.insertPlaces(3341,175,"Kilwa");
//        databaseHelper.insertPlaces(3342,175,"Kimbe");
//        databaseHelper.insertPlaces(3343,175,"Kisangasa");
//        databaseHelper.insertPlaces(3344,175,"Kwediboma");
//        databaseHelper.insertPlaces(3345,175,"Kwekivu");
//        databaseHelper.insertPlaces(3346,175,"Lwande");
//        databaseHelper.insertPlaces(3347,175,"Mabalanga");
//        databaseHelper.insertPlaces(3348,175,"Masagulu");
//        databaseHelper.insertPlaces(3349,175,"Mkindi");
//        databaseHelper.insertPlaces(3350,175,"Msanja");
//        databaseHelper.insertPlaces(3351,175,"Mvungwe ");
//        databaseHelper.insertPlaces(3352,175,"Negero ");
//        databaseHelper.insertPlaces(3353,175,"Pagwi");
//        databaseHelper.insertPlaces(3354,175,"Saunyi");
//        databaseHelper.insertPlaces(3355,175,"Songe");
//        databaseHelper.insertPlaces(3356,175,"Tunguli");
//
//        databaseHelper.insertPlaces(3357,176,"Mashewa ");
//        databaseHelper.insertPlaces(3358,176,"Kizara");
//        databaseHelper.insertPlaces(3359,176,"Magoma");
//        databaseHelper.insertPlaces(3360,176,"Kerenge ");
//        databaseHelper.insertPlaces(3361,176,"Kwamndolwa ");
//        databaseHelper.insertPlaces(3362,176,"Kwagunda");
//        databaseHelper.insertPlaces(3363,176,"Mnyuzi");
//        databaseHelper.insertPlaces(3364,176,"Korogwe");
//        databaseHelper.insertPlaces(3365,176,"Ngombezi");
//        databaseHelper.insertPlaces(3366,176,"Msambiazi");
//        databaseHelper.insertPlaces(3367,176,"Vugiri");
//        databaseHelper.insertPlaces(3368,176,"Dindira");
//        databaseHelper.insertPlaces(3369,176,"Bungu");
//        databaseHelper.insertPlaces(3370,176,"Lutindi");
//        databaseHelper.insertPlaces(3371,176,"Makuyuni");
//        databaseHelper.insertPlaces(3372,176,"Chekelei");
//        databaseHelper.insertPlaces(3373,176,"Mombo");
//        databaseHelper.insertPlaces(3374,176,"Mkalamo");
//        databaseHelper.insertPlaces(3375,176,"Mazinde ");
//        databaseHelper.insertPlaces(3376,176,"Mkomazi");
//
//        databaseHelper.insertPlaces(3377,177,"Baga ");
//        databaseHelper.insertPlaces(3378,177,"Bumbuli");
//        databaseHelper.insertPlaces(3379,177,"Dule B ");
//        databaseHelper.insertPlaces(3380,177,"Dule M ");
//        databaseHelper.insertPlaces(3381,177,"Funta");
//        databaseHelper.insertPlaces(3382,177,"Gare");
//        databaseHelper.insertPlaces(3383,177,"Hamtoye");
//        databaseHelper.insertPlaces(3384,177,"Kilole (Lushoto) ");
//        databaseHelper.insertPlaces(3385,177," Kwai ");
//        databaseHelper.insertPlaces(3386,177,"Kwekanga");
//        databaseHelper.insertPlaces(3387,177,"Kwemashai");
//        databaseHelper.insertPlaces(3388,177,"Kwemshasha");
//        databaseHelper.insertPlaces(3389,177," Lukozi ");
////        databaseHelper.insertPlaces(3380,1,"Lunguza");
//        databaseHelper.insertPlaces(3390,177,"Lushoto");
//        databaseHelper.insertPlaces(3391,177,"Maheza Ngulu ");
//        databaseHelper.insertPlaces(3392,177,"Makanya");
//        databaseHelper.insertPlaces(3393,177,"Malibwi");
//        databaseHelper.insertPlaces(3394,177,"Malindi");
//        databaseHelper.insertPlaces(3395,177,"Mamba");
//        databaseHelper.insertPlaces(3396,177,"Manolo");
//        databaseHelper.insertPlaces(3397,177,"Mayo");
//        databaseHelper.insertPlaces(3398,177,"Mbaramo");
//        databaseHelper.insertPlaces(3399,177,"Mbuzii");
//        databaseHelper.insertPlaces(3400,177,"Mgwashi");
//        databaseHelper.insertPlaces(3401,177," Milingano");
//        databaseHelper.insertPlaces(3402,177,"Mlalo");
//        databaseHelper.insertPlaces(3403,177,"Mlola ");
//        databaseHelper.insertPlaces(3404,177,"Mnazi");
//        databaseHelper.insertPlaces(3405,177,"Mng'aro");
//        databaseHelper.insertPlaces(3406,177,"Mponde");
//        databaseHelper.insertPlaces(3407,177,"Mtae");
//        databaseHelper.insertPlaces(3408,177,"Mwangoi ");
//        databaseHelper.insertPlaces(3409,177,"Ngulwi ");
//        databaseHelper.insertPlaces(3410,177,"Ngwelo");
//        databaseHelper.insertPlaces(3411,177,"Nkongoi");
//        databaseHelper.insertPlaces(3412,177,"Rangwi");
//        databaseHelper.insertPlaces(3413,177,"Shume");
//        databaseHelper.insertPlaces(3414,177,"Soni ");
//        databaseHelper.insertPlaces(3415,177," Sunga ");
//        databaseHelper.insertPlaces(3416,177,"Tamota");
//        databaseHelper.insertPlaces(3417,177,"Ubiri");
//        databaseHelper.insertPlaces(3418,177,"Usambara");
//        databaseHelper.insertPlaces(3419,177,"Vuga");
//
//
//        databaseHelper.insertPlaces(3420,178,"Boma ");
//        databaseHelper.insertPlaces(3421,178,"Bosha");
//        databaseHelper.insertPlaces(3422,178,"Bwiti");
//        databaseHelper.insertPlaces(3423,178,"Daluni ");
//        databaseHelper.insertPlaces(3424,178,"Doda");
//        databaseHelper.insertPlaces(3425,178,"Duga ");
//        databaseHelper.insertPlaces(3426,178,"Gombero ");
//        databaseHelper.insertPlaces(3427,178,"Kigongoi Magharibi");
//        databaseHelper.insertPlaces(3428,178,"Kigongoi Mashariki");
//        databaseHelper.insertPlaces(3429,178,"Kwale");
//        databaseHelper.insertPlaces(3430,178,"Manza");
//        databaseHelper.insertPlaces(3431,178,"Maramba ");
//        databaseHelper.insertPlaces(3432,178,"Mayomboni");
//        databaseHelper.insertPlaces(3433,178,"Mhinduro ");
//        databaseHelper.insertPlaces(3434,178,"Mkinga ");
//        databaseHelper.insertPlaces(3435,178,"Mnyenzani");
//        databaseHelper.insertPlaces(3436,178,"Moa");
//        databaseHelper.insertPlaces(3437,178,"Mtimbwani");
//        databaseHelper.insertPlaces(3438,178,"Mwakijembe");
//        databaseHelper.insertPlaces(3439,178,"Parungu Kasera ");
//        databaseHelper.insertPlaces(3440,178,"Sigaya");
//
//        databaseHelper.insertPlaces(3441,179,"Amani");
//        databaseHelper.insertPlaces(3442,179,"Bwembwera");
//        databaseHelper.insertPlaces(3443,179,"Genge");
//        databaseHelper.insertPlaces(3444,179,"Kicheba");
//        databaseHelper.insertPlaces(3445,179,"Kigombe");
//        databaseHelper.insertPlaces(3446,179,"Kilulu ");
//        databaseHelper.insertPlaces(3447,179,"Kisiwani");
//        databaseHelper.insertPlaces(3448,179," Kwafungo");
//        databaseHelper.insertPlaces(3449,179," Kwakifua");
//        databaseHelper.insertPlaces(3450,179,"Kwemkabala");
//        databaseHelper.insertPlaces(3451,179,"Lusanga");
//        databaseHelper.insertPlaces(3452,179,"Magila");
//        databaseHelper.insertPlaces(3453,179,"Magoroto");
//        databaseHelper.insertPlaces(3454,179,"Majengo ");
//        databaseHelper.insertPlaces(3455,179,"Masuguru");
//        databaseHelper.insertPlaces(3456,179,"Mbaramo");
//        databaseHelper.insertPlaces(3457,179,"Mbomole");
//        databaseHelper.insertPlaces(3458,179,"Mhamba");
//        databaseHelper.insertPlaces(3459,179,"Misalai");
//        databaseHelper.insertPlaces(3460,179," Misozwe");
//        databaseHelper.insertPlaces(3461,179,"Mkuzi");
//        databaseHelper.insertPlaces(3462,179,"Mlingoni");
//        databaseHelper.insertPlaces(3463,179,"Mpapayu");
//        databaseHelper.insertPlaces(3464,179,"Mtindiro");
//        databaseHelper.insertPlaces(3465,179,"Ngomeni");
//        databaseHelper.insertPlaces(3466,179,"Nkumba");
//        databaseHelper.insertPlaces(3467,179,"Pande Darajani");
//        databaseHelper.insertPlaces(3468,179,"Potwe");
//        databaseHelper.insertPlaces(3469,179,"Songa");
//        databaseHelper.insertPlaces(3470,179,"Tanganyika");
//        databaseHelper.insertPlaces(3471,179,"Tingeni");
//        databaseHelper.insertPlaces(3472,179,"Tongwe");
//        databaseHelper.insertPlaces(3473,179,"Zirai");
//
//        databaseHelper.insertPlaces(3474,180,"Bushiri");
//        databaseHelper.insertPlaces(3475,180,"Bweni");
//        databaseHelper.insertPlaces(3476,180,"Kimang'a");
//        databaseHelper.insertPlaces(3477,180,"Kipumbwi");
//        databaseHelper.insertPlaces(3478,180,"Madanga");
//        databaseHelper.insertPlaces(3479,180,"Mkunguni");
//        databaseHelper.insertPlaces(3480,180,"Mkalamo (Pangani)");
//        databaseHelper.insertPlaces(3481,180,"Mkwaja ");
//        databaseHelper.insertPlaces(3482,180,"Mwera");
//        databaseHelper.insertPlaces(3483,180,"Pangani Magharibi");
//        databaseHelper.insertPlaces(3484,180,"Pangani Mashariki");
//        databaseHelper.insertPlaces(3485,180,"Tungamaa");
//        databaseHelper.insertPlaces(3486,180,"Ubangaa");
//
//        databaseHelper.insertPlaces(3487,181,"Central");
//        databaseHelper.insertPlaces(3488,181,"Chongoleani ");
//        databaseHelper.insertPlaces(3489,181,"Chumbageni");
//        databaseHelper.insertPlaces(3490,181,"Duga");
//        databaseHelper.insertPlaces(3491,181,"Kiomoni");
//        databaseHelper.insertPlaces(3492,181,"Kirare");
//        databaseHelper.insertPlaces(3493,181,"Mabawa");
//        databaseHelper.insertPlaces(3494,181,"Mabokweni");
//        databaseHelper.insertPlaces(3495,181,"Majengo");
//        databaseHelper.insertPlaces(3496,181,"Makorora");
//        databaseHelper.insertPlaces(3497,181,"Marungu");
//        databaseHelper.insertPlaces(3498,181,"Marungu");
//        databaseHelper.insertPlaces(3499,181,"Maweni");
//        databaseHelper.insertPlaces(3500,181,"Msambweni");
//        databaseHelper.insertPlaces(3501,181,"Mwanzange");
//        databaseHelper.insertPlaces(3502,181,"Mzingani");
//        databaseHelper.insertPlaces(3503,181,"Mzizima ");
//        databaseHelper.insertPlaces(3504,181,"Ngamiani Kaskazini");
//        databaseHelper.insertPlaces(3505,181,"Ngamiani Kati ");
//        databaseHelper.insertPlaces(3506,181,"Ngamiani Kusini");
//        databaseHelper.insertPlaces(3507,181,"Nguvumali ");
//        databaseHelper.insertPlaces(3508,181," Pongwe");
//        databaseHelper.insertPlaces(3509,181,"Tangasisi ");
//        databaseHelper.insertPlaces(3510,181,"Tongoni");
//        databaseHelper.insertPlaces(3511,181,"Usagara");
//
//
//        //mikoa ya zanzibar
//        databaseHelper.insertPlaces(3512,0,"Pemba Kaskazini");
//        databaseHelper.insertPlaces(3513,0,"Pemba Kusini");
//        databaseHelper.insertPlaces(3514,0,"Zanzibar Kati/Kusini");
//        databaseHelper.insertPlaces(3515,0,"Zanzibar Mjini Kaskazi");
//        databaseHelper.insertPlaces(3516,0,"Zanzibar Mjini Magharibi");
//
////willaya za zanzibar
//        databaseHelper.insertPlaces(3517,3512,"Wete");
//        databaseHelper.insertPlaces(3518,3512,"Micheweni");
//        databaseHelper.insertPlaces(3519,3513,"Chake Chake");
//        databaseHelper.insertPlaces(3520,3513,"Mkoani");
//        databaseHelper.insertPlaces(3521,3514,"Zanzibar Kati");
//        databaseHelper.insertPlaces(3522,3514,"Zanzibar Kusini");
//        databaseHelper.insertPlaces(3523,3515,"Zanzibar Mjini Kaskazini A");
//        databaseHelper.insertPlaces(3524,3515,"Zanzibar Mjini Kaskazini B");
//        databaseHelper.insertPlaces(3525,3516,"Zanzibar Mjini");
//        databaseHelper.insertPlaces(3526,3516,"Zanzibar Mjini Magaribi");
////kata za wete
//        databaseHelper.insertPlaces(3527,3517,"Bopwe");
//        databaseHelper.insertPlaces(3528,3517,"Chwale");
//        databaseHelper.insertPlaces(3529,3517,"Fundo");
//        databaseHelper.insertPlaces(3530,3517,"Gando");
//        databaseHelper.insertPlaces(3531,3517,"Jadida");
//        databaseHelper.insertPlaces(3532,3517,"Junguni");
//        databaseHelper.insertPlaces(3533,3517,"Kambini");
//        databaseHelper.insertPlaces(3534,3517,"Kangagani");
//        databaseHelper.insertPlaces(3535,3517,"Kinyikani");
//        databaseHelper.insertPlaces(3536,3517,"Kipangani");
//        databaseHelper.insertPlaces(3537,3517,"Kisiwani");
//        databaseHelper.insertPlaces(3538,3517,"Kiungoni ");
//        databaseHelper.insertPlaces(3539,3517,"Kiuyu Kigongoni ");
//        databaseHelper.insertPlaces(3540,3517,"Kiuyu Minungwini ");
//        databaseHelper.insertPlaces(3541,3517,"Kizimbani");
//        databaseHelper.insertPlaces(3542,3517,"Kojani");
//        databaseHelper.insertPlaces(3543,3517,"Limbani ");
//        databaseHelper.insertPlaces(3544,3517,"Maziwani");
//        databaseHelper.insertPlaces(3545,3517,"Mchanga Mdogo ");
//        databaseHelper.insertPlaces(3546,3517,"Mjini Ole ");
//        databaseHelper.insertPlaces(3547,3517,"Mpambani ");
//        databaseHelper.insertPlaces(3548,3517,"Mtambwe Kaskazini ");
//        databaseHelper.insertPlaces(3549,3517,"Mtambwe Kusini ");
//        databaseHelper.insertPlaces(3550,3517,"Mzambarauni Takao");
//        databaseHelper.insertPlaces(3551,3517,"Ole ");
//        databaseHelper.insertPlaces(3552,3517,"Pandani ");
//        databaseHelper.insertPlaces(3553,3517,"Pembeni ");
//        databaseHelper.insertPlaces(3554,3517,"Piki ");
//        databaseHelper.insertPlaces(3555,3517,"Selem ");
//        databaseHelper.insertPlaces(3556,3517,"Shengejuu");
//        databaseHelper.insertPlaces(3557,3517,"Ukunjwi ");
//        databaseHelper.insertPlaces(3558,3517,"Utaani");
//        //kata za wilaya ya micheweni
//        databaseHelper.insertPlaces(3559,3518,"Chimba ");
//        databaseHelper.insertPlaces(3560,3518,"Finya");
//        databaseHelper.insertPlaces(3561,3518,"Kifundi");
//        databaseHelper.insertPlaces(3562,3518,"Kinowe");
//        databaseHelper.insertPlaces(3563,3518,"Kinyasini");
//        databaseHelper.insertPlaces(3564,3518,"Kiuyu Mbuyuni");
//        databaseHelper.insertPlaces(3565,3518,"Konde Pemba");
//        databaseHelper.insertPlaces(3566,3518,"Majenzi ");
//        databaseHelper.insertPlaces(3567,3518,"Makangale");
//        databaseHelper.insertPlaces(3568,3518,"Maziwa Ng'ombe ");
//        databaseHelper.insertPlaces(3569,3518,"Mgogoni");
//        databaseHelper.insertPlaces(3570,3518,"Micheweni");
//        databaseHelper.insertPlaces(3571,3518,"Mihogoni");
//        databaseHelper.insertPlaces(3572,3518," Mjini Wingwi ");
//        databaseHelper.insertPlaces(3573,3518,"Mlindo");
//        databaseHelper.insertPlaces(3574,3518,"Msuka Magharibi");
//        databaseHelper.insertPlaces(3575,3518," Msuka Mashariki");
//        databaseHelper.insertPlaces(3576,3518,"Mtemani");
//        databaseHelper.insertPlaces(3577,3518,"Shumba Mjini");
//        databaseHelper.insertPlaces(3578,3518,"Sizini");
//        databaseHelper.insertPlaces(3579,3518,"Tondooni");
//        databaseHelper.insertPlaces(3580,3518,"Tumbe Magharibi ");
//        databaseHelper.insertPlaces(3581,3518,"Tumbe Mashariki");
//        databaseHelper.insertPlaces(3582,3518,"Wingwi Mapofu");
//        databaseHelper.insertPlaces(3583,3518,"Wingwi Mjananza");
//        databaseHelper.insertPlaces(3584,3518,"Wingwi Njuguni");
//
//        //kata za chake chake
//        databaseHelper.insertPlaces(3585,3519,"Chachani");
//        databaseHelper.insertPlaces(3586,3519,"Chanjaani");
//        databaseHelper.insertPlaces(3587,3519,"Chonga (Chakechake) ");
//        databaseHelper.insertPlaces(3588,3519,"Kilindi (Chakechake)");
//        databaseHelper.insertPlaces(3589,3519,"Kwale (Chakechake)");
//        databaseHelper.insertPlaces(3590,3519," Matale (Chakechake)");
//        databaseHelper.insertPlaces(3591,3519,"Mgelema");
//        databaseHelper.insertPlaces(3592,3519,"Mvumoni");
//        databaseHelper.insertPlaces(3593,3519,"Ndagoni");
//        databaseHelper.insertPlaces(3594,3519,"Ng'ambwa (Pemba) ");
//        databaseHelper.insertPlaces(3595,3519,"Pujini");
//        databaseHelper.insertPlaces(3596,3519,"Shungi ");
//        databaseHelper.insertPlaces(3597,3519,"Tibirinzi");
//        databaseHelper.insertPlaces(3598,3519,"Uwandani");
//        databaseHelper.insertPlaces(3599,3519,"Vitongoji (Chakechake)");
//        databaseHelper.insertPlaces(3600,3519,"Wara");
//        databaseHelper.insertPlaces(3601,3519," Wawi ");
//        databaseHelper.insertPlaces(3602,3519,"Wesha");
//        databaseHelper.insertPlaces(3603,3519,"Ziwani (Chakechake)");
//
//        //kata za wilaya ya mkoani
//        databaseHelper.insertPlaces(3604,3520,"Chambani");
//        databaseHelper.insertPlaces(3605,3520,"Chokocho ");
//        databaseHelper.insertPlaces(3606,3520,"Chumbageni (Pemba)");
//        databaseHelper.insertPlaces(3607,3520,"Kangani");
//        databaseHelper.insertPlaces(3608,3520,"Kengeja ");
//        databaseHelper.insertPlaces(3609,3520,"Kisiwa Panza ");
//        databaseHelper.insertPlaces(3610,3520,"Kiwani");
//        databaseHelper.insertPlaces(3611,3520,"Makombeni");
//        databaseHelper.insertPlaces(3612,3520,"Makoongwe ");
//        databaseHelper.insertPlaces(3613,3520,"Mbuguani ");
//        databaseHelper.insertPlaces(3614,3520,"Michenzani ");
//        databaseHelper.insertPlaces(3615,3520,"Mizingani");
//        databaseHelper.insertPlaces(3616,3520,"Mkanyageni");
//        databaseHelper.insertPlaces(3617,3520,"Mtambile ");
//        databaseHelper.insertPlaces(3618,3520,"Mtangani");
//        databaseHelper.insertPlaces(3619,3520,"Muambe");
//        databaseHelper.insertPlaces(3620,3520,"Ngombeni");
//        databaseHelper.insertPlaces(3621,3520,"Ngwachani");
//        databaseHelper.insertPlaces(3622,3520,"Shidi ");
//        databaseHelper.insertPlaces(3623,3520,"Ukutini");
//        databaseHelper.insertPlaces(3624,3520,"Uweleni");
//        databaseHelper.insertPlaces(3625,3520,"Wambaa");
//
//        //kata za wilaya ya zanzibar kati
//        databaseHelper.insertPlaces(3626,3521,"Bambi");
//        databaseHelper.insertPlaces(3627,3521,"Binguni ");
//        databaseHelper.insertPlaces(3628,3521,"Bungi ");
//        databaseHelper.insertPlaces(3629,3521,"Charawe");
//        databaseHelper.insertPlaces(3630,3521," Cheju");
//        databaseHelper.insertPlaces(3631,3521,"Chwaka ");
//        databaseHelper.insertPlaces(3632,3521,"Dunga Bweni");
//        databaseHelper.insertPlaces(3633,3521,"Dunga Kiembeni");
//        databaseHelper.insertPlaces(3634,3521,"Ghana (Unguja)");
//        databaseHelper.insertPlaces(3635,3521,"Jendele");
//        databaseHelper.insertPlaces(3636,3521,"Jumbi");
//        databaseHelper.insertPlaces(3637,3521,"Kiboje Mkwajuni");
//        databaseHelper.insertPlaces(3638,3521,"Kiboje Mwembe Shauri");
//        databaseHelper.insertPlaces(3639,3521,"Kidimni");
//        databaseHelper.insertPlaces(3640,3521,"Kikungwi");
//        databaseHelper.insertPlaces(3641,3521,"Koani");
//        databaseHelper.insertPlaces(3642,3521,"Machui");
//        databaseHelper.insertPlaces(3643,3521,"Marumbi ");
//        databaseHelper.insertPlaces(3644,3521,"Mchangani (Kati Unguja)");
//        databaseHelper.insertPlaces(3645,3521,"Mgeni Haji");
//        databaseHelper.insertPlaces(3646,3521,"Michamvi");
//        databaseHelper.insertPlaces(3647,3521,"Mitakawani");
//        databaseHelper.insertPlaces(3648,3521,"Miwani (Unguja) ");
//        databaseHelper.insertPlaces(3649,3521,"Mpapa (Unguja)");
//        databaseHelper.insertPlaces(3650,3521,"Ndijani");
//        databaseHelper.insertPlaces(3651,3521,"Ng'ambwa (Unguja)");
//        databaseHelper.insertPlaces(3652,3521,"Pagali");
//        databaseHelper.insertPlaces(3653,3521,"Pongwe (Unguja) ");
//        databaseHelper.insertPlaces(3654,3521,"Tunduni");
//        databaseHelper.insertPlaces(3655,3521,"Tunguu");
//        databaseHelper.insertPlaces(3656,3521,"Ubago");
//        databaseHelper.insertPlaces(3657,3521,"Ukongoroni");
//        databaseHelper.insertPlaces(3658,3521,"Umbuji");
//        databaseHelper.insertPlaces(3659,3521,"Unguja Ukuu Kaebona");
//        databaseHelper.insertPlaces(3660,3521,"Unguja Ukuu Kaepwani");
//        databaseHelper.insertPlaces(3661,3521,"Uroa ");
//        databaseHelper.insertPlaces(3662,3521,"Uzi (Unguja)");
//        databaseHelper.insertPlaces(3663,3521,"Uzini");
//
//        //kata za wilaya ya zanzibar kusini
//        databaseHelper.insertPlaces(3664,3522,"Bwejuu");
//        databaseHelper.insertPlaces(3665,3522,"Jambiani Kibigija");
//        databaseHelper.insertPlaces(2666,3522,"Jambiani Kikadini ");
//        databaseHelper.insertPlaces(3667,3522,"Kajengwa ");
//        databaseHelper.insertPlaces(3668,3522,"Kibuteni");
//        databaseHelper.insertPlaces(3669,3522,"Kijini");
//        databaseHelper.insertPlaces(3670,3522,"Kitogani");
//        databaseHelper.insertPlaces(3671,3522," Kizimkazi Dimbani");
//        databaseHelper.insertPlaces(3672,3522,"Kizimkazi Mkunguni");
//        databaseHelper.insertPlaces(3673,3522,"Mtende ");
//        databaseHelper.insertPlaces(3674,3522,"Muungoni ");
//        databaseHelper.insertPlaces(3675,3522,"Muyuni A");
//        databaseHelper.insertPlaces(3676,3522,"Muyuni B");
//        databaseHelper.insertPlaces(3677,3522,"Muyuni C");
//        databaseHelper.insertPlaces(3678,3522,"Mzuri (Unguja)");
//        databaseHelper.insertPlaces(3679,3522,"Nganani");
//        databaseHelper.insertPlaces(3680,3522,"Paje");
//        databaseHelper.insertPlaces(3681,3522,"Pete (Unguja)");
//
//        //kata za wilaya ya mjini
//        databaseHelper.insertPlaces(3682,3525,"Amaani");
//        databaseHelper.insertPlaces(3683,3525,"Chumbuni");
//        databaseHelper.insertPlaces(3684,3525,"Gulioni");
//        databaseHelper.insertPlaces(3685,3525,"Jang'ombe");
//        databaseHelper.insertPlaces(3686,3525,"Karakana");
//        databaseHelper.insertPlaces(3687,3525,"Kidongo Chekundu");
//        databaseHelper.insertPlaces(3688,3525,"Kikwajuni Bondeni");
//        databaseHelper.insertPlaces(3689,3525,"Kikwajuni Juu");
//        databaseHelper.insertPlaces(3690,3525,"Kilimahewa ");
//        databaseHelper.insertPlaces(3691,3525,"Kilimani");
//        databaseHelper.insertPlaces(3692,3525,"Kiponda");
//        databaseHelper.insertPlaces(3693,3525,"Kisima Majongoo");
//        databaseHelper.insertPlaces(3694,3525,"Kwaalamsha ");
//        databaseHelper.insertPlaces(3695,3525,"Kwaalinato ");
//        databaseHelper.insertPlaces(3696,3525,"Kwahani");
//        databaseHelper.insertPlaces(3697,3525,"Kwamtipura ");
//        databaseHelper.insertPlaces(3698,3525,"Magomeni");
//        databaseHelper.insertPlaces(3699,3525,"Makadara");
//        databaseHelper.insertPlaces(3700,3525,"Malindi");
//        databaseHelper.insertPlaces(3701,3525,"Matarumbeta");
//        databaseHelper.insertPlaces(3702,3525,"Mchangani");
//        databaseHelper.insertPlaces(3703,3525,"Miembeni");
//        databaseHelper.insertPlaces(3704,3525,"Mikunguni");
//        databaseHelper.insertPlaces(3705,3525,"Mkele ");
//        databaseHelper.insertPlaces(3706,3525,"Mkunazini");
//        databaseHelper.insertPlaces(3707,3525,"Mlandege");
//        databaseHelper.insertPlaces(3708,3525,"Mpendae");
//        databaseHelper.insertPlaces(3709,3525,"Muembe Ladu");
//        databaseHelper.insertPlaces(3710,3525,"Muembe Makumbi");
//        databaseHelper.insertPlaces(3711,3525,"Muungano");
//        databaseHelper.insertPlaces(3712,3525,"Mwembeshauri");
//        databaseHelper.insertPlaces(3713,3525,"Mwembetanga");
//        databaseHelper.insertPlaces(3714,3525,"Nyerere");
//        databaseHelper.insertPlaces(3715,3525,"Rahaleo");
//        databaseHelper.insertPlaces(3716,3525,"Sebleni");
//        databaseHelper.insertPlaces(3717,3525,"Shangani ");
//        databaseHelper.insertPlaces(3718,3525,"Shaurimoyo");
//        databaseHelper.insertPlaces(3719,3525,"Sogea");
//        databaseHelper.insertPlaces(3720,3525,"Urusi");
//        databaseHelper.insertPlaces(3721,3525,"Vikokotoni");
//
//        //kata za wilaya magharibi
//        databaseHelper.insertPlaces(3722,3526,"Bububu");
//        databaseHelper.insertPlaces(3723,3526,"Bumbwisudi");
//        databaseHelper.insertPlaces(3724,3526,"Bweleo");
//        databaseHelper.insertPlaces(3725,3526,"Chuini");
//        databaseHelper.insertPlaces(3726,3526,"Chukwani");
//        databaseHelper.insertPlaces(3727,3526,"Dimani");
//        databaseHelper.insertPlaces(3728,3526,"Dole");
//        databaseHelper.insertPlaces(3729,3526,"Fumba");
//        databaseHelper.insertPlaces(3730,3526,"uoni Kibondeni");
//        databaseHelper.insertPlaces(3731,3526,"Fuoni Kijitoupele");
//        databaseHelper.insertPlaces(3732,3526,"Kama");
//        databaseHelper.insertPlaces(3733,3526,"Kianga");
//        databaseHelper.insertPlaces(3734,3526,"Kiembe Samaki");
//        databaseHelper.insertPlaces(3735,3526,"Kizimbani");
//        databaseHelper.insertPlaces(3736,3526,"Kombeni");
//        databaseHelper.insertPlaces(3737,3526,"Magogoni ");
//        databaseHelper.insertPlaces(3738,3526,"Maungani ");
//        databaseHelper.insertPlaces(3739,3526,"Mbuzini");
//        databaseHelper.insertPlaces(3740,3526,"Mfenesini");
//        databaseHelper.insertPlaces(3741,3526,"Mto Pepo");
//        databaseHelper.insertPlaces(3742,3526,"Mtoni");
//        databaseHelper.insertPlaces(3743,3526,"Mtoni Kidatu");
//        databaseHelper.insertPlaces(3744,3526,"Mwakaje");
//        databaseHelper.insertPlaces(3745,3526,"Mwanakwerekwe");
//        databaseHelper.insertPlaces(3746,3526,"Mwanyanya");
//        databaseHelper.insertPlaces(3747,3526,"Mwera ");
//        databaseHelper.insertPlaces(3748,3526,"Shakani");
//        databaseHelper.insertPlaces(3749,3526,"Tomondo");
//        databaseHelper.insertPlaces(3750,3526,"Welezo");
//
//        //kata za wilaya ya unguja kaskazini A
//        databaseHelper.insertPlaces(3751,3523,"Bandamaji");
//        databaseHelper.insertPlaces(3752,3523,"Bwereu");
//        databaseHelper.insertPlaces(3753,3523,"Chaani Kubwa");
//        databaseHelper.insertPlaces(3754,3523,"Chaani Masingini");
//        databaseHelper.insertPlaces(3755,3523,"Chutama");
//        databaseHelper.insertPlaces(3756,3523,"Fukuchani");
//        databaseHelper.insertPlaces(3757,3523,"Gamba");
//        databaseHelper.insertPlaces(3758,3523,"Kandwi");
//        databaseHelper.insertPlaces(3759,3523,"Kibeni");
//        databaseHelper.insertPlaces(3760,3523,"Kidombo");
//        databaseHelper.insertPlaces(3761,3523,"Kidoti");
//        databaseHelper.insertPlaces(3762,3523,"Kigomani");
//        databaseHelper.insertPlaces(3763,3523,"Kigunda");
//        databaseHelper.insertPlaces(3764,3523,"Kijini");
//        databaseHelper.insertPlaces(3765,3523,"Kikobweni");
//        databaseHelper.insertPlaces(3766,3523,"Kilimani");
//        databaseHelper.insertPlaces(3767,3523,"Kilindi");
//        databaseHelper.insertPlaces(3768,3523,"Kinyasini");
//        databaseHelper.insertPlaces(3770,3523,"Kisongoni");
//        databaseHelper.insertPlaces(3771,3523,"Kivunge ");
//        databaseHelper.insertPlaces(3772,3523,"Matemwe");
//        databaseHelper.insertPlaces(3773,3523,"Mchenza Shauri");
//        databaseHelper.insertPlaces(3774,3523,"Mkokotoni Unguja");
//        databaseHelper.insertPlaces(3775,3523,"Mkwajuni");
//        databaseHelper.insertPlaces(3776,3523,"Moga");
//        databaseHelper.insertPlaces(3777,3523,"Mto wa Pwani");
//        databaseHelper.insertPlaces(3778,3523,"Muwange");
//        databaseHelper.insertPlaces(3779,3523,"Nungwi");
//        databaseHelper.insertPlaces(3780,3523,"Pale");
//        databaseHelper.insertPlaces(3781,3523,"Pitanazako");
//        databaseHelper.insertPlaces(3782,3523,"Potoa");
//        databaseHelper.insertPlaces(3783,3523,"Pwani Mchangani");
//        databaseHelper.insertPlaces(3784,3523,"Tazari");
//        databaseHelper.insertPlaces(3785,3523,"Tumbatu Gomani");
//        databaseHelper.insertPlaces(3786,3523,"Tumbatu Jongowe");
//        databaseHelper.insertPlaces(3787,3523,"Uvivini");
//
//
//        //kata za wilaya ya unguja Kaskazini B
//        databaseHelper.insertPlaces(3788,3524,"Donge Karange");
//        databaseHelper.insertPlaces(3789,3524,"Donge Kipange");
//        databaseHelper.insertPlaces(3790,3524,"Donge Mbiji");
//        databaseHelper.insertPlaces(3791,3524,"Donge Mchangani");
//        databaseHelper.insertPlaces(3792,3524,"Donge Mnyimbi");
//        databaseHelper.insertPlaces(3793,3524,"Donge Mtambile");
//        databaseHelper.insertPlaces(3794,3524,"Donge Vijibweni");
//        databaseHelper.insertPlaces(3795,3524,"Fujoni");
//        databaseHelper.insertPlaces(3796,3524,"Kilombero (Unguja)");
//        databaseHelper.insertPlaces(3797,3524,"Kinduni");
//        databaseHelper.insertPlaces(3798,3524,"Kiomba Mvua");
//        databaseHelper.insertPlaces(3799,3524,"Kitope");
//        databaseHelper.insertPlaces(3800,3524,"Kiwengwa");
//        databaseHelper.insertPlaces(3801,3524,"Mahonda ");
//        databaseHelper.insertPlaces(3802,3524,"Makoba");
//        databaseHelper.insertPlaces(3803,3524,"Manga Pwani");
//        databaseHelper.insertPlaces(3804,3524,"Mgambo (Unguja)");
//        databaseHelper.insertPlaces(3805,3524,"Misufini (Unguja)");
//        databaseHelper.insertPlaces(3806,3524,"Mkadini");
//        databaseHelper.insertPlaces(3807,3524,"Muwanda");
//        databaseHelper.insertPlaces(3808,3524,"Pangeni");
//        databaseHelper.insertPlaces(3809,3524,"Upenja");
//        databaseHelper.insertPlaces(3810,3524,"Zingwe Zingwe");
//
//
//        //baadhi ya kata za wilaya 174
//        databaseHelper.insertPlaces(3811,174,"Mazingara");
//        databaseHelper.insertPlaces(3812,174,"Kwamsisi");
//        databaseHelper.insertPlaces(3813,174,"Kwasunga");
//        databaseHelper.insertPlaces(3814,174,"Kwaluguru");
//        databaseHelper.insertPlaces(3815,174,"Kang'ata");
//        databaseHelper.insertPlaces(3816,174,"Kwamkonje");
//        databaseHelper.insertPlaces(3817,174,"Vibaoni");
//        databaseHelper.insertPlaces(3818,174,"Sindeni");
//        databaseHelper.insertPlaces(3819,174,"Misima");
//        databaseHelper.insertPlaces(3820,174,"Kiva");
//

    };
}

