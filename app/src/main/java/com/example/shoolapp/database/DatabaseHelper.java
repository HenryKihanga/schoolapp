package com.example.shoolapp.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;

import androidx.annotation.RequiresApi;

import com.example.shoolapp.model.PasswordUtils;
import com.example.shoolapp.model.User;

import java.util.ArrayList;

public class DatabaseHelper extends SQLiteOpenHelper {
  public static final String Database_Name = "SchoolAppDB";
  public static final int Database_version = 1;

  PasswordUtils passwordUtils = new PasswordUtils();

  //CONSTANTS FOR TABLE PLACES
  public static final String Table_places = "places";
  public static final String Places_Col_1 = "id";
  public static final String Places_Col_2 = "parent_id";
  public static final String Places_Col_3 = "name";
  public static final String Delete_Table_Places = "DROP TABLE IF EXISTS " + Table_places;
  public static final String Create_Table_Places = "CREATE TABLE IF NOT EXISTS "+ Table_places + "("
          +Places_Col_1+" INTEGER PRIMARY KEY NOT NULL UNIQUE,"
          +Places_Col_2+" INTEGER,"
          +Places_Col_3+" TEXT NOT NULL )";
  //CONSTANTS FOR TABLE COURSES
  public static final String Table_courses = "courses";
  public static final String Courses_Col_1 = "course_code";
  public static final String Courses_Col_2 = "course_name";
  public static final String Courses_Col_3 = "year_of_study";
  public static final String Courses_Col_4 = "semester_of_study";
  public static final String Courses_Col_5 = "day";
  public static final String Courses_Col_6 = "time";
  public static final String Courses_Col_7 = "venue";
  public static final String Delete_Table_Courses = "DROP TABLE IF EXISTS " + Table_courses;
  public static final String Create_Table_Courses = "CREATE TABLE IF NOT EXISTS " + Table_courses + "("
          +Courses_Col_1+ " CHAR(5) PRIMARY KEY NOT NULL UNIQUE,"
          +Courses_Col_2+ " VARCHAR(30) NOT NULL,"
          +Courses_Col_3+ " INTEGER NOT NULL,"
          +Courses_Col_4+ " INTEGER NOT NULL,"
          +Courses_Col_5+ " VARCHAR(20),"
          +Courses_Col_6+ " TIME,"
          +Courses_Col_7 +" VARCHAR(20) )";
  //CONSTANTS FOR TABLE PROGRAMS
    public static final String Table_Programs = "programs";
    public static final String Programs_Col_1 = "program_id";
    public static final String Programs_Col_2 = "program_name";
    public static final String Delete_Table_Programs = "DROP TABLE IF EXISTS " + Table_Programs;
    public static final String Create_Table_Programs = "CREATE TABLE IF NOT EXISTS "+ Table_Programs + "("
            +Programs_Col_1+" INTEGER PRIMARY KEY,"
            +Programs_Col_2+" VARCHAR(50) NOT NULL )";

    //CONSTANTS FOR TABLE STAFFS
    public static final String Table_staffs = "staffs";
    public static final String Staffs_Col_1 = "staff_id";
    public static final String Staffs_Col_2 = "first_name";
    public static final String Staffs_Col_3 = "middle_name";
    public static final String Staffs_Col_4 = "last_name";
    public static final String Staffs_Col_5 = "phone_number";
    public static final String Staffs_Col_6 = "emails";
    public static final String Staffs_Col_7 = "gender";
    public static final String Staffs_Col_8 = "address";
    public static final String Staffs_Col_9 = "date_of_birth";
    public static final String Staffs_Col_10 = "profile_picture_file";
    public static final String Delete_Table_Staffs = "DROP TABLE IF EXISTS " + Table_staffs;
    public static final String Create_Table_Staffs = "CREATE TABLE IF NOT EXISTS " + Table_staffs + "("
            +Staffs_Col_1 + " CHAR(5) PRIMARY KEY NOT NULL UNIQUE,"
            +Staffs_Col_2 + " VARCHAR(15) NOT NULL,"
            +Staffs_Col_3 + " VARCHAR(15) ,"
            +Staffs_Col_4 + " VARCHAR(15) NOT NULL,"
            +Staffs_Col_5 + " CHAR(10),"
            +Staffs_Col_6 + " VARCHAR(30),"
            +Staffs_Col_7 + " CHAR ,"
            +Staffs_Col_8 + " INTEGER ,"
            +Staffs_Col_9 + " DATE,"
            +Staffs_Col_10+ " TEXT )";
    //CONSTANTS FOR TABLE STUDENTS
    public static final String Table_students = "students";
    public static final String Students_Col_1 = "reg_no";
    public static final String Students_Col_2 = "first_name";
    public static final String Students_Col_3 = "middle_name";
    public static final String Students_Col_4 = "last_name";
    public static final String Students_Col_5 = "phone_number";
    public static final String Students_Col_6 = "emails";
    public static final String Students_Col_7 = "gender";
    public static final String Students_Col_9 = "date_of_birth";
    public static final String Students_Col_10 = "profile_picture_file";
    public static final String Students_Col_11 = "year_of_study";
    public static final String Students_Col_12 = "semester_of_study";
    public static final String Students_Col_13 = "degree_program_id";
    public static final String Students_Col_14 = "date_of_registration";
    public static final String Delete_Table_Students = "DROP TABLE IF EXISTS " + Table_students;
    public static final String Create_Table_Students = "CREATE TABLE IF NOT EXISTS " + Table_students + "("
            +Students_Col_1 + " CHAR(13) PRIMARY KEY NOT NULL UNIQUE,"
            +Students_Col_2 + " VARCHAR(15) NOT NULL,"
            +Students_Col_3 + " VARCHAR(15) ,"
            +Students_Col_4 + " VARCHAR(15) NOT NULL,"
            +Students_Col_5 + " CHAR(10),"
            +Students_Col_6 + " VARCHAR(30),"
            +Students_Col_7 + " CHAR ,"
            +Students_Col_9 + " DATE,"
            +Students_Col_10+ " TEXT ,"
            +Students_Col_11 + " INTEGER,"
            +Students_Col_12 + " INTEGER,"
            +Students_Col_13 + " INTEGER REFERENCES programs(program_code),"
            +Students_Col_14 + " DATE)";
    //CONSTANTS FOR TABLE ROLES
    public static final String Table_Roles = "roles";
    public static final String Roles_Col_1 = "role_id";
    public static final String Roles_Col_2 = "role_name";
    public static final String Roles_Col_3 = "role_description";
    public static final String Delete_Table_Roles = "DROP TABLE IF EXISTS " + Table_Roles;
    public static final String Create_Table_Roles = "CREATE TABLE IF NOT EXISTS "+ Table_Roles + "("
            +Roles_Col_1+" INTEGER PRIMARY KEY,"
            +Roles_Col_2+" VARCHAR(10) NOT NULL,"
            +Roles_Col_3 +" VARCHAR(50) NOT NULL )";

    //CONSTANTS FOR TABLE USER
    public static final String Table_Users = "users";
    public static final String Users_Col_1 = "user_name";
    public static final String Users_Col_2 = "user_role_id";
    public static final String Users_Col_3 = "password";
    public static final String Users_Col_4 = "salt";
    public static final String Delete_Table_Users = "DROP TABLE IF EXISTS " + Table_Users;
    public static final String Create_Table_Users = "CREATE TABLE IF NOT EXISTS "+ Table_Users + "("
            +Users_Col_1+" VARCHAR(15) PRIMARY KEY,"
            +Users_Col_2+" INTEGER NOT NULL REFERENCES roles(role_id),"
            +Users_Col_3 +" VARCHAR(60) NOT NULL,"
            +Users_Col_4 +" CHAR(30) NOT NULL)";

    //CONSTANTS FOR TABLE STUDENT_ADDRESS
    public static final String Table_StudentAddress = " student_address";
    public static final String StudentAddress_Col_1 = " student_id";
    public static final String StudentAddress_Col_2 = " student_region_id";
    public static final String StudentAddress_Col_3 = " student_district_id";
    public static final String StudentAddress_Col_4 = " student_ward_id";
    public static final String Delete_Table_StudentAddress = "DROP TABLE IF EXISTS " + Table_StudentAddress;
    public static final String Create_Table_StudentAddress = "CREATE TABLE IF NOT EXISTS "+ Table_StudentAddress + "("
            +StudentAddress_Col_1+" CHAR(13) ,"
            +StudentAddress_Col_2+" INTEGER NOT NULL REFERENCES places(id) ,"
            +StudentAddress_Col_3 +" INTEGER NOT NULL REFERENCES places(id) ,"
            +StudentAddress_Col_4 +" INTEGER NOT NULL REFERENCES places(id) ," +
            "PRIMARY KEY(student_id,student_region_id,student_district_id,student_ward_id))";


   //CREATING A USER OBJECT
   public User user = new User();

  //CREATING THE DATABASE
    public DatabaseHelper(Context context){
        super(context,Database_Name,null,Database_version);


    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(Create_Table_Places);
        db.execSQL(Create_Table_Courses);
        db.execSQL(Create_Table_Programs);
        db.execSQL(Create_Table_Staffs);
        db.execSQL(Create_Table_Students);
        db.execSQL(Create_Table_Roles);
        db.execSQL(Create_Table_Users);
        db.execSQL(Create_Table_StudentAddress);


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(Delete_Table_Places);
        db.execSQL(Delete_Table_Courses);
        db.execSQL(Delete_Table_Programs);
        db.execSQL(Delete_Table_Staffs);
        db.execSQL(Delete_Table_Students);
        db.execSQL(Delete_Table_Roles);
        db.execSQL(Delete_Table_Users);
        db.execSQL(Delete_Table_StudentAddress);
        onCreate(db);
    }


    //METHOD TO INSERT PLACES
    public void insertPlaces(Integer id,Integer parent_id,String name){
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        ContentValues contentValues = new ContentValues();
        try {
            contentValues.put(Places_Col_1,id);
            contentValues.put(Places_Col_2 ,parent_id);
            contentValues.put(Places_Col_3,name);
            db.insert(Table_places,null,contentValues);
            db.setTransactionSuccessful();
        }
        catch (Exception e){
            e.printStackTrace();
        }
        finally {
            db.endTransaction();
            db.close();
        }

    };

    //METHOD TO INSERT PROGRAMS IN TABLE PROGRAM
    public void insertPrograms(Integer id , String name){
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        ContentValues contentValues = new ContentValues();
        try {
            contentValues.put(Programs_Col_1,id);
            contentValues.put(Programs_Col_2,name);
            db.insert(Table_Programs,null,contentValues);
            db.setTransactionSuccessful();
        }
        catch (Exception e){
            e.printStackTrace();
        }
        finally {
            db.endTransaction();
            db.close();
        }

    }

    //METHOD TO INSERT ROLES IN TABLE ROLES
    public void insertRoles(Integer id,String name,String discription){
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        ContentValues contentValues = new ContentValues();
        try {
            contentValues.put(Roles_Col_1,id);
            contentValues.put(Roles_Col_2,name);
            contentValues.put(Roles_Col_3,discription);
            db.insert(Table_Roles,null,contentValues);
            db.setTransactionSuccessful();
        }catch (Exception e){
            e.printStackTrace();
        }
        finally {
            db.endTransaction();
            db.close();
        }
    }

    //METHOD TO INSERT USERS IN TABLE USERS
    @RequiresApi(api = Build.VERSION_CODES.O)
    public void insertUsers(String username, String password, Integer role){
    SQLiteDatabase db = this.getWritableDatabase();
    db.beginTransaction();
    ContentValues contentValues = new ContentValues();

    //HASHING THE USER PASSWORD
        String myPassword = password;

        // Generate Salt. The generated value can be stored in DB.
        String salt = passwordUtils.getSalt(30);
        // Protect user's password. The generated value can be stored in DB.
        String mySecurePassword = passwordUtils.generateSecurePassword(myPassword, salt);
        // Print out protected password
        System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXx");
        System.out.println("My secure password = " + mySecurePassword);
        System.out.println("Salt value = " + salt);


        try {
        contentValues.put(Users_Col_1,username);
        contentValues.put(Users_Col_2,role);
        contentValues.put(Users_Col_3,mySecurePassword);
        contentValues.put(Users_Col_4,salt);
        db.insert(Table_Users,null,contentValues);
        db.setTransactionSuccessful();
    }catch (Exception e){
        e.printStackTrace();
    }
    finally {
        db.endTransaction();
        db.close();
    }
}

public void insertStudents(
        String regno,String fname,
        String mname,String lname,
        String phone,String email,
        String gender,
        String dob, String filePath,
        Integer studyYear, Integer studySemester,
        Integer degreeProgramId, String dor ){
    SQLiteDatabase db = this.getWritableDatabase();
    db.beginTransaction();
    ContentValues contentValues = new ContentValues();
    try {
        contentValues.put(Students_Col_1,regno);
        contentValues.put(Students_Col_2,fname);
        contentValues.put(Students_Col_3,mname);
        contentValues.put(Students_Col_4,lname);
        contentValues.put(Students_Col_5,phone);
        contentValues.put(Students_Col_6,email);
        contentValues.put(Students_Col_7,gender);
        contentValues.put(Students_Col_9,dob);
        contentValues.put(Students_Col_10,filePath);
        contentValues.put(Students_Col_11,studyYear);
        contentValues.put(Students_Col_12,studySemester);
        contentValues.put(Students_Col_13,degreeProgramId);
        contentValues.put(Students_Col_14,dor);
        db.insert(Table_students,null,contentValues);
        db.setTransactionSuccessful();



    }
    catch (Exception e){
        e.printStackTrace();
    }
    finally {
        db.endTransaction();
        db.close();
    }

}

public void insertStudentAddress(String studentId,Integer regionId,Integer districtId,Integer wardId){
    SQLiteDatabase db = this.getWritableDatabase();
    db.beginTransaction();
    ContentValues contentValues = new ContentValues();
    try {
        contentValues.put(StudentAddress_Col_1,studentId);
        contentValues.put(StudentAddress_Col_2,regionId);
        contentValues.put(StudentAddress_Col_3,districtId);
        contentValues.put(StudentAddress_Col_4,wardId);
        db.insert(Table_StudentAddress,null,contentValues);
        db.setTransactionSuccessful();

    }
    catch (Exception e){
        e.printStackTrace();
    }
    finally {
        db.endTransaction();
        db.close();
    }

}






    //METHOD TO GET PLACES PER PLACE_ID FROM TABLE PLACE
    public ArrayList<String> getPlaces(Integer parent_id){
        ArrayList<String> places = new ArrayList<String>();
        SQLiteDatabase db = this.getReadableDatabase();
        db.beginTransaction();
        try {

            String selectQuery = "SELECT name FROM places WHERE parent_id  =  ?";
            Cursor cursor = db.rawQuery(selectQuery,new String[]{parent_id.toString()});
            while (cursor.moveToNext()){
                String name = cursor.getString(cursor.getColumnIndex("name"));
                places.add(name);
            }
            db.setTransactionSuccessful();

        }
        catch (Exception e){
            e.printStackTrace();
        }
        finally {
            db.endTransaction();
            db.close();
        }
        return places;


    };

    //METHOD TO GET PARENT ID OF A PLACE
    public Integer getParentId(String place_name){
        SQLiteDatabase db = this.getReadableDatabase();
        Integer id = null;
        db.beginTransaction();
        try {
            String selectQuery = "SELECT id FROM places WHERE name  = ?";
            Cursor cursor = db.rawQuery(selectQuery,new String[]{place_name});
            if (cursor.moveToNext()){
                id = cursor.getInt(cursor.getColumnIndex("id"));

            }
            db.setTransactionSuccessful();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            db.endTransaction();
            db.close();
        }
        return id;
    };

    //METHOD TO GET USER DATA
    public void getUserInfo(String userName){

        SQLiteDatabase db = this.getReadableDatabase();
        db.beginTransaction();
        try {
            String selectQuery = "SELECT * FROM users,roles WHERE user_role_id = role_id AND user_name = ?";
            Cursor cursor = db.rawQuery(selectQuery,new String[]{userName});
            if(cursor.moveToNext()){
                user.setUserName(cursor.getString(cursor.getColumnIndex("user_name")));
                user.setPassWord( cursor.getString(cursor.getColumnIndex("password")));
                user.setSalt(cursor.getString(cursor.getColumnIndex("salt")));
                user.setRole(cursor.getString(cursor.getColumnIndex("role_name")));

            }
            db.setTransactionSuccessful();
        }
        catch (Exception e){
            e.printStackTrace();
        }
        finally {
            db.endTransaction();
            db.close();
        }


    }



}
