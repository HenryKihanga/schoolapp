
package com.example.shoolapp;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.shoolapp.model.Course;

import java.util.ArrayList;
import java.util.List;

public class manage_course_activity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private List<Course> courses;

    private Button timetable_btn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_course_activity);
        timetable_btn = findViewById(R.id.timetable_btn);

        recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);

        adapter = new MyAdapter(courses, this);
        recyclerView.setAdapter(adapter);


        setCourseDetails();


    }

    public void setCourseDetails() {
        courses = new ArrayList<>();
        for (int a = 0; a < Course.courses.length; a++) {
            courses.add(Course.courses[a]);
        }
    }


}
