package com.example.shoolapp.model;

import java.util.List;

public class Course {
    final String code;
    final String name;
    final Integer semisterofstudy;
    final Integer yearofstudy;
    final String degreeprogam;
    final String type;
    final String credit;
    final boolean isRegistered;


    public String getType() {
        return type;
    }

    public Course(String code, String name, Integer semisterofstudy, Integer yearofstudy, String degreeprogam, String type ,String credit ,boolean isRegistered) {
        this.code = code;
        this.name = name;
        this.semisterofstudy = semisterofstudy;
        this.yearofstudy = yearofstudy;
        this.degreeprogam = degreeprogam;
        this.type = type;
        this.credit = credit;
        this.isRegistered = isRegistered;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public Integer getSemisterofstudy() {
        return semisterofstudy;
    }

    public Integer getYearofstudy() {
        return yearofstudy;
    }

    public String getCredit() { return credit; }

    public boolean isRegistered() { return isRegistered; }

    public String getDegreeprogam() {
        return degreeprogam;
    }


   public static Course[] courses ={
            new Course("IS268","Database",1,1,"computer engineering","core" , "credit 12" , true),
            new Course("CS 345","Computer hardware",2,3,"computer engineering","optional" , "credit 12" , false),
            new Course("TE 248","Signal processing",1,2,"Telecommunication engineering","core" , "credit 12" , true),
            new Course("MT 161","sets and logic",1,1,"computer engineering","optional" , "credit 12" , false),
            new Course("IS268","Database",1,1,"computer engineering","core" , "credit 12" , true),
            new Course("IS268","Database",1,1,"computer engineering","core" , "credit 12" , true),
            new Course("IS268","Database",1,1,"computer engineering","core" , "credit 12" , true),

    };




}
